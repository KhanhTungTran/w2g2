import math
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
from torch_geometric.nn.dense import DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
# from torch.nn import Linear
from torch import Tensor
from typing import Optional
from torch_geometric.nn import Linear
import torch_geometric

class GAT(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1):
        super(GAT, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # GraphConvolution, GCNConv, GCNConv2
        graph_arch = DenseGATConv # GraphConvolution
        self.layers = nn.Sequential(
            graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        for i in range(n_hidden_dim):
            self.layers.append(
                graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            )
        
        self.layers.append(
            graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        if adj is None:
            # if self.first:
            #     for b in range(x.shape[0]):
            #         adj_mat =  torch.corrcoef(x[b]).detach().cpu().numpy()
            #         import matplotlib.pyplot as plt
            #         plt.imshow(adj_mat, cmap='hot')
            #         plt.savefig(f'{b}.png')
            temp_adj = []
            for b in range(x.shape[0]):
                if padding_mask[b].any():
                    try:
                        temp_adj.append(torch.corrcoef(x[b,:, :-int(padding_mask[b].sum().item()/x.shape[1])]))
                    except Exception as e:
                        import pdb
                        pdb.set_trace()
                else:
                    temp_adj.append(torch.corrcoef(x[b]))
            adj = torch.stack(temp_adj)
            # adj = torch.stack([torch.corrcoef(x[b,:, :padding_mask[b].sum().item()/x.shape[1] if padding_mask[b].any() else ]) for b in range(x.shape[0])])
            # print(adj)
            # adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
            # adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here

        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        # adj = 1 - adj # NOTE: Try decorrelate
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            x = F.dropout(x, self.dropout, training=self.training)
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)

        x = torch.max(x, dim=1, keepdim=False)[0]

        # x = GradMultiply.apply(x, self.grad_mul)
        return x


# with norm and skip connection
class GAT_v2(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_v2, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # GraphConvolution, GCNConv, GCNConv2
        graph_arch = DenseGATConv # GraphConvolution
        self.layers = nn.Sequential(
            graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        for i in range(n_hidden_dim):
            self.layers.append(
                graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
        
        self.layers.append(
            graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        if adj is None:
            # if self.first:
            #     for b in range(x.shape[0]):
            #         adj_mat =  torch.corrcoef(x[b]).detach().cpu().numpy()
            #         import matplotlib.pyplot as plt
            #         plt.imshow(adj_mat, cmap='hot')
            #         plt.savefig(f'{b}.png')
            temp_adj = []
            for b in range(x.shape[0]):
                if padding_mask[b].any():
                    try:
                        temp_adj.append(torch.corrcoef(x[b,:, :-int(padding_mask[b].sum().item()/x.shape[1])]))
                    except Exception as e:
                        import pdb
                        pdb.set_trace()
                else:
                    temp_adj.append(torch.corrcoef(x[b]))
            adj = torch.stack(temp_adj)
            # adj = torch.stack([torch.corrcoef(x[b,:, :padding_mask[b].sum().item()/x.shape[1] if padding_mask[b].any() else ]) for b in range(x.shape[0])])
            # print(adj)
            # adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
            # adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here

        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        # adj = torch.ones(adj.size()).cuda()
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            prev = x
            x = F.dropout(x, self.dropout, training=self.training)
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)
            if i != 0: # only works if final output dim == hidden dim
                x = x + prev
            x = x.permute(0,2,1)
            x = self.norms[i](x)
            x = x.permute(0,2,1)

        x = torch.max(x, dim=1, keepdim=False)[0]

        # x = GradMultiply.apply(x, self.grad_mul)
        return x


# with norm and skip connection, and global attention
class GAT_v3(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_v3, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # GraphConvolution, GCNConv, GCNConv2
        graph_arch = DenseGATConv # GraphConvolution
        self.layers = nn.Sequential(
            graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        for i in range(n_hidden_dim):
            self.layers.append(
                graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
        
        self.layers.append(
            graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        self.attn_fc = torch.nn.Linear(nfeat, nclass)
        self.self_attn = torch.nn.MultiheadAttention(
                nclass, 2, dropout=0.2, batch_first=True)
        self.attn_norm = nn.LayerNorm([n_node,nclass])

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        if adj is None:
            for b in range(x.shape[0]):
                if padding_mask[b].any():
                    try:
                        temp_adj.append(torch.corrcoef(x[b,:, :-int(padding_mask[b].sum().item()/x.shape[1])]))
                    except Exception as e:
                        import pdb
                        pdb.set_trace()
                else:
                    temp_adj.append(torch.corrcoef(x[b]))
            adj = torch.stack(temp_adj)
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        # adj = torch.ones(adj.size()).cuda()
        x = torch.nan_to_num(x, nan=0.0)
        pre = x
        # import pdb; pdb.set_trace()

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            prev = x
            x = F.dropout(x, self.dropout, training=self.training)
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)
            if i != 0: # only works if final output dim == hidden dim
                x = x + prev
            x = x.permute(0,2,1)
            x = self.norms[i](x)
            x = x.permute(0,2,1)

        # import pdb; pddb.set_trace()
        attn_in = self.attn_fc(pre)
        x_global = self.self_attn(attn_in, attn_in, attn_in, need_weights=False)
        x_global = x_global[0]
        x = x + x_global
        x = self.attn_norm(x)
        x = torch.max(x, dim=1, keepdim=False)[0]

        # x = GradMultiply.apply(x, self.grad_mul)
        return x


# with norm and skip connection, and global attention
# mean across temporal dim, follow WhisperAE
class GAT_v4(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_v4, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # GraphConvolution, GCNConv, GCNConv2
        graph_arch = DenseGCNConv # GraphConvolution
        self.layers = nn.Sequential(
            # graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(nfeat, hidden_dim),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim)
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
        
        self.layers.append(
            # graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(hidden_dim, nclass)
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        self.attn_fc = torch.nn.Linear(nfeat, nclass)
        self.self_attn = torch.nn.MultiheadAttention(
                nclass, 2, dropout=0.2, batch_first=True)
        self.attn_norm = nn.LayerNorm([n_node, nclass])

        self.final_norm = nn.LayerNorm([n_node*2])

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        if adj is None:
            for b in range(x.shape[0]):
                if padding_mask[b].any():
                    try:
                        temp_adj.append(torch.corrcoef(x[b,:, :-int(padding_mask[b].sum().item()/x.shape[1])]))
                    except Exception as e:
                        import pdb
                        pdb.set_trace()
                else:
                    temp_adj.append(torch.corrcoef(x[b]))
            adj = torch.stack(temp_adj)
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        # adj = torch.ones(adj.size()).cuda()
        x = torch.nan_to_num(x, nan=0.0)
        pre = x
        # import pdb; pdb.set_trace()

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            prev = x
            x = F.dropout(x, self.dropout, training=self.training)
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)
            if i != 0: # only works if final output dim == hidden dim
                x = x + prev
            x = x.permute(0,2,1)
            x = self.norms[i](x)
            x = x.permute(0,2,1)

        # import pdb; pdb.set_trace()
        attn_in = self.attn_fc(pre)
        x_global = self.self_attn(attn_in, attn_in, attn_in, need_weights=False)
        x_global = x_global[0]
        x_global = self.attn_norm(x_global)

        x = torch.mean(x, dim=2)
        x_global = torch.mean(x_global, dim=2)

        # x = x + x_global
        x = torch.concat([x, x_global], dim=-1)
        x = self.final_norm(x)
        # x = torch.max(x, dim=1, keepdim=False)[0]

        # x = GradMultiply.apply(x, self.grad_mul)
        return x


# with virtual node
# with norm and skip connection, and global attention
# mean across temporal dim, follow WhisperAE
class GAT_virtual_node(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_virtual_node, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # GraphConvolution, GCNConv, GCNConv2
        graph_arch = DenseGATConv # GraphConvolution
        self.layers = nn.Sequential(
            graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        self.fc_virtual_node = nn.Sequential(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.ReLU(),
                nn.BatchNorm1d(hidden_dim),
            )
        )

        for i in range(n_hidden_dim):
            self.layers.append(
                graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
            self.fc_virtual_node.append(
                nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.ReLU(),
                nn.BatchNorm1d(hidden_dim),
            )
            )
        
        self.layers.append(
            graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )
        self.fc_virtual_node.append(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.ReLU(),
                nn.BatchNorm1d(hidden_dim),
            )
        )
        self.virtual_node = nn.Parameter(torch.zeros(1, 1, hidden_dim)) # NOTE: only works if hidden_dim == nclass (output dim)

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        # self.attn_fc = torch.nn.Linear(nfeat, nclass)
        # self.self_attn = torch.nn.MultiheadAttention(
        #         nclass, 2, dropout=0.2, batch_first=True)
        # self.attn_norm = nn.LayerNorm([n_node, nclass])

        # self.final_norm = nn.LayerNorm([nclass])

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]

        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        # adj = torch.ones(adj.size()).cuda()
        x = torch.nan_to_num(x, nan=0.0)
        pre = x
        # import pdb; pdb.set_trace()

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            prev = x
            x = F.dropout(x, self.dropout, training=self.training)
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)
            if i != 0: # only works if final output dim == hidden dim
                x = x + prev
            x = x.permute(0,2,1)
            x = self.norms[i](x)
            x = x.permute(0,2,1)

            if i == 0:
                virtual_node = self.fc_virtual_node[i](torch.cat((self.virtual_node.expand(x.shape[0], -1, -1), x), dim=1).permute(0,2,1))
            else:
                virtual_node = self.fc_virtual_node[i](torch.cat((virtual_node, x), dim=1).permute(0,2,1))
            virtual_node = virtual_node.permute(0,2,1) # after permute: B x 1 x 256

        # import pdb; pdb.set_trace()
        # attn_in = self.attn_fc(pre)
        # x_global = self.self_attn(attn_in, attn_in, attn_in, need_weights=False)
        # x_global = x_global[0]
        # x_global = self.attn_norm(x_global)

        # x = torch.mean(virtual, dim=2)
        # x_global = torch.mean(x_global, dim=2)

        # x = x + x_global
        # # x = torch.concat([x, x_global], dim=-1)
        # x = self.final_norm(x)
        # x = torch.max(x, dim=1, keepdim=False)[0]

        # x = GradMultiply.apply(x, self.grad_mul)
        # virtual_node = self.final_norm(virtual_node)
        # virtual_node = GradMultiply.apply(virtual_node, self.grad_mul)
        return virtual_node


# with virtual node
# with norm and skip connection, and global attention
# mean across temporal dim, follow WhisperAE
class GAT_virtual_node_v2(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_virtual_node_v2, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGATConv # GraphConvolution
        self.layers = nn.Sequential(
            graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        self.fc_virtual_node = nn.Sequential(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )

        for i in range(n_hidden_dim):
            self.layers.append(
                graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
            self.fc_virtual_node.append(
                nn.Sequential(
                nn.Linear(n_node+1, 1),
                # nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
            )
        
        self.layers.append(
            graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )
        self.fc_virtual_node.append(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )
        self.virtual_node = nn.Parameter(torch.zeros(1, 1, hidden_dim)) # NOTE: only works if hidden_dim == nclass (output dim)

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        # self.attn_fc = torch.nn.Linear(nfeat, nclass)
        # self.self_attn = torch.nn.MultiheadAttention(
        #         nclass, 2, dropout=0.2, batch_first=True)
        # self.attn_norm = nn.LayerNorm([n_node, nclass])

        # self.final_norm = nn.LayerNorm([nclass])

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]

        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        # adj = torch.ones(adj.size()).cuda()
        x = torch.nan_to_num(x, nan=0.0)
        pre = x
        # import pdb; pdb.set_trace()

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            prev = x
            x = F.dropout(x, self.dropout, training=self.training)
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)
            if i != 0: # only works if final output dim == hidden dim
                x = x + prev
            x = x.permute(0,2,1)
            x = self.norms[i](x)
            x = x.permute(0,2,1)

            if i == 0:
                virtual_node = self.fc_virtual_node[i](torch.cat((self.virtual_node.expand(x.shape[0], -1, -1), x), dim=1).permute(0,2,1))
            else:
                virtual_node = self.fc_virtual_node[i](torch.cat((virtual_node, x), dim=1).permute(0,2,1))
            virtual_node = virtual_node.permute(0,2,1) # after permute: B x 1 x 256

        # import pdb; pdb.set_trace()
        # attn_in = self.attn_fc(pre)
        # x_global = self.self_attn(attn_in, attn_in, attn_in, need_weights=False)
        # x_global = x_global[0]
        # x_global = self.attn_norm(x_global)

        # x = torch.mean(virtual, dim=2)
        # x_global = torch.mean(x_global, dim=2)

        # x = x + x_global
        # # x = torch.concat([x, x_global], dim=-1)
        # x = self.final_norm(x)
        # x = torch.max(x, dim=1, keepdim=False)[0]

        # x = GradMultiply.apply(x, self.grad_mul)
        # virtual_node = self.final_norm(virtual_node)
        # virtual_node = GradMultiply.apply(virtual_node, self.grad_mul)
        return virtual_node


# with virtual node
# with norm and skip connection, and global attention
# mean across temporal dim, follow WhisperAE
class GAT_virtual_node_v3(GAT_virtual_node_v2):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_virtual_node_v3, self).__init__(nfeat, hidden_dim, nclass, dropout=dropout, grad_mul=grad_mul, n_hidden_dim=n_hidden_dim, adj_thresh=adj_thresh, inner_dropout=adj_thresh, add_self_loops=add_self_loops, n_head=n_head, n_node=n_node)
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGATConv # GraphConvolution
        self.layers = nn.Sequential(
            graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        self.fc_virtual_node = nn.Sequential(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.GELU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )

        for i in range(n_hidden_dim):
            self.layers.append(
                graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
            self.fc_virtual_node.append(
                nn.Sequential(
                nn.Linear(n_node+1, 1),
                # nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
            )
        
        self.layers.append(
            graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )
        self.fc_virtual_node.append(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.GELU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )
        self.virtual_node = nn.Parameter(torch.zeros(1, 1, hidden_dim)) # NOTE: only works if hidden_dim == nclass (output dim)

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops


# with virtual node
# with norm and skip connection, and global attention
# mean across temporal dim, follow WhisperAE
# Graph Conv
class GAT_virtual_node_v4(GAT_virtual_node_v2):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_virtual_node_v4, self).__init__(nfeat, hidden_dim, nclass, dropout=dropout, grad_mul=grad_mul, n_hidden_dim=n_hidden_dim, adj_thresh=adj_thresh, inner_dropout=adj_thresh, add_self_loops=add_self_loops, n_head=n_head, n_node=n_node)
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.layers = nn.Sequential(
            # graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(nfeat, hidden_dim,),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        self.fc_virtual_node = nn.Sequential(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )

        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
            self.fc_virtual_node.append(
                nn.Sequential(
                nn.Linear(n_node+1, 1),
                # nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
            )
        
        self.layers.append(
            # graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(hidden_dim, nclass,),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )
        self.fc_virtual_node.append(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )
        self.virtual_node = nn.Parameter(torch.zeros(1, 1, hidden_dim)) # NOTE: only works if hidden_dim == nclass (output dim)

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        # self.attn_fc = torch.nn.Linear(nfeat, nclass)
        # self.self_attn = torch.nn.MultiheadAttention(
        #         nclass, 2, dropout=0.2, batch_first=True)
        # self.attn_norm = nn.LayerNorm([n_node, nclass])

        # self.final_norm = nn.LayerNorm([nclass])



# with virtual node
# with norm and skip connection, and global attention
# mean across temporal dim, follow WhisperAE
# Graph Conv + global attention
class GAT_virtual_node_v5(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_virtual_node_v5, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.layers = nn.Sequential(
            # graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(nfeat, hidden_dim,),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        self.fc_virtual_node = nn.Sequential(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )

        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
            self.fc_virtual_node.append(
                nn.Sequential(
                nn.Linear(n_node+1, 1),
                # nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
            )
        
        self.layers.append(
            # graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(hidden_dim, nclass,),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )
        self.fc_virtual_node.append(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )
        self.virtual_node = nn.Parameter(torch.zeros(1, 1, hidden_dim)) # NOTE: only works if hidden_dim == nclass (output dim)

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        self.attn_fc = torch.nn.Linear(nfeat, nclass)
        self.self_attn = torch.nn.MultiheadAttention(
                nclass, 2, dropout=0.2, batch_first=True)
        # self.attn_norm = nn.LayerNorm([n_node, nclass])

        self.final_norm = nn.LayerNorm([nclass])

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]

        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        # adj = torch.ones(adj.size()).cuda()
        x = torch.nan_to_num(x, nan=0.0)
        pre = x
        # import pdb; pdb.set_trace()

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            prev = x
            x = F.dropout(x, self.dropout, training=self.training)
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)
            if i != 0: # only works if final output dim == hidden dim
                x = x + prev
            x = x.permute(0,2,1)
            x = self.norms[i](x)
            x = x.permute(0,2,1)

            if i == 0:
                virtual_node = self.fc_virtual_node[i](torch.cat((self.virtual_node.expand(x.shape[0], -1, -1), x), dim=1).permute(0,2,1))
            else:
                virtual_node = self.fc_virtual_node[i](torch.cat((virtual_node, x), dim=1).permute(0,2,1))
            virtual_node = virtual_node.permute(0,2,1) # after permute: B x 1 x 256

        attn_in = self.attn_fc(pre)
        x_global = self.self_attn(attn_in, attn_in, attn_in, need_weights=False)
        x_global = x_global[0]
        x_global = torch.mean(x_global, dim=1)

        virtual_node = virtual_node.squeeze()
        virtual_node = virtual_node + x_global
        virtual_node = self.final_norm(virtual_node)

        return virtual_node


# with virtual node, gelu instead of relu
# with norm and skip connection, and global attention
# mean across temporal dim, follow WhisperAE
# Graph Conv
class GAT_virtual_node_v6(GAT_virtual_node_v2):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_virtual_node_v6, self).__init__(nfeat, hidden_dim, nclass, dropout=dropout, grad_mul=grad_mul, n_hidden_dim=n_hidden_dim, adj_thresh=adj_thresh, inner_dropout=adj_thresh, add_self_loops=add_self_loops, n_head=n_head, n_node=n_node)
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.layers = nn.Sequential(
            # graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(nfeat, hidden_dim,),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        self.fc_virtual_node = nn.Sequential(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.GELU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )

        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
            self.fc_virtual_node.append(
                nn.Sequential(
                nn.Linear(n_node+1, 1),
                # nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
            )

        self.layers.append(
            # graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(hidden_dim, nclass,),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )
        self.fc_virtual_node.append(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.GELU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )
        self.virtual_node = nn.Parameter(torch.zeros(1, 1, hidden_dim)) # NOTE: only works if hidden_dim == nclass (output dim)

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops


# with virtual node, gelu instead of relu, and batchnorm at last
# with norm and skip connection, and global attention
# mean across temporal dim, follow WhisperAE
# Graph Conv
class GAT_virtual_node_v7(GAT_virtual_node_v2):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_virtual_node_v7, self).__init__(nfeat, hidden_dim, nclass, dropout=dropout, grad_mul=grad_mul, n_hidden_dim=n_hidden_dim, adj_thresh=adj_thresh, inner_dropout=adj_thresh, add_self_loops=add_self_loops, n_head=n_head, n_node=n_node)
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.layers = nn.Sequential(
            # graph_arch(nfeat, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(nfeat, hidden_dim,),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )
        self.fc_virtual_node = nn.Sequential(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.GELU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )

        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
            self.fc_virtual_node.append(
                nn.Sequential(
                nn.Linear(n_node+1, 1),
                # nn.ReLU(),
                # nn.BatchNorm1d(hidden_dim),
            )
            )
        
        self.layers.append(
            # graph_arch(hidden_dim, nclass, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
            graph_arch(hidden_dim, nclass,),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )
        self.fc_virtual_node.append(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.GELU(),
                nn.BatchNorm1d(hidden_dim),
            )
        )
        self.virtual_node = nn.Parameter(torch.zeros(1, 1, hidden_dim)) # NOTE: only works if hidden_dim == nclass (output dim)

        self.act = nn.ELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops


# with virtual node, gelu instead of relu, init with normal distribution
# with norm and skip connection, and global attention
# mean across temporal dim, follow WhisperAE
# Graph Conv
class GAT_virtual_node_v8(GAT_virtual_node_v6):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GAT_virtual_node_v8, self).__init__(nfeat, hidden_dim, nclass, dropout=dropout, grad_mul=grad_mul, n_hidden_dim=n_hidden_dim, adj_thresh=adj_thresh, inner_dropout=adj_thresh, add_self_loops=add_self_loops, n_head=n_head, n_node=n_node)
        nn.init.normal_(self.virtual_node, std=1e-6)



# different embeddings for each node
class GNN_sep_emb(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GNN_sep_emb, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_embs = [
            nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        ]
        self.layers = nn.Sequential(
            graph_arch(hidden_dim, hidden_dim),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )

        for i in range(n_hidden_dim):
            self.pre_embs.append(
                nn.Parameter(torch.randn(n_node, hidden_dim, hidden_dim))
            )
            self.layers.append(
                graph_arch(hidden_dim, hidden_dim),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
        
        self.pre_embs.append(
            nn.Parameter(torch.randn(n_node, hidden_dim, hidden_dim))
        )
        self.layers.append(
            graph_arch(hidden_dim, nclass),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )

        self.act = nn.LeakyReLU(0.2)
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        for pre_emb in self.pre_embs:
            stdv = 1. / math.sqrt(pre_emb.size(2))
            pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            # pre-embeddings:
            x = torch.einsum('bnh,nho->bno', x, self.pre_embs[i].cuda())
            prev = x
            x = F.dropout(x, self.dropout, training=self.training)

            # graph:
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)

            # skip connection:
            x = x + prev

            # norm:
            x = x.permute(0,2,1)
            x = self.norms[i](x)
            x = x.permute(0,2,1)

        x = torch.max(x, dim=1, keepdim=False)[0]

        return x


# different embeddings for each node
# gelu between
class GNN_sep_emb_v2(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GNN_sep_emb_v2, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_embs = [
            nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        ]
        self.layers = nn.Sequential(
            graph_arch(hidden_dim, hidden_dim),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )

        for i in range(n_hidden_dim):
            self.pre_embs.append(
                nn.Parameter(torch.randn(n_node, hidden_dim, hidden_dim))
            )
            self.layers.append(
                graph_arch(hidden_dim, hidden_dim),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
        
        self.pre_embs.append(
            nn.Parameter(torch.randn(n_node, hidden_dim, hidden_dim))
        )
        self.layers.append(
            graph_arch(hidden_dim, nclass),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )

        self.act = nn.LeakyReLU(0.2)
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        for pre_emb in self.pre_embs:
            stdv = 1. / math.sqrt(pre_emb.size(2))
            pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            # pre-embeddings:
            x = torch.einsum('bnh,nho->bno', x, self.pre_embs[i].cuda())
            x = torch.nn.functional.gelu(x)
            prev = x
            x = F.dropout(x, self.dropout, training=self.training)

            # graph:
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)

            # skip connection:
            x = x + prev

            # norm:
            x = x.permute(0,2,1)
            x = self.norms[i](x)
            x = x.permute(0,2,1)

        x = torch.max(x, dim=1, keepdim=False)[0]

        return x



# different embeddings for each node
# with virtual node
class GNN_sep_emb_v3(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GNN_sep_emb_v3, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_embs = [
            nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        ]
        self.layers = nn.Sequential(
            graph_arch(hidden_dim, hidden_dim),
        )
        self.norms = nn.Sequential(
            # nn.BatchNorm1d(hidden_dim)
            nn.LayerNorm([hidden_dim,n_node])
        )

        self.fc_virtual_node = nn.Sequential(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.GELU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )

        for i in range(n_hidden_dim):
            self.pre_embs.append(
                nn.Parameter(torch.randn(n_node, hidden_dim, hidden_dim))
            )
            self.layers.append(
                graph_arch(hidden_dim, hidden_dim),
            )
            self.norms.append(
                # nn.BatchNorm1d(hidden_dim)
                nn.LayerNorm([hidden_dim,n_node])
            )
            self.fc_virtual_node.append(
                nn.Sequential(
                    nn.Linear(n_node+1, 1),
                    # nn.ReLU(),
                    # nn.BatchNorm1d(hidden_dim),
                )
            )
        
        self.pre_embs.append(
            nn.Parameter(torch.randn(n_node, hidden_dim, hidden_dim))
        )
        self.layers.append(
            graph_arch(hidden_dim, nclass),
        )
        self.norms.append(
            # nn.BatchNorm1d(nclass)
            nn.LayerNorm([nclass,n_node])
        )
        self.fc_virtual_node.append(
            nn.Sequential(
                nn.Linear(n_node+1, 1),
                nn.GELU(),
                # nn.BatchNorm1d(hidden_dim),
            )
        )
        self.virtual_node = nn.Parameter(torch.zeros(1, 1, hidden_dim)) # NOTE: only works if hidden_dim == nclass (output dim)
        nn.init.normal_(self.virtual_node, std=1e-6)

        self.act = nn.LeakyReLU(0.2)
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        for pre_emb in self.pre_embs:
            stdv = 1. / math.sqrt(pre_emb.size(2))
            pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        for i in range(len(self.layers)):
            # pre-embeddings:
            x = torch.einsum('bnh,nho->bno', x, self.pre_embs[i].cuda())
            prev = x
            x = F.dropout(x, self.dropout, training=self.training)

            # graph:
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            if i != len(self.layers) - 1:
                x = self.act(x)

            # skip connection:
            x = x + prev

            # norm:
            x = x.permute(0,2,1)
            x = self.norms[i](x)
            x = x.permute(0,2,1)

            if i == 0:
                virtual_node = self.fc_virtual_node[i](torch.cat((self.virtual_node.expand(x.shape[0], -1, -1), x), dim=1).permute(0,2,1))
            else:
                virtual_node = self.fc_virtual_node[i](torch.cat((virtual_node, x), dim=1).permute(0,2,1))
            virtual_node = virtual_node.permute(0,2,1) # after permute: B x 1 x 256

        # x = torch.max(x, dim=1, keepdim=False)[0]

        return virtual_node



# different embeddings for each node
# only 1 layer, to match a single layer fully connected
class GNN_sep_emb_v4(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GNN_sep_emb_v4, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_emb = nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        self.layer = graph_arch(nfeat, hidden_dim)
        # self.norm = nn.LayerNorm([hidden_dim,n_node])

        self.act = nn.GELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        stdv = 1. / math.sqrt(self.pre_emb.size(2))
        self.pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        # pre-embeddings:
        prev = torch.einsum('bnh,nho->bo', x, self.pre_emb.cuda())

        # graph:
        x = self.layer(x, adj, add_loop=self.add_loop)
        # skip connection:
        x = torch.max(x, dim=1, keepdim=False)[0]
        x = F.dropout(x, self.dropout, training=self.training)

        prev = self.act(prev)
        x = x + prev

        x = F.dropout(x, self.dropout, training=self.training)

        return x


# different embeddings for each node
# only 1 layer, to match a single layer fully connected
# 2 gnn layers before skip connection
class GNN_sep_emb_v5(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GNN_sep_emb_v5, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_emb = nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        self.layers = nn.Sequential(
            graph_arch(nfeat, hidden_dim),
            graph_arch(hidden_dim, hidden_dim),
        )
        # self.norm = nn.LayerNorm([hidden_dim,n_node])

        self.act = nn.LeakyReLU(0.2)
        self.final_act = nn.GELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        stdv = 1. / math.sqrt(self.pre_emb.size(2))
        self.pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        # pre-embeddings:
        prev = torch.einsum('bnh,nho->bo', x, self.pre_emb.cuda())

        for i in range(len(self.layers)):
            # graph:
            x = self.layers[i](x, adj, add_loop=self.add_loop)
            # skip connection:
            x = self.act(x)
            x = F.dropout(x, self.dropout, training=self.training)

        x = torch.max(x, dim=1, keepdim=False)[0]
        prev = self.final_act(prev)
        x = x + prev

        x = F.dropout(x, self.dropout, training=self.training)

        return x


# different embeddings for each node
# only 1 layer, to match a single layer fully connected
# input to gnn are output of different embeddings
class GNN_sep_emb_v6(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(GNN_sep_emb_v6, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_emb = nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        self.layers = nn.Sequential(graph_arch(hidden_dim, hidden_dim))
        # self.norm = nn.LayerNorm([hidden_dim,n_node])
        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )

        self.act = nn.GELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        stdv = 1. / math.sqrt(self.pre_emb.size(2))
        self.pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        # pre-embeddings:
        x = torch.einsum('bnh,nho->bno', x, self.pre_emb.cuda())
        prev = x

        # graph:
        for layer in self.layers:
            x = layer(x, adj, add_loop=self.add_loop)
            # skip connection:
            x = self.act(x)
            x = F.dropout(x, self.dropout, training=self.training)

        # skip connection:
        # prev = torch.max(prev, dim=1, keepdim=False)[0]
        # prev = self.act(prev)

        # x = torch.max(x, dim=1, keepdim=False)[0]
        # x = x + prev

        # skip connection:
        # prev = self.act(prev)
        # prev = torch.max(prev, dim=1, keepdim=False)[0]

        # x = torch.max(x, dim=1, keepdim=False)[0]
        # x = x + prev

        # skip connection:
        prev = self.act(prev)
        prev = torch.mean(prev, dim=1)

        x = torch.max(x, dim=1, keepdim=False)[0]
        x = x + prev

        x = F.dropout(x, self.dropout, training=self.training)

        return x


# different embeddings for each node
# with virtual node
# no GNN layer
class only_emb(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(only_emb, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        self.pre_emb = nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))

        self.act = nn.GELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        stdv = 1. / math.sqrt(self.pre_emb.size(2))
        self.pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        # pre-embeddings:
        prev = torch.einsum('bnh,nho->bo', x, self.pre_emb.cuda())

        # graph:
        x = prev
        x = self.act(x)

        return x


# different embeddings for each node
# only 1 layer, to match a single layer fully connected
# no skip connection
class only_gnn(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(only_gnn, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_emb = nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        self.layers = nn.Sequential(graph_arch(hidden_dim, hidden_dim))
        # self.norm = nn.LayerNorm([hidden_dim,n_node])
        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )

        self.act = nn.GELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        stdv = 1. / math.sqrt(self.pre_emb.size(2))
        self.pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)
        # pre-embeddings:
        x = torch.einsum('bnh,nho->bno', x, self.pre_emb.cuda())

        # graph:
        for layer in self.layers:
            x = layer(x, adj, add_loop=self.add_loop)
            # skip connection:
            x = self.act(x)
            x = F.dropout(x, self.dropout, training=self.training)
        x = torch.max(x, dim=1, keepdim=False)[0]
        x = F.dropout(x, self.dropout, training=self.training)

        return x


# different embeddings for each node
# only 1 layer, to match a single layer fully connected
# no skip connection
class only_gnn_decorr_adj(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(only_gnn_decorr_adj, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_emb = nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        self.layers = nn.Sequential(graph_arch(hidden_dim, hidden_dim))
        # self.norm = nn.LayerNorm([hidden_dim,n_node])
        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )

        self.act = nn.GELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        stdv = 1. / math.sqrt(self.pre_emb.size(2))
        self.pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        adj = 1 - adj
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)
        # pre-embeddings:
        x = torch.einsum('bnh,nho->bno', x, self.pre_emb.cuda())

        # graph:
        for layer in self.layers:
            x = layer(x, adj, add_loop=self.add_loop)
            # skip connection:
            x = self.act(x)
            x = F.dropout(x, self.dropout, training=self.training)
        x = torch.max(x, dim=1, keepdim=False)[0]
        x = F.dropout(x, self.dropout, training=self.training)

        return x


# different embeddings for each node
# only 1 layer, to match a single layer fully connected
# no skip connection
class only_gnn_fc_adj(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(only_gnn_fc_adj, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_emb = nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        self.layers = nn.Sequential(graph_arch(hidden_dim, hidden_dim))
        # self.norm = nn.LayerNorm([hidden_dim,n_node])
        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )

        self.act = nn.GELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        stdv = 1. / math.sqrt(self.pre_emb.size(2))
        self.pre_emb.data.uniform_(-stdv, stdv)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        adj = torch.ones_like(adj)
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)
        # pre-embeddings:
        x = torch.einsum('bnh,nho->bno', x, self.pre_emb.cuda())

        # graph:
        for layer in self.layers:
            x = layer(x, adj, add_loop=self.add_loop)
            # skip connection:
            x = self.act(x)
            x = F.dropout(x, self.dropout, training=self.training)
        x = torch.max(x, dim=1, keepdim=False)[0]
        x = F.dropout(x, self.dropout, training=self.training)

        return x


# different embeddings for each node
# only 1 layer, to match a single layer fully connected
# no skip connection
class only_gnn_v2(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(only_gnn_v2, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_emb = nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        self.layers = nn.Sequential(graph_arch(hidden_dim, hidden_dim))
        # self.norm = nn.LayerNorm([hidden_dim,n_node])
        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )

        self.act = nn.GELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        stdv = 1. / math.sqrt(self.pre_emb.size(2))
        self.pre_emb.data.uniform_(-stdv, stdv)
        self.norm = torch.nn.LayerNorm(hidden_dim)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        # pre-embeddings:
        x = torch.einsum('bnh,nho->bno', x, self.pre_emb.cuda())

        # graph:
        for layer in self.layers:
            x = layer(x, adj, add_loop=self.add_loop)
            # skip connection:
            x = self.act(x)
            x = F.dropout(x, self.dropout, training=self.training)
        x = torch.max(x, dim=1, keepdim=False)[0]
        x = self.norm(x)
        x = F.dropout(x, self.dropout, training=self.training)

        return x


class only_gnn_v3(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(only_gnn_v3, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.pre_emb = nn.Parameter(torch.randn(n_node, nfeat, hidden_dim))
        self.layers = nn.Sequential(graph_arch(hidden_dim, hidden_dim))
        # self.norm = nn.LayerNorm([hidden_dim,n_node])
        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )

        self.act = nn.GELU()
        self.grad_mul = grad_mul
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

        stdv = 1. / math.sqrt(self.pre_emb.size(2))
        self.pre_emb.data.uniform_(-stdv, stdv)
        self.norm = torch.nn.BatchNorm1d(hidden_dim)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        # pre-embeddings:
        x = torch.einsum('bnh,nho->bno', x, self.pre_emb.cuda())

        # graph:
        for layer in self.layers:
            x = layer(x, adj, add_loop=self.add_loop)
            # skip connection:
            x = self.act(x)
            x = F.dropout(x, self.dropout, training=self.training)
        x = torch.max(x, dim=1, keepdim=False)[0]
        x = self.norm(x)
        x = F.dropout(x, self.dropout, training=self.training)

        return x


# only 1 layer, to match a single layer fully connected
# no skip connection, and no different embeddings
class only_gnn_no_diff_emb(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5, grad_mul=1.0, n_hidden_dim=1, adj_thresh=-1.0, inner_dropout=0.0, add_self_loops=True, n_head=1, n_node=64):
        super(only_gnn_no_diff_emb, self).__init__()
        self.dropout = dropout
        self.nfeat = nfeat

        # DenseGCNConv, DenseGraphConv, DenseSAGEConv, DenseGATConv
        graph_arch = DenseGCNConv # GraphConvolution
        self.layers = nn.Sequential(graph_arch(nfeat, hidden_dim))
        for i in range(n_hidden_dim):
            self.layers.append(
                # graph_arch(hidden_dim, hidden_dim, dropout=inner_dropout, heads=n_head, concat=False if n_head!=1 else True),
                graph_arch(hidden_dim, hidden_dim,),
            )

        self.act = nn.GELU()
        self.adj_thresh = adj_thresh
        self.add_loop=add_self_loops

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [batch_size, num_nodes, feature_dim]
        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x = torch.nan_to_num(x, nan=0.0)

        if self.adj_thresh != -1.0:
            adj = torch.where(adj > self.adj_thresh, adj, 0.0)

        # graph:
        for layer in self.layers:
            x = layer(x, adj, add_loop=self.add_loop)
            # skip connection:
            x = self.act(x)
            x = F.dropout(x, self.dropout, training=self.training)
        x = torch.max(x, dim=1, keepdim=False)[0]
        x = F.dropout(x, self.dropout, training=self.training)

        return x
