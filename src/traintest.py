import os
import datetime
from utilities import *
import time
import torch
import numpy as np
import pickle
from torch.optim.swa_utils import AveragedModel, SWALR
import wandb
from tqdm import tqdm
from munch import Munch
from drloc import cal_selfsupervised_loss

def _weight_decay(init_weight, epoch, warmup_epochs=20, total_epoch=300):
    if epoch <= warmup_epochs:
        cur_weight = min(init_weight / warmup_epochs * epoch, init_weight)
    else:
        cur_weight = init_weight * (1.0 - (epoch - warmup_epochs)/(total_epoch - warmup_epochs))
    return cur_weight


def train(audio_model, train_loader, test_loader, args):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)
    torch.set_grad_enabled(True)
    # Initialize all of the statistics we want to keep track of
    batch_time = AverageMeter()
    per_sample_time = AverageMeter()
    data_time = AverageMeter()
    per_sample_data_time = AverageMeter()
    loss_meter = AverageMeter()
    loss_sup_meter = AverageMeter()
    loss_ssup_meter = AverageMeter()
    per_sample_dnn_time = AverageMeter()
    progress = []
    best_epoch, best_cum_epoch, best_mAP, best_acc, best_cum_mAP = 0, 0, -np.inf, -np.inf, -np.inf
    global_step, epoch = 0, 0
    swa_sign = False
    start_time = time.time()
    exp_dir = args.exp_dir

    def _save_progress():
        progress.append([epoch, global_step, best_epoch, best_mAP,
                time.time() - start_time])
        with open("%s/progress.pkl" % exp_dir, "wb") as f:
            pickle.dump(progress, f)

    if not isinstance(audio_model, nn.DataParallel):
        audio_model = nn.DataParallel(audio_model)

    audio_model = audio_model.to(device)
    # Set up the optimizer
    audio_trainables = audio_model.module.get_config_optim(args.lr, args.weight_decay, args.lrp)  # [p for p in audio_model.parameters() if p.requires_grad]
    print('Total parameter number is : {:.3f} million'.format(sum(p.numel() for p in audio_model.parameters()) / 1000000))
    #print('Total trainable parameter number is : {:.3f} million'.format(sum(p.numel() for p in audio_trainables) / 1000000))
    trainables = audio_trainables

    optimizer = torch.optim.AdamW(trainables, args.lr, weight_decay=args.weight_decay, betas=(0.95, 0.999))

    # print('now use new scheduler')
    # scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, list(range(10, 60)), gamma=1.0)
    # scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, args.lr, epochs=args.n_epochs, steps_per_epoch=492,)
    # scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, start_factor=0.1, total_iters=4)

    class_weights=None
    if args.class_weights:
        label_count = np.zeros(train_loader.dataset.label_num)
        for datum in train_loader.dataset.data:
            for label_str in datum['labels'].split(','):
                label_count[int(train_loader.dataset.index_dict[label_str])] += 1.0
        label_count = (1/label_count) * sum(label_count)
        class_weights = torch.FloatTensor(label_count).to(device)
        if args.model == 'AST_pretrained': class_weights = class_weights.to(torch.float16)

    epoch += 1

    print("current #steps=%s, #epochs=%s" % (global_step, epoch))
    print("start training...")

    result = np.zeros([args.n_epochs, 10])
    audio_model.train()
    while epoch < args.n_epochs + 1:
        print("---------------------Epoch {:d} Start---------------------".format(epoch))
        begin_time = time.time()
        end_time = time.time()
        audio_model.train()
        print(datetime.datetime.now())

        A_predictions = []
        A_targets = []
        # print("LEARNING RATE: ", scheduler.get_last_lr())
        for i, (input_data, labels) in tqdm(enumerate(train_loader)):
            if args.model.endswith('mfcc'):
                audio_input, _, _ = input_data
            elif args.model.endswith('mfcc_temporal'):
                audio_input, _, _, _ = input_data
            elif args.model == 'AST_GNN' or args.model == 'AST_GNN_diff_size' or args.model=='AST_GNN_multi_res':
                audio_input, _ = input_data
            else:
                audio_input = input_data
            # measure data loading time
            B = audio_input.size(0)
            audio_input = audio_input.to(device, non_blocking=True)
            if args.multi_task:
                labels, second_task_labels, third_task_labels = labels
                second_task_labels = second_task_labels.to(device, non_blocking=True)
                third_task_labels = third_task_labels.to(device, non_blocking=True)
            A_targets.append(labels)
            labels = labels.to(device, non_blocking=True)

            data_time.update(time.time() - end_time)
            per_sample_data_time.update((time.time() - end_time) / audio_input.shape[0])
            dnn_start_time = time.time()

            if args.use_drloc:
                # import pdb; pdb.set_trace()
                audio_output, outs = audio_model(input_data)
            elif args.multi_task:
                audio_output, second_task_audio_output, third_task_audio_output = audio_model(input_data)
            else:
                audio_output = audio_model(input_data)

            if args.multi_label: 
                loss_fn = nn.BCELoss(weight=class_weights)
                if args.model == 'AST_pretrained': labels = labels.to(torch.float16)
                loss = loss_fn(audio_output, labels)
            else:
                loss_fn = nn.CrossEntropyLoss(weight=class_weights)
                loss = loss_fn(audio_output, torch.argmax(labels.long(), axis=1))

            if args.multi_task:
                num_label = second_task_labels.shape[-1]
                second_task_labels = second_task_labels.unsqueeze(1).repeat(1, int(second_task_audio_output.shape[0]/second_task_labels.shape[0]), 1).reshape(-1,num_label)
                loss_fn = nn.CrossEntropyLoss()
                second_task_loss = loss_fn(second_task_audio_output, torch.argmax(second_task_labels.long(), axis=1))

                num_label = third_task_labels.shape[-1]
                third_task_labels = third_task_labels.unsqueeze(1).repeat(1, int(third_task_audio_output.shape[0]/third_task_labels.shape[0]), 1).reshape(-1,num_label)
                loss_fn = nn.CrossEntropyLoss()
                third_task_loss = loss_fn(third_task_audio_output, torch.argmax(third_task_labels.long(), axis=1))

                loss = loss + args.second_task_lambda * (second_task_loss+third_task_loss)

            if args.use_drloc:
                # import pdb; pdb.set_trace()
                init_lambda_drloc = _weight_decay(
                    args.lambda_drloc, 
                    epoch, 
                    int(args.n_epochs/6), 
                    args.n_epochs)
                loss_ssup, ssup_items = cal_selfsupervised_loss(outs, args, init_lambda_drloc)
                loss_sup = loss
                loss += loss_ssup

            # original optimization
            optimizer.zero_grad()
            loss.backward()

            torch.nn.utils.clip_grad_norm_(audio_model.module.parameters(), max_norm=args.clip_norm)
            optimizer.step()

            # record loss
            predictions = audio_output.to('cpu').detach()
            A_predictions.append(predictions)
            loss_meter.update(loss.item(), B)
            if args.use_drloc:
                loss_sup_meter.update(loss_sup.item(), B)
                loss_ssup_meter.update(loss_ssup.item(), B)
            batch_time.update(time.time() - end_time)
            per_sample_time.update((time.time() - end_time)/audio_input.shape[0])
            per_sample_dnn_time.update((time.time() - dnn_start_time)/audio_input.shape[0])

            print_step = global_step % args.n_print_steps == 0
            early_print_step = epoch == 0 and global_step % (args.n_print_steps/10) == 0
            print_step = print_step or early_print_step

            if print_step and global_step != 0:
                print('Epoch: [{0}][{1}/{2}]\t'
                  'Per Sample Total Time {per_sample_time.avg:.5f}\t'
                  'Per Sample Data Time {per_sample_data_time.avg:.5f}\t'
                  'Per Sample DNN Time {per_sample_dnn_time.avg:.5f}\t'
                  'Train Loss {loss_meter.val:.4f}\t'.format(
                   epoch, i, len(train_loader), per_sample_time=per_sample_time, per_sample_data_time=per_sample_data_time,
                      per_sample_dnn_time=per_sample_dnn_time, loss_meter=loss_meter), flush=True)
                if np.isnan(loss_meter.avg):
                    print("training diverged...")
                    return

            end_time = time.time()
            global_step += 1

        audio_output = torch.cat(A_predictions)
        target = torch.cat(A_targets)
        stats = calculate_stats(audio_output, target)

        cum_stats = stats

        mAP = np.mean([stat['AP'] for stat in stats])
        mAUC = np.mean([stat['auc'] for stat in stats])
        acc = np.mean([stat['acc'] for stat in stats])

        middle_ps = [stat['precisions'][int(len(stat['precisions'])/2)] for stat in stats]
        middle_rs = [stat['recalls'][int(len(stat['recalls'])/2)] for stat in stats]
        average_precision = np.mean(middle_ps)
        average_recall = np.mean(middle_rs)

        wandb.log({
            "train/loss": loss_meter.avg,
            "train/loss_sup": loss_sup_meter.avg,
            "train/loss_ssup": loss_ssup_meter.avg,
            "train/batch_time": batch_time.avg,
            "train/per_sample_time": per_sample_time.avg,
            "train/per_sample_data_time": per_sample_data_time.avg,
            "train/accuracy": acc,
            "train/mAP": mAP,
            "train/AUC": mAUC,
            "train/precision": average_precision,
            "train/recall": average_recall,
            "train/d_prime": d_prime(mAUC),
            # "train/lr": scheduler.get_last_lr()
        })

        print('start validation')
        stats, valid_loss = validate(audio_model, test_loader, args, epoch)
        print('validation finished')

        cum_stats = stats

        cum_mAP = np.mean([stat['AP'] for stat in cum_stats])
        cum_mAUC = np.mean([stat['auc'] for stat in cum_stats])
        cum_acc = np.mean([stat['acc'] for stat in cum_stats])

        mAP = np.mean([stat['AP'] for stat in stats])
        mAUC = np.mean([stat['auc'] for stat in stats])
        acc = np.mean([stat['acc'] for stat in stats])

        middle_ps = [stat['precisions'][int(len(stat['precisions'])/2)] for stat in stats]
        middle_rs = [stat['recalls'][int(len(stat['recalls'])/2)] for stat in stats]
        average_precision = np.mean(middle_ps)
        average_recall = np.mean(middle_rs)

        print("Epoch {:d} Results".format(epoch))
        print("ACC: {:.6f}".format(acc))
        print("mAP: {:.6f}".format(mAP))
        print("AUC: {:.6f}".format(mAUC))
        print("Avg Precision: {:.6f}".format(average_precision))
        print("Avg Recall: {:.6f}".format(average_recall))
        print("d_prime: {:.6f}".format(d_prime(mAUC)))
        print("train_loss: {:.6f}".format(loss_meter.avg))
        print("train_loss_sup: {:.6f}".format(loss_sup_meter.avg))
        print("train_loss_ssup: {:.6f}".format(loss_ssup_meter.avg))
        print("valid_loss: {:.6f}".format(valid_loss))

        wandb.log({
            "valid/accuracy": acc,
            "valid/mAP": mAP,
            "valid/AUC": mAUC,
            "valid/precision": average_precision,
            "valid/recall": average_recall,
            "valid/d_prime": d_prime(mAUC),
            "valid/loss": valid_loss,
        })

        result[epoch-1, :] = [mAP, acc, average_precision, average_recall, d_prime(mAUC), loss_meter.avg, valid_loss, cum_mAP, cum_acc, optimizer.param_groups[0]['lr']]

        np.savetxt(exp_dir + '/result.csv', result, delimiter=',')

        if acc > best_acc:
            best_acc = acc
            best_acc_epoch = epoch
            torch.save(audio_model.state_dict(), "%s/models/best_audio_model.pth" % (exp_dir))
        wandb.log({'valid/best_accuracy': best_acc})

        if cum_mAP > best_cum_mAP:
            best_cum_epoch = epoch
            best_cum_mAP = cum_mAP

        if args.save_model == True:
            torch.save(audio_model.state_dict(), "%s/models/audio_model.%d.pth" % (exp_dir, epoch))

        # scheduler.step()

        #print('number of params groups:' + str(len(optimizer.param_groups)))
        print('Epoch-{0} lr: {1}'.format(epoch, optimizer.param_groups[0]['lr']))

        with open(exp_dir + '/stats_' + str(epoch) +'.pickle', 'wb') as handle:
            pickle.dump(stats, handle, protocol=pickle.HIGHEST_PROTOCOL)
        _save_progress()

        finish_time = time.time()
        print('epoch {:d} training time: {:.3f}'.format(epoch, finish_time-begin_time))
        wandb.log({"train/epoch_time": finish_time-begin_time})

        epoch += 1

        batch_time.reset()
        per_sample_time.reset()
        data_time.reset()
        per_sample_data_time.reset()
        loss_meter.reset()
        loss_sup_meter.reset()
        loss_ssup_meter.reset()
        per_sample_dnn_time.reset()

def validate(audio_model, val_loader, args, epoch, extract_embedding=False):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    batch_time = AverageMeter()
    if not isinstance(audio_model, nn.DataParallel):
        audio_model = nn.DataParallel(audio_model)
    audio_model = audio_model.to(device)
    audio_model.eval()

    end = time.time()
    A_predictions = []
    A_targets = []
    A_loss = []
    A_loss_ssup = []
    attn_hm = []
    embeddings = []
    if args.dataset == 'CRS':
        if not os.path.exists(args.exp_dir+'/embeddings'):
            os.mkdir(args.exp_dir+'/embeddings')
    with torch.no_grad():
    # with torch.enable_grad():
        for i, (input_data, labels) in enumerate(val_loader):
            # compute output
            if extract_embedding:
                # print(labels)
                # if args.dataset == 'CRS':
                if True:
                    audio_output, curr_emb = audio_model(input_data, extract_embedding=True)
                    embeddings.append(curr_emb.detach().cpu().numpy())
                    colors = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'magenta', 'cyan', 'black']
                    classes = {int(v): k for k,v in val_loader.dataset.index_dict.items()}
                    data = val_loader.dataset.data
                    import matplotlib.pyplot as plt
                    import matplotlib.patches as patches
                    # for j in range(audio_output.shape[0]):
                    #     fig, axs = plt.subplots(2, 1, figsize=(40, 16))
                    #     image = input_data[j].detach().cpu().squeeze().transpose(0,1).numpy()
                    #     # Display the image
                    #     axs[0].imshow(image)
                    #     # lines = [[]]*len(colors)
                    #     for l in range(len(colors)):
                    #         line = []
                    #         for k in range(curr_emb[j].shape[-1]):
                    #             line.extend([float(curr_emb[j][l][k].detach().cpu().numpy())]*200)
                    #         axs[1].plot(line, color=colors[l], label=classes[l])
                    #     axs[1].set_title(f'file: {data[i*audio_output.shape[0] + j]["wav"]}, ground truth: {[classes[int(num)] for num in torch.where(labels[j]==1)[0]]},\n final predict: {audio_output[j]}')
                    #     axs[1].legend()
                    #     plt.tight_layout()
                    #     plt.savefig(f'imgs/{i*audio_output.shape[0] + j}.png')
                else:
                    audio_output, curr_attn_hm = audio_model(input_data, extract_embedding=True)
                    curr_attn_hm = np.array([curr_attn.detach().cpu().numpy() for curr_attn in curr_attn_hm])
                    attn_hm.append(curr_attn_hm)
            elif args.use_drloc:
                audio_output, outs = audio_model(input_data)
                loss_ssup, ssup_items = cal_selfsupervised_loss(outs, args, args.lambda_drloc)
                A_loss_ssup.append(loss_ssup.to('cpu').detach())
            elif args.multi_task:
                audio_output, second_task_audio_output, third_task_audio_output = audio_model(input_data)
            else:
                # import pdb; pdb.set_trace()
                audio_output = audio_model(input_data)
            predictions = audio_output.to('cpu').detach()

            A_predictions.append(predictions)
            if args.multi_task:
                labels, second_task_labels, third_task_labels = labels
                second_task_labels = second_task_labels.to(device)
                third_task_labels = third_task_labels.to(device)
            A_targets.append(labels)
            # compute the loss
            labels = labels.to(device)
            # loss without reduction, easy to check per-sample loss
            if args.multi_label: 
                loss_fn = nn.BCELoss()
                if args.model == 'AST_pretrained': labels = labels.to(torch.float16)
                loss = loss_fn(audio_output, labels)
            else:
                loss_fn = nn.CrossEntropyLoss()
                loss = loss_fn(audio_output, torch.argmax(labels.long(), axis=1))

            if args.multi_task:
                num_label = second_task_labels.shape[-1]
                second_task_labels = second_task_labels.unsqueeze(1).repeat(1, int(second_task_audio_output.shape[0]/second_task_labels.shape[0]), 1).reshape(-1,num_label)
                loss_fn = nn.CrossEntropyLoss()
                second_task_loss = loss_fn(second_task_audio_output, torch.argmax(second_task_labels.long(), axis=1))

                num_label = third_task_labels.shape[-1]
                third_task_labels = third_task_labels.unsqueeze(1).repeat(1, int(third_task_audio_output.shape[0]/third_task_labels.shape[0]), 1).reshape(-1,num_label)
                loss_fn = nn.CrossEntropyLoss()
                third_task_loss = loss_fn(third_task_audio_output, torch.argmax(third_task_labels.long(), axis=1))
                loss = loss + args.second_task_lambda * (second_task_loss + third_task_loss)

            A_loss.append(loss.to('cpu').detach())

            batch_time.update(time.time() - end)
            end = time.time()

        audio_output = torch.cat(A_predictions)
        target = torch.cat(A_targets)
        loss = np.mean(A_loss)
        stats = calculate_stats(audio_output, target)

        loss_ssup = np.mean(A_loss_ssup)
        # wandb.log({
        #     "valid/loss_ssup": loss_ssup,
        # })
        print("valid_loss_ssup: {:.6f}".format(loss_ssup))

        # save the prediction here
        exp_dir = args.exp_dir
        if os.path.exists(exp_dir+'/predictions') == False:
            os.mkdir(exp_dir+'/predictions')
            np.savetxt(exp_dir+'/predictions/target.csv', target, delimiter=',')
        np.savetxt(exp_dir+'/predictions/predictions_' + str(epoch) + '.csv', audio_output, delimiter=',')

        if extract_embedding:
            # if args.dataset == 'CRS':
            if True:
                embeddings = np.concatenate(embeddings, axis=0)
                data = val_loader.dataset.data
                np.save(f'{exp_dir}/{epoch}.npy', embeddings)
                assert len(data) == embeddings.shape[0], (embeddings.shape[0], len(data))

                for i in range(len(data)):
                    # data[i]['embedding'] =  f'embeddings/{epoch}_{i}.npy'
                    # data[i]['start_time'] = data[i]['index']*10
                    # data[i]['end_time'] = (data[i]['index']+1)*10
                    # np.save(f'{exp_dir}/embeddings/{epoch}_{i}.npy', embeddings[i])
                    data[i]['embedding_index'] = i

                # Create the final JSON structure
                json_data = {
                    "data": data
                }

                import json
                with open(f'{args.exp_dir}/{epoch}.json', 'w') as json_file:
                    json.dump(json_data, json_file, indent=2)
                pass
            else:
                attn_hm = np.concatenate(attn_hm, axis=1)
                np.save(f'{exp_dir}/attn_hm_{epoch}.npy', attn_hm)
                np.save(f'{exp_dir}/labels_{epoch}.npy', target.to('cpu').detach().numpy())

    return stats, loss
