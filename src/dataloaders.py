import csv
import json
import torchaudio
import numpy as np
import scipy.signal
import torch
import torch.nn.functional
from torch.utils.data import Dataset
# from torch_geometric.data import Data
#from torch_geometric.nn import GCNConv, SAGEConv, GATConv, TransformerConv
#import torch_geometric.nn as pyg_nn
#from torch_geometric.loader import DataLoader
import random
import os
import librosa
from tqdm import tqdm

def make_index_dict(label_csv):
    index_lookup = {}
    with open(label_csv, 'r') as f:
        csv_reader = csv.DictReader(f)
        line_count = 0
        for row in csv_reader:
            index_lookup[row['mid']] = row['index']
            line_count += 1
    return index_lookup

def make_name_dict(label_csv):
    name_lookup = {}
    with open(label_csv, 'r') as f:
        csv_reader = csv.DictReader(f)
        line_count = 0
        for row in csv_reader:
            name_lookup[row['index']] = row['display_name']
            line_count += 1
    return name_lookup

def lookup_list(index_list, label_csv):
    label_list = []
    table = make_name_dict(label_csv)
    for item in index_list:
        label_list.append(table[item])
    return label_list

class VSDataset(Dataset):
    def __init__(self, dataset_json_file, label_csv=None, audio_conf=None, raw_wav_mode=False, specaug=False, dataset='VocalSound'):
        self.datapath = dataset_json_file
        with open(dataset_json_file, 'r') as fp:
            data_json = json.load(fp)
        self.data = data_json['data']
        if dataset == 'CRS':
            new_data = list()
            for i in range(len(self.data)):
                for j in range(6): # split size: 10 seconds
                    dct = self.data[i].copy()
                    dct['index'] = j
                    new_data.append(dct)
                # self.data[i]['wav'] = self.data[i]['wav'][self.data[i]['wav'].find('data_16k'):]
                # self.data[i]['wav'] = os.path.join('../data', self.data[i]['wav'].replace('data_16k', 'audio_16k'))
            self.data = new_data
        self.audio_conf = audio_conf
        self.mode = self.audio_conf.get('mode')
        self.melbins = self.audio_conf.get('num_mel_bins')
        self.index_dict = make_index_dict(label_csv)
        self.label_num = len(self.index_dict)
        #print('Number of classes is {:d}'.format(self.label_num))

        self.windows = {'hamming': scipy.signal.hamming, 'hann': scipy.signal.hann, 'blackman': scipy.signal.blackman, 'bartlett': scipy.signal.bartlett}

        # if just load raw wavform
        self.raw_wav_mode = raw_wav_mode
        if specaug == True:
            self.freqm = self.audio_conf.get('freqm')
            self.timem = self.audio_conf.get('timem')
            #print('now using following mask: {:d} freq, {:d} time'.format(self.audio_conf.get('freqm'), self.audio_conf.get('timem')))
        self.specaug = specaug
        self.mixup = self.audio_conf.get('mixup')
        #print('now using mix-up with rate {:f}'.format(self.mixup))
        #print('now add rolling and new mixup stategy')
        self.dataset = dataset

    def _wav2fbank(self, filename, filename2=None, index=None):
        # not mix-up
        if filename2 == None:
            waveform, sr = torchaudio.load(filename)
            if self.dataset == 'CRS':
                waveform = waveform[:, sr*index*10: sr*(index+1)*10]
            waveform = waveform - waveform.mean()
        # use for mix-up
        else:
            waveform1, sr = torchaudio.load(filename)
            waveform2, _ = torchaudio.load(filename2)

            waveform1 = waveform1 - waveform1.mean()
            waveform2 = waveform2 - waveform2.mean()

            if waveform1.shape[1] != waveform2.shape[1]:
                if waveform1.shape[1] > waveform2.shape[1]:
                    # do a padding
                    temp_wav = torch.zeros(1, waveform1.shape[1])
                    temp_wav[0, 0:waveform2.shape[1]] = waveform2
                    waveform2 = temp_wav
                else:
                    # do cutting
                    waveform2 = waveform2[0, 0:waveform1.shape[1]]

            mix_lambda = np.random.beta(10, 10)

            mix_waveform = mix_lambda * waveform1 + (1 - mix_lambda) * waveform2
            waveform = mix_waveform - mix_waveform.mean()

        fbank = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=self.melbins, dither=0.0, frame_shift=10)

        target_length = self.audio_conf.get('target_length', 1056)
        n_frames = fbank.shape[0]

        p = target_length - n_frames

        # cut and pad
        if p > 0:
            m = torch.nn.ZeroPad2d((0, 0, 0, p))
            fbank = m(fbank)
        elif p < 0:
            start = 0
            # if self.dataset == 'CRS':
            #     start = np.random.randint(0, -p)
            fbank = fbank[start:start+target_length, :]

        if filename2 == None:
            return fbank
        else:
            return fbank, mix_lambda

    def __getitem__(self, index):

        if random.random() < self.mixup:
            datum = self.data[index]
            mix_sample_idx = random.randint(0, len(self.data)-1)
            mix_datum = self.data[mix_sample_idx]

            # get the mixed fbank
            fbank, mix_lambda = self._wav2fbank(datum['wav'], mix_datum['wav'])
            # initialize the label
            label_indices = np.zeros(self.label_num)
            # add labels for the original sample
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += mix_lambda
            # add more labels to the original sample labels
            for label_str in mix_datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += 1-mix_lambda
            label_indices = torch.FloatTensor(label_indices)
        # not doing mixup
        else:
            # import pdb
            # pdb.set_trace()
            datum = self.data[index]
            label_indices = np.zeros(self.label_num) + 0.00
            fbank = self._wav2fbank(datum['wav'], index=datum['index'] if self.dataset == 'CRS' else None)
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)
            mix_lambda = 0

        # SpecAug, not do for eval set, by intention, it is done after the mix-up step
        if self.specaug == True:
            freqm = torchaudio.transforms.FrequencyMasking(self.freqm)
            timem = torchaudio.transforms.TimeMasking(self.timem)
            fbank = torch.transpose(fbank, 0, 1)
            fbank = fbank.unsqueeze(0)
            fbank = freqm(fbank)
            fbank = timem(fbank)
            fbank = fbank.squeeze(0)
            fbank = torch.transpose(fbank, 0, 1)

        # # mean/std is get from the val set as a prior.
        # fbank = (fbank + 3.05) / 5.42

        # shift if in the training set, training set typically use mixup
        if self.mode == 'train':
            fbank = torch.roll(fbank, np.random.randint(0, 24), 0)
        mix_ratio = min(mix_lambda, 1-mix_lambda) / max(mix_lambda, 1-mix_lambda)
        return fbank, label_indices

    def __len__(self):
        return len(self.data)


class VSDataset_multi_task(Dataset):
    def __init__(self, dataset_json_file, label_csv=None, audio_conf=None, raw_wav_mode=False, specaug=False, dataset='VocalSound'):
        self.datapath = dataset_json_file
        with open(dataset_json_file, 'r') as fp:
            data_json = json.load(fp)
        self.data = data_json['data']
        if dataset == 'CRS':
            new_data = list()
            for i in range(len(self.data)):
                for j in range(6): # split size: 10 seconds
                    dct = self.data[i].copy()
                    dct['index'] = j
                    new_data.append(dct)
                # self.data[i]['wav'] = self.data[i]['wav'][self.data[i]['wav'].find('data_16k'):]
                # self.data[i]['wav'] = os.path.join('../data', self.data[i]['wav'].replace('data_16k', 'audio_16k'))
            self.data = new_data
        self.audio_conf = audio_conf
        self.mode = self.audio_conf.get('mode')
        self.melbins = self.audio_conf.get('num_mel_bins')
        self.index_dict = make_index_dict(label_csv)
        self.label_num = len(self.index_dict)
        # if 'fish_multilabel_multitask_health_metadata' in label_csv:
        second_label_csv = '../data_CRS/reef_state_metadata/class_labels_indices_crs.csv'
        # else:
        third_label_csv = '../data_CRS/location_metadata/class_labels_indices_crs.csv'
        self.second_task_index_dict = make_index_dict(second_label_csv)
        self.second_task_label_num = len(self.second_task_index_dict)
        self.third_task_index_dict = make_index_dict(third_label_csv)
        self.third_task_label_num = len(self.third_task_index_dict)
        #print('Number of classes is {:d}'.format(self.label_num))

        self.windows = {'hamming': scipy.signal.hamming, 'hann': scipy.signal.hann, 'blackman': scipy.signal.blackman, 'bartlett': scipy.signal.bartlett}

        # if just load raw wavform
        self.raw_wav_mode = raw_wav_mode
        if specaug == True:
            self.freqm = self.audio_conf.get('freqm')
            self.timem = self.audio_conf.get('timem')
            #print('now using following mask: {:d} freq, {:d} time'.format(self.audio_conf.get('freqm'), self.audio_conf.get('timem')))
        self.specaug = specaug
        self.mixup = self.audio_conf.get('mixup')
        #print('now using mix-up with rate {:f}'.format(self.mixup))
        #print('now add rolling and new mixup stategy')
        self.dataset = dataset

    def _wav2fbank(self, filename, filename2=None, index=None):
        # not mix-up
        if filename2 == None:
            waveform, sr = torchaudio.load(filename)
            if self.dataset == 'CRS':
                waveform = waveform[:, sr*index*10: sr*(index+1)*10]
            waveform = waveform - waveform.mean()
        # use for mix-up
        else:
            waveform1, sr = torchaudio.load(filename)
            waveform2, _ = torchaudio.load(filename2)

            waveform1 = waveform1 - waveform1.mean()
            waveform2 = waveform2 - waveform2.mean()

            if waveform1.shape[1] != waveform2.shape[1]:
                if waveform1.shape[1] > waveform2.shape[1]:
                    # do a padding
                    temp_wav = torch.zeros(1, waveform1.shape[1])
                    temp_wav[0, 0:waveform2.shape[1]] = waveform2
                    waveform2 = temp_wav
                else:
                    # do cutting
                    waveform2 = waveform2[0, 0:waveform1.shape[1]]

            mix_lambda = np.random.beta(10, 10)

            mix_waveform = mix_lambda * waveform1 + (1 - mix_lambda) * waveform2
            waveform = mix_waveform - mix_waveform.mean()

        fbank = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=self.melbins, dither=0.0, frame_shift=10)

        target_length = self.audio_conf.get('target_length', 1056)
        n_frames = fbank.shape[0]

        p = target_length - n_frames

        # cut and pad
        if p > 0:
            m = torch.nn.ZeroPad2d((0, 0, 0, p))
            fbank = m(fbank)
        elif p < 0:
            start = 0
            # if self.dataset == 'CRS':
            #     start = np.random.randint(0, -p)
            fbank = fbank[start:start+target_length, :]

        if filename2 == None:
            return fbank
        else:
            return fbank, mix_lambda

    def __getitem__(self, index):

        if random.random() < self.mixup:
            datum = self.data[index]
            mix_sample_idx = random.randint(0, len(self.data)-1)
            mix_datum = self.data[mix_sample_idx]

            # get the mixed fbank
            fbank, mix_lambda = self._wav2fbank(datum['wav'], mix_datum['wav'])
            # initialize the label
            label_indices = np.zeros(self.label_num)
            # add labels for the original sample
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += mix_lambda
            # add more labels to the original sample labels
            for label_str in mix_datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += 1-mix_lambda
            label_indices = torch.FloatTensor(label_indices)
        # not doing mixup
        else:
            datum = self.data[index]
            label_indices = np.zeros(self.label_num) + 0.00
            second_task_label_indices = np.zeros(self.second_task_label_num) + 0.00
            third_task_label_indices = np.zeros(self.third_task_label_num) + 0.00
            fbank = self._wav2fbank(datum['wav'], index=datum['index'] if self.dataset == 'CRS' else None)
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] = 1.0
            for label_str in datum['second_task_labels'].split(','):
                second_task_label_indices[int(self.second_task_index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)
            second_task_label_indices = torch.FloatTensor(second_task_label_indices)

            for label_str in datum['third_task_labels'].split(','):
                third_task_label_indices[int(self.third_task_index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)
            third_task_label_indices = torch.FloatTensor(third_task_label_indices)
            mix_lambda = 0

        # SpecAug, not do for eval set, by intention, it is done after the mix-up step
        if self.specaug == True:
            freqm = torchaudio.transforms.FrequencyMasking(self.freqm)
            timem = torchaudio.transforms.TimeMasking(self.timem)
            fbank = torch.transpose(fbank, 0, 1)
            fbank = fbank.unsqueeze(0)
            fbank = freqm(fbank)
            fbank = timem(fbank)
            fbank = fbank.squeeze(0)
            fbank = torch.transpose(fbank, 0, 1)

        # shift if in the training set, training set typically use mixup
        if self.mode == 'train':
            fbank = torch.roll(fbank, np.random.randint(0, 24), 0)
        mix_ratio = min(mix_lambda, 1-mix_lambda) / max(mix_lambda, 1-mix_lambda)
        return fbank, (label_indices, second_task_label_indices, third_task_label_indices)

    def __len__(self):
        return len(self.data)


class VSDataset_multi_res(VSDataset):
    def __init__(self, dataset_json_file, label_csv=None, audio_conf=None, raw_wav_mode=False, specaug=False, dataset='VocalSound'):
        super(VSDataset_multi_res, self).__init__(dataset_json_file, label_csv, audio_conf, raw_wav_mode, specaug, dataset)

    def _wav2fbank(self, filename, filename2=None):
        # not mix-up
        if filename2 == None:
            waveform, sr = torchaudio.load(filename)
            waveform = waveform - waveform.mean()
        # use for mix-up
        else:
            waveform1, sr = torchaudio.load(filename)
            waveform2, _ = torchaudio.load(filename2)

            waveform1 = waveform1 - waveform1.mean()
            waveform2 = waveform2 - waveform2.mean()

            if waveform1.shape[1] != waveform2.shape[1]:
                if waveform1.shape[1] > waveform2.shape[1]:
                    # do a padding
                    temp_wav = torch.zeros(1, waveform1.shape[1])
                    temp_wav[0, 0:waveform2.shape[1]] = waveform2
                    waveform2 = temp_wav
                else:
                    # do cutting
                    waveform2 = waveform2[0, 0:waveform1.shape[1]]

            mix_lambda = np.random.beta(10, 10)

            mix_waveform = mix_lambda * waveform1 + (1 - mix_lambda) * waveform2
            waveform = mix_waveform - mix_waveform.mean()

        fbank = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=self.melbins, dither=0.0, frame_shift=10, frame_length=25)

        fbank_high_level = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=4, dither=0.0, frame_shift=10*self.audio_conf.get('temporal_stride'), frame_length=10*self.audio_conf.get('temporal_patch_size')+25)
        fbank_high_level = torch.mean(fbank_high_level, dim=1).unsqueeze(1)

        target_length = self.audio_conf.get('target_length', 1056)
        n_frames = fbank.shape[0]

        p = target_length - n_frames
        from math import floor
        high_level_target_length = floor(target_length*10 - (10*self.audio_conf.get('temporal_patch_size')+25))/(10*self.audio_conf.get('temporal_stride'))

        # cut and pad
        if p >= 0:
            # print("Short")
            # print(fbank.shape, fbank_high_level.shape)
            m = torch.nn.ZeroPad2d((0, 0, 0, p))
            fbank = m(fbank)
            p_high_level = 101-fbank_high_level.shape[0]
            m = torch.nn.ZeroPad2d((0, 0, 0, p_high_level))
            fbank_high_level = m(fbank_high_level)
            # if fbank_high_level.shape[0] != 26:
            # print(waveform.shape, fbank.shape, fbank.dtype, fbank_high_level.shape, fbank_high_level.dtype)
        elif p < 0:
            # print("Long")
            # print(fbank.shape, fbank_high_level.shape)
            fbank = fbank[0:target_length, :]
            fbank_high_level = fbank_high_level[:101, :]
            # if fbank_high_level.shape[0] != 26:
            # print(waveform.shape, fbank.shape, fbank.dtype, fbank_high_level.shape, fbank_high_level.dtype)

        if filename2 == None:
            return fbank, fbank_high_level
        else:
            return fbank, mix_lambda

    def __getitem__(self, index):
        if random.random() < self.mixup:
            datum = self.data[index]
            mix_sample_idx = random.randint(0, len(self.data)-1)
            mix_datum = self.data[mix_sample_idx]

            # get the mixed fbank
            fbank, mix_lambda = self._wav2fbank(datum['wav'], mix_datum['wav'])
            # initialize the label
            label_indices = np.zeros(self.label_num)
            # add labels for the original sample
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += mix_lambda
            # add more labels to the original sample labels
            for label_str in mix_datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += 1-mix_lambda
            label_indices = torch.FloatTensor(label_indices)
        # not doing mixup
        else:
            datum = self.data[index]
            label_indices = np.zeros(self.label_num) + 0.00
            fbank, fbank_high_level = self._wav2fbank(datum['wav'])
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)
            mix_lambda = 0

        # SpecAug, not do for eval set, by intention, it is done after the mix-up step
        # if self.specaug == True:
        #     freqm = torchaudio.transforms.FrequencyMasking(self.freqm)
        #     timem = torchaudio.transforms.TimeMasking(self.timem)
        #     fbank = torch.transpose(fbank, 0, 1)
        #     fbank = fbank.unsqueeze(0)
        #     fbank = freqm(fbank)
        #     fbank = timem(fbank)
        #     fbank = fbank.squeeze(0)
        #     fbank = torch.transpose(fbank, 0, 1)

        # shift if in the training set, training set typically use mixup
        # if self.mode == 'train':
        #     fbank = torch.roll(fbank, np.random.randint(0, 24), 0)
        # print(fbank.shape)
        # print(fbank_high_level.shape)
        # assert fbank_high_level.shape == torch.Size([26, 4]), (self.data[index], fbank_high_level.shape)
        # assert fbank.shape == torch.Size([416, 128]), '12323123sad'
        return (fbank, fbank_high_level), label_indices


class VSDataset_w2g(Dataset):
    def __init__(self, dataset_json_file, label_csv=None, audio_conf=None, raw_wav_mode=False, specaug=False):
        self.datapath = dataset_json_file
        with open(dataset_json_file, 'r') as fp:
            data_json = json.load(fp)
        self.data = data_json['data']
        for i in range(len(self.data)):
            self.data[i]['wav'] = self.data[i]['wav'][self.data[i]['wav'].find('data_16k'):]
            self.data[i]['wav'] = os.path.join('../data', self.data[i]['wav'].replace('data_16k', 'audio_16k'))

        self.audio_conf = audio_conf
        self.mode = self.audio_conf.get('mode')
        self.melbins = self.audio_conf.get('num_mel_bins')
        self.index_dict = make_index_dict(label_csv)
        self.label_num = len(self.index_dict)
        #print('Number of classes is {:d}'.format(self.label_num))

        self.windows = {'hamming': scipy.signal.hamming, 'hann': scipy.signal.hann, 'blackman': scipy.signal.blackman, 'bartlett': scipy.signal.bartlett}

        # if just load raw wavform
        self.raw_wav_mode = raw_wav_mode
        if specaug == True:
            self.freqm = self.audio_conf.get('freqm')
            self.timem = self.audio_conf.get('timem')
            #print('now using following mask: {:d} freq, {:d} time'.format(self.audio_conf.get('freqm'), self.audio_conf.get('timem')))
        self.specaug = specaug
        self.mixup = self.audio_conf.get('mixup')
        #print('now using mix-up with rate {:f}'.format(self.mixup))
        #print('now add rolling and new mixup stategy')

        mean_path = os.path.join(self.data[0]['wav'][:self.data[0]['wav'].rfind('/')].replace('audio_16k', '2048window'), f'mean_{self.mode}.npy')
        std_path = os.path.join(self.data[0]['wav'][:self.data[0]['wav'].rfind('/')].replace('audio_16k', '2048window'), f'std_{self.mode}.npy')
        mean = np.load(mean_path)
        std = np.load(std_path)

        self.mfccs = []
        self.corrs = []

        for datum in self.data:
            mfcc = np.load(datum['wav'][:-4].replace('audio_16k', '2048window')+'_mfcc.npy')
            corr = np.load(datum['wav'][:-4].replace('audio_16k', '2048window')+'_corr.npy')

            diff = mfcc.shape[-1] - 800
            if diff==0:
                mfcc = mfcc
            elif diff < 0:
                mfcc = np.concatenate([mfcc, np.zeros((39, -diff))], axis=1)
            else:
                start = np.random.randint(0, diff+1)
                end = mfcc.shape[-1] - diff + start
                mfcc = mfcc[:, start:end]

            mfcc = (mfcc - mean)/std
            self.mfccs.append(mfcc)
            self.corrs.append(corr)
        self.mfccs = np.array(self.mfccs)
        self.corrs = np.array(self.corrs)

    def _wav2fbank(self, filename, filename2=None):
        # not mix-up
        if filename2 == None:
            waveform, sr = torchaudio.load(filename)
            waveform = waveform - waveform.mean()
        # use for mix-up
        else:
            waveform1, sr = torchaudio.load(filename)
            waveform2, _ = torchaudio.load(filename2)

            waveform1 = waveform1 - waveform1.mean()
            waveform2 = waveform2 - waveform2.mean()

            if waveform1.shape[1] != waveform2.shape[1]:
                if waveform1.shape[1] > waveform2.shape[1]:
                    # do a padding
                    temp_wav = torch.zeros(1, waveform1.shape[1])
                    temp_wav[0, 0:waveform2.shape[1]] = waveform2
                    waveform2 = temp_wav
                else:
                    # do cutting
                    waveform2 = waveform2[0, 0:waveform1.shape[1]]

            mix_lambda = np.random.beta(10, 10)

            mix_waveform = mix_lambda * waveform1 + (1 - mix_lambda) * waveform2
            waveform = mix_waveform - mix_waveform.mean()

        fbank = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=self.melbins, dither=0.0, frame_shift=10)

        target_length = self.audio_conf.get('target_length', 1056)
        n_frames = fbank.shape[0]

        p = target_length - n_frames

        # cut and pad
        if p > 0:
            m = torch.nn.ZeroPad2d((0, 0, 0, p))
            fbank = m(fbank)
        elif p < 0:
            fbank = fbank[0:target_length, :]

        if filename2 == None:
            return fbank
        else:
            return fbank, mix_lambda

    def __getitem__(self, index):
        if random.random() < self.mixup:
            datum = self.data[index]
            mix_sample_idx = random.randint(0, len(self.data)-1)
            mix_datum = self.data[mix_sample_idx]

            # get the mixed fbank
            fbank, mix_lambda = self._wav2fbank(datum['wav'], mix_datum['wav'])
            # initialize the label
            label_indices = np.zeros(self.label_num)
            # add labels for the original sample
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += mix_lambda
            # add more labels to the original sample labels
            for label_str in mix_datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += 1-mix_lambda
            label_indices = torch.FloatTensor(label_indices)
        # not doing mixup
        else:
            datum = self.data[index]
            label_indices = np.zeros(self.label_num) + 0.00
            fbank = self._wav2fbank(datum['wav'])
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)
            mix_lambda = 0

        # SpecAug, not do for eval set, by intention, it is done after the mix-up step
        if self.specaug == True:
            freqm = torchaudio.transforms.FrequencyMasking(self.freqm)
            timem = torchaudio.transforms.TimeMasking(self.timem)
            fbank = torch.transpose(fbank, 0, 1)
            fbank = fbank.unsqueeze(0)
            fbank = freqm(fbank)
            fbank = timem(fbank)
            fbank = fbank.squeeze(0)
            fbank = torch.transpose(fbank, 0, 1)

        # mean/std is get from the val set as a prior.
        # fbank = (fbank + 3.05) / 5.42

        # shift if in the training set, training set typically use mixup
        if self.mode == 'train':
            fbank = torch.roll(fbank, np.random.randint(0, 1024), 0)
        mix_ratio = min(mix_lambda, 1-mix_lambda) / max(mix_lambda, 1-mix_lambda)

        return (fbank, self.mfccs[index], self.corrs[index]), label_indices

    def __len__(self):
        return len(self.data)


class VSDataset_temp_w2g(Dataset):
    def __init__(self, dataset_json_file, label_csv=None, audio_conf=None, raw_wav_mode=False, specaug=False):
        self.datapath = dataset_json_file
        with open(dataset_json_file, 'r') as fp:
            data_json = json.load(fp)
        self.data = data_json['data']
        # for i in range(len(self.data)):
        #     self.data[i]['wav'] = self.data[i]['wav'][self.data[i]['wav'].find('data_16k'):]
        #     self.data[i]['wav'] = os.path.join('../data', self.data[i]['wav'].replace('data_16k', 'audio_16k'))

        self.audio_conf = audio_conf
        self.mode = self.audio_conf.get('mode')
        self.melbins = self.audio_conf.get('num_mel_bins')
        self.index_dict = make_index_dict(label_csv)
        self.label_num = len(self.index_dict)
        #print('Number of classes is {:d}'.format(self.label_num))

        self.windows = {'hamming': scipy.signal.hamming, 'hann': scipy.signal.hann, 'blackman': scipy.signal.blackman, 'bartlett': scipy.signal.bartlett}

        # if just load raw wavform
        self.raw_wav_mode = raw_wav_mode
        if specaug == True:
            self.freqm = self.audio_conf.get('freqm')
            self.timem = self.audio_conf.get('timem')
            #print('now using following mask: {:d} freq, {:d} time'.format(self.audio_conf.get('freqm'), self.audio_conf.get('timem')))
        self.specaug = specaug
        self.mixup = self.audio_conf.get('mixup')
        #print('now using mix-up with rate {:f}'.format(self.mixup))
        #print('now add rolling and new mixup stategy')

        mean_path = os.path.join(self.data[0]['wav'][:self.data[0]['wav'].rfind('/')].replace('audio_16k', '2048w_temporal'), f'mean_{self.mode}.npy')
        std_path = os.path.join(self.data[0]['wav'][:self.data[0]['wav'].rfind('/')].replace('audio_16k', '2048w_temporal'), f'std_{self.mode}.npy')
        self.mean = np.load(mean_path)
        self.std = np.load(std_path)

        # self.mfccs = []
        # self.corrs = []

        # for datum in self.data:
        #     mfcc = np.load(datum['wav'][:-4].replace('audio_16k', '2048window')+'_mfcc.npy')
        #     corr = np.load(datum['wav'][:-4].replace('audio_16k', '2048window')+'_corr.npy')

        #     diff = mfcc.shape[-1] - 800
        #     if diff==0:
        #         mfcc = mfcc
        #     elif diff < 0:
        #         mfcc = np.concatenate([mfcc, np.zeros((39, -diff))], axis=1)
        #     else:
        #         start = np.random.randint(0, diff+1)
        #         end = mfcc.shape[-1] - diff + start
        #         mfcc = mfcc[:, start:end]

        #     mfcc = (mfcc - mean)/std
        #     self.mfccs.append(mfcc)
        #     self.corrs.append(corr)
        # self.mfccs = np.array(self.mfccs)
        # self.corrs = np.array(self.corrs)

    def _wav2fbank(self, filename, filename2=None):
        # not mix-up
        if filename2 == None:
            waveform, sr = torchaudio.load(filename)
            waveform = waveform - waveform.mean()
        # use for mix-up
        else:
            waveform1, sr = torchaudio.load(filename)
            waveform2, _ = torchaudio.load(filename2)

            waveform1 = waveform1 - waveform1.mean()
            waveform2 = waveform2 - waveform2.mean()

            if waveform1.shape[1] != waveform2.shape[1]:
                if waveform1.shape[1] > waveform2.shape[1]:
                    # do a padding
                    temp_wav = torch.zeros(1, waveform1.shape[1])
                    temp_wav[0, 0:waveform2.shape[1]] = waveform2
                    waveform2 = temp_wav
                else:
                    # do cutting
                    waveform2 = waveform2[0, 0:waveform1.shape[1]]

            mix_lambda = np.random.beta(10, 10)

            mix_waveform = mix_lambda * waveform1 + (1 - mix_lambda) * waveform2
            waveform = mix_waveform - mix_waveform.mean()

        fbank = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=self.melbins, dither=0.0, frame_shift=10)

        target_length = self.audio_conf.get('target_length', 1056)
        n_frames = fbank.shape[0]

        p = target_length - n_frames

        # cut and pad
        if p > 0:
            m = torch.nn.ZeroPad2d((0, 0, 0, p))
            fbank = m(fbank)
        elif p < 0:
            fbank = fbank[0:target_length, :]

        if filename2 == None:
            return fbank
        else:
            return fbank, mix_lambda

    def __getitem__(self, index):
        if random.random() < self.mixup:
            datum = self.data[index]
            mix_sample_idx = random.randint(0, len(self.data)-1)
            mix_datum = self.data[mix_sample_idx]

            # get the mixed fbank
            fbank, mix_lambda = self._wav2fbank(datum['wav'], mix_datum['wav'])
            # initialize the label
            label_indices = np.zeros(self.label_num)
            # add labels for the original sample
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += mix_lambda
            # add more labels to the original sample labels
            for label_str in mix_datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += 1-mix_lambda
            label_indices = torch.FloatTensor(label_indices)
        # not doing mixup
        else:
            datum = self.data[index]
            label_indices = np.zeros(self.label_num) + 0.00
            fbank = self._wav2fbank(datum['wav'])
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)
            mix_lambda = 0

        # SpecAug, not do for eval set, by intention, it is done after the mix-up step
        if self.specaug == True:
            freqm = torchaudio.transforms.FrequencyMasking(self.freqm)
            timem = torchaudio.transforms.TimeMasking(self.timem)
            fbank = torch.transpose(fbank, 0, 1)
            fbank = fbank.unsqueeze(0)
            fbank = freqm(fbank)
            fbank = timem(fbank)
            fbank = fbank.squeeze(0)
            fbank = torch.transpose(fbank, 0, 1)

        # mean/std is get from the val set as a prior.
        fbank = (fbank + 3.05) / 5.42

        # shift if in the training set, training set typically use mixup
        if self.mode == 'train':
            fbank = torch.roll(fbank, np.random.randint(0, 1024), 0)
        mix_ratio = min(mix_lambda, 1-mix_lambda) / max(mix_lambda, 1-mix_lambda)

        y, _ = librosa.load(datum['wav'], sr = 16_000)
        y, _ = librosa.effects.trim(y, frame_length=2048, hop_length=84)
        mfccs = []
        corrs = []
        for i in range(max(int(y.shape[0]/8_000) - 1, 1)):
            try:
                mfcc = np.load(datum['wav'].replace('audio_16k', '2048w_temporal')[:-4] + f'_{i}_mfcc.npy')
                corr = np.load(datum['wav'].replace('audio_16k', '2048w_temporal')[:-4] + f'_{i}_corr.npy')
                diff = mfcc.shape[-1] - 96
                if diff==0:
                    mfcc = mfcc
                elif diff < 0:
                    mfcc = np.concatenate([mfcc, np.zeros((39, -diff))], axis=1)
                else:
                    start = np.random.randint(0, diff+1)
                    end = mfcc.shape[-1] - diff + start
                    mfcc = mfcc[:, start:end]

                mfcc = (mfcc - self.mean)/self.std

                mfccs.append(mfcc)
                corrs.append(corr)
            except Exception as _:
                print(i, y.shape, datum['wav'])
        mfccs = np.array(mfccs)
        corrs = np.array(corrs)
        if len(mfccs.shape)!= 3:
            print(datum['wav'])

        return (fbank, mfccs, corrs), label_indices

    def __len__(self):
        return len(self.data)


def collate_fn(data):
    """
       data: is a list of tuples with (example, label)
             where 'example' is a tensor of arbitrary shape
             and label are scalars
            examples is tuple of (mel, mfccs, corrs), each MFCC: num in 1 sample x 39 x 96
    """
    samples, labels = zip(*data)
    # print("DATA 0: ", data[0])
    # print("DATA 0 0: ", data[0][0])
    max_len = max([sample[1].shape[0] for sample in samples])
    # print(max_len)
    num_band = data[0][0][1].shape[1]
    num_timestep = data[0][0][1].shape[2]
    mfccs = torch.zeros((len(data), max_len, num_band, num_timestep))
    corrs = torch.zeros((len(data), max_len, num_band, num_band))
    padding_mask = torch.BoolTensor(len(data), max_len).fill_(False)
    labels = torch.stack(labels)

    for i in range(len(data)):
        # print(data[i][0][1].shape)
        j, k, l = data[i][0][1].shape[0], data[i][0][1].shape[1], data[i][0][1].shape[2]
        mfccs[i] = torch.cat([torch.Tensor(data[i][0][1]), torch.zeros((max_len - j, k, l))])
        corrs[i] = torch.cat([torch.Tensor(data[i][0][2]), torch.zeros((max_len - j, k, k))])

        if j != max_len:
            padding_mask[i, (j-max_len):] = True
            # print(data[i][0][1].shape)
            # print(max_len)

    # print(padding_mask[6])
    # print(type(samples))
    # print(type(samples[0]))
    # print(type(samples[0][0]))
    return (torch.cat([sample[0] for sample in samples]), mfccs, corrs, padding_mask), labels


class VSDataset_split_patches(Dataset):
    def __init__(self, dataset_json_file, label_csv=None, audio_conf=None, raw_wav_mode=False, specaug=False):
        self.datapath = dataset_json_file
        with open(dataset_json_file, 'r') as fp:
            data_json = json.load(fp)
        self.data = data_json['data']
        for i in range(len(self.data)):
            self.data[i]['wav'] = self.data[i]['wav'][self.data[i]['wav'].find('data_16k'):]
            self.data[i]['wav'] = os.path.join('../data', self.data[i]['wav'].replace('data_16k', 'audio_16k'))

        self.audio_conf = audio_conf
        self.mode = self.audio_conf.get('mode')
        self.melbins = self.audio_conf.get('num_mel_bins')
        self.index_dict = make_index_dict(label_csv)
        self.label_num = len(self.index_dict)
        #print('Number of classes is {:d}'.format(self.label_num))

        self.windows = {'hamming': scipy.signal.hamming, 'hann': scipy.signal.hann, 'blackman': scipy.signal.blackman, 'bartlett': scipy.signal.bartlett}

        # if just load raw wavform
        self.raw_wav_mode = raw_wav_mode
        if specaug == True:
            self.freqm = self.audio_conf.get('freqm')
            self.timem = self.audio_conf.get('timem')
            #print('now using following mask: {:d} freq, {:d} time'.format(self.audio_conf.get('freqm'), self.audio_conf.get('timem')))
        self.specaug = specaug
        self.mixup = self.audio_conf.get('mixup')
        #print('now using mix-up with rate {:f}'.format(self.mixup))
        #print('now add rolling and new mixup stategy')

    def _wav2fbank(self, filename, filename2=None):
        # not mix-up
        if filename2 == None:
            waveform, sr = torchaudio.load(filename)
            waveform = waveform - waveform.mean()
        # use for mix-up
        else:
            waveform1, sr = torchaudio.load(filename)
            waveform2, _ = torchaudio.load(filename2)

            waveform1 = waveform1 - waveform1.mean()
            waveform2 = waveform2 - waveform2.mean()

            if waveform1.shape[1] != waveform2.shape[1]:
                if waveform1.shape[1] > waveform2.shape[1]:
                    # do a padding
                    temp_wav = torch.zeros(1, waveform1.shape[1])
                    temp_wav[0, 0:waveform2.shape[1]] = waveform2
                    waveform2 = temp_wav
                else:
                    # do cutting
                    waveform2 = waveform2[0, 0:waveform1.shape[1]]

            mix_lambda = np.random.beta(10, 10)

            mix_waveform = mix_lambda * waveform1 + (1 - mix_lambda) * waveform2
            waveform = mix_waveform - mix_waveform.mean()

        fbank = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=self.melbins, dither=0.0, frame_shift=10)

        target_length = self.audio_conf.get('target_length', 1056)
        n_frames = fbank.shape[0]

        p = target_length - n_frames

        # cut and pad
        if p > 0:
            m = torch.nn.ZeroPad2d((0, 0, 0, p))
            fbank = m(fbank)
        elif p < 0:
            fbank = fbank[0:target_length, :]

        if filename2 == None:
            return fbank
        else:
            return fbank, mix_lambda

    def __getitem__(self, index):

        if random.random() < self.mixup:
            datum = self.data[index]
            mix_sample_idx = random.randint(0, len(self.data)-1)
            mix_datum = self.data[mix_sample_idx]

            # get the mixed fbank
            fbank, mix_lambda = self._wav2fbank(datum['wav'], mix_datum['wav'])
            # initialize the label
            label_indices = np.zeros(self.label_num)
            # add labels for the original sample
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += mix_lambda
            # add more labels to the original sample labels
            for label_str in mix_datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += 1-mix_lambda
            label_indices = torch.FloatTensor(label_indices)
        # not doing mixup
        else:
            # import pdb
            # pdb.set_trace()
            datum = self.data[index]
            label_indices = np.zeros(self.label_num) + 0.00
            fbank = self._wav2fbank(datum['wav'])
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)
            mix_lambda = 0

        # SpecAug, not do for eval set, by intention, it is done after the mix-up step
        if self.specaug == True:
            freqm = torchaudio.transforms.FrequencyMasking(self.freqm)
            timem = torchaudio.transforms.TimeMasking(self.timem)
            fbank = torch.transpose(fbank, 0, 1)
            fbank = fbank.unsqueeze(0)
            fbank = freqm(fbank)
            fbank = timem(fbank)
            fbank = fbank.squeeze(0)
            fbank = torch.transpose(fbank, 0, 1)

        # mean/std is get from the val set as a prior.
        fbank = (fbank + 3.05) / 5.42

        # shift if in the training set, training set typically use mixup
        if self.mode == 'train':
            fbank = torch.roll(fbank, np.random.randint(0, 1024), 0)
        mix_ratio = min(mix_lambda, 1-mix_lambda) / max(mix_lambda, 1-mix_lambda)

        fbank = fbank.unfold(0, 16, 10).unfold(1, 16, 10).reshape(-1, 16, 16)
        fbank = fbank.transpose(1,2)
        corr = torch.stack([torch.corrcoef(temp) for temp in torch.unbind(fbank, dim=0)], axis=0)
        return (fbank, corr), label_indices

    def __len__(self):
        return len(self.data)


class VSDataset_split_patches_v2(Dataset):
    def __init__(self, dataset_json_file, label_csv=None, audio_conf=None, raw_wav_mode=False, specaug=False):
        self.datapath = dataset_json_file
        with open(dataset_json_file, 'r') as fp:
            data_json = json.load(fp)
        self.data = data_json['data']
        for i in range(len(self.data)):
            self.data[i]['wav'] = self.data[i]['wav'][self.data[i]['wav'].find('data_16k'):]
            self.data[i]['wav'] = os.path.join('../data', self.data[i]['wav'].replace('data_16k', 'audio_16k'))

        self.audio_conf = audio_conf
        self.mode = self.audio_conf.get('mode')
        self.melbins = self.audio_conf.get('num_mel_bins')
        self.index_dict = make_index_dict(label_csv)
        self.label_num = len(self.index_dict)
        #print('Number of classes is {:d}'.format(self.label_num))

        self.windows = {'hamming': scipy.signal.hamming, 'hann': scipy.signal.hann, 'blackman': scipy.signal.blackman, 'bartlett': scipy.signal.bartlett}

        # if just load raw wavform
        self.raw_wav_mode = raw_wav_mode
        if specaug == True:
            self.freqm = self.audio_conf.get('freqm')
            self.timem = self.audio_conf.get('timem')
            #print('now using following mask: {:d} freq, {:d} time'.format(self.audio_conf.get('freqm'), self.audio_conf.get('timem')))
        self.specaug = specaug
        self.mixup = self.audio_conf.get('mixup')
        #print('now using mix-up with rate {:f}'.format(self.mixup))
        #print('now add rolling and new mixup stategy')

        self.samples = []
        for datum in tqdm(self.data):
            fbank = self._wav2fbank(datum['wav'])

            # SpecAug, not do for eval set, by intention, it is done after the mix-up step
            if self.specaug == True:
                freqm = torchaudio.transforms.FrequencyMasking(self.freqm)
                timem = torchaudio.transforms.TimeMasking(self.timem)
                fbank = torch.transpose(fbank, 0, 1)
                fbank = fbank.unsqueeze(0)
                fbank = freqm(fbank)
                fbank = timem(fbank)
                fbank = fbank.squeeze(0)
                fbank = torch.transpose(fbank, 0, 1)

            # mean/std is get from the val set as a prior.
            fbank = (fbank + 3.05) / 5.42

            if self.mode =='train':
                fbank = torch.roll(fbank, np.random.randint(0, 1024), 0)

            fbank = fbank.unfold(0, self.audio_conf.get('patch_size'), self.audio_conf.get('stride')).unfold(1, self.audio_conf.get('patch_size'), self.audio_conf.get('stride')).reshape(-1, self.audio_conf.get('patch_size'), self.audio_conf.get('patch_size'))
            fbank = fbank.transpose(1,2)
            corr = torch.stack([torch.corrcoef(temp) for temp in torch.unbind(fbank, dim=0)], axis=0)

            self.samples.append((fbank, corr))
    

    def _wav2fbank(self, filename, filename2=None):
        # not mix-up
        if filename2 == None:
            waveform, sr = torchaudio.load(filename)
            waveform = waveform - waveform.mean()
        # use for mix-up
        else:
            waveform1, sr = torchaudio.load(filename)
            waveform2, _ = torchaudio.load(filename2)

            waveform1 = waveform1 - waveform1.mean()
            waveform2 = waveform2 - waveform2.mean()

            if waveform1.shape[1] != waveform2.shape[1]:
                if waveform1.shape[1] > waveform2.shape[1]:
                    # do a padding
                    temp_wav = torch.zeros(1, waveform1.shape[1])
                    temp_wav[0, 0:waveform2.shape[1]] = waveform2
                    waveform2 = temp_wav
                else:
                    # do cutting
                    waveform2 = waveform2[0, 0:waveform1.shape[1]]

            mix_lambda = np.random.beta(10, 10)

            mix_waveform = mix_lambda * waveform1 + (1 - mix_lambda) * waveform2
            waveform = mix_waveform - mix_waveform.mean()

        fbank = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=self.melbins, dither=0.0, frame_shift=10)

        target_length = self.audio_conf.get('target_length', 1056)
        n_frames = fbank.shape[0]

        p = target_length - n_frames

        # cut and pad
        if p > 0:
            m = torch.nn.ZeroPad2d((0, 0, 0, p))
            fbank = m(fbank)
        elif p < 0:
            fbank = fbank[0:target_length, :]

        if filename2 == None:
            return fbank
        else:
            return fbank, mix_lambda

    def __getitem__(self, index):

        if random.random() < self.mixup:
            datum = self.data[index]
            mix_sample_idx = random.randint(0, len(self.data)-1)
            mix_datum = self.data[mix_sample_idx]

            # get the mixed fbank
            fbank, mix_lambda = self._wav2fbank(datum['wav'], mix_datum['wav'])
            # initialize the label
            label_indices = np.zeros(self.label_num)
            # add labels for the original sample
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += mix_lambda
            # add more labels to the original sample labels
            for label_str in mix_datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += 1-mix_lambda
            label_indices = torch.FloatTensor(label_indices)
        # not doing mixup
        else:
            # import pdb
            # pdb.set_trace()
            datum = self.data[index]
            label_indices = np.zeros(self.label_num) + 0.00
            # fbank = self._wav2fbank(datum['wav'])
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)
            mix_lambda = 0

        return self.samples[index], label_indices

    def __len__(self):
        return len(self.data)


class VSDataset_split_patches_v3(Dataset):
    def __init__(self, dataset_json_file, label_csv=None, audio_conf=None, raw_wav_mode=False, specaug=False, dataset='VocalSound'):
        self.datapath = dataset_json_file
        with open(dataset_json_file, 'r') as fp:
            data_json = json.load(fp)
        self.data = data_json['data']
        # for i in range(len(self.data)):
        #     self.data[i]['wav'] = self.data[i]['wav'][self.data[i]['wav'].find('data_16k'):]
        #     self.data[i]['wav'] = os.path.join('../data', self.data[i]['wav'].replace('data_16k', 'audio_16k'))

        self.audio_conf = audio_conf
        self.mode = self.audio_conf.get('mode')
        self.melbins = self.audio_conf.get('num_mel_bins')
        self.index_dict = make_index_dict(label_csv)
        self.label_num = len(self.index_dict)
        #print('Number of classes is {:d}'.format(self.label_num))

        self.windows = {'hamming': scipy.signal.hamming, 'hann': scipy.signal.hann, 'blackman': scipy.signal.blackman, 'bartlett': scipy.signal.bartlett}

        # if just load raw wavform
        self.raw_wav_mode = raw_wav_mode
        if specaug == True:
            self.freqm = self.audio_conf.get('freqm')
            self.timem = self.audio_conf.get('timem')
            #print('now using following mask: {:d} freq, {:d} time'.format(self.audio_conf.get('freqm'), self.audio_conf.get('timem')))
        self.specaug = specaug
        self.mixup = self.audio_conf.get('mixup')
        #print('now using mix-up with rate {:f}'.format(self.mixup))
        #print('now add rolling and new mixup stategy')

        self.samples = []
        for datum in tqdm(self.data):
            fbank = self._wav2fbank(datum['wav'])

            # SpecAug, not do for eval set, by intention, it is done after the mix-up step
            if self.specaug == True:
                freqm = torchaudio.transforms.FrequencyMasking(self.freqm)
                timem = torchaudio.transforms.TimeMasking(self.timem)
                fbank = torch.transpose(fbank, 0, 1)
                fbank = fbank.unsqueeze(0)
                fbank = freqm(fbank)
                fbank = timem(fbank)
                fbank = fbank.squeeze(0)
                fbank = torch.transpose(fbank, 0, 1)

            # mean/std is get from the val set as a prior.
            if dataset == 'VocalSound':
                fbank = (fbank + 3.05) / 5.42

            if self.mode =='train':
                fbank = torch.roll(fbank, np.random.randint(0, 256), 0)
            fbank = fbank.unfold(0, self.audio_conf.get('temporal_patch_size'), self.audio_conf.get('temporal_stride')).unfold(1, self.audio_conf.get('spatial_patch_size'), self.audio_conf.get('spatial_stride')).reshape(-1, self.audio_conf.get('temporal_patch_size'), self.audio_conf.get('spatial_patch_size'))
            fbank = fbank.transpose(1,2)
            corr = torch.stack([torch.corrcoef(temp) for temp in torch.unbind(fbank, dim=0)], axis=0)

            self.samples.append((fbank, corr))

    def _wav2fbank(self, filename, filename2=None):
        # not mix-up
        if filename2 == None:
            waveform, sr = torchaudio.load(filename)
            waveform = waveform - waveform.mean()
        # use for mix-up
        else:
            waveform1, sr = torchaudio.load(filename)
            waveform2, _ = torchaudio.load(filename2)

            waveform1 = waveform1 - waveform1.mean()
            waveform2 = waveform2 - waveform2.mean()

            if waveform1.shape[1] != waveform2.shape[1]:
                if waveform1.shape[1] > waveform2.shape[1]:
                    # do a padding
                    temp_wav = torch.zeros(1, waveform1.shape[1])
                    temp_wav[0, 0:waveform2.shape[1]] = waveform2
                    waveform2 = temp_wav
                else:
                    # do cutting
                    waveform2 = waveform2[0, 0:waveform1.shape[1]]

            mix_lambda = np.random.beta(10, 10)

            mix_waveform = mix_lambda * waveform1 + (1 - mix_lambda) * waveform2
            waveform = mix_waveform - mix_waveform.mean()

        fbank = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=self.melbins, dither=0.0, frame_shift=10)

        target_length = self.audio_conf.get('target_length', 1056)
        n_frames = fbank.shape[0]

        p = target_length - n_frames

        # cut and pad
        if p > 0:
            m = torch.nn.ZeroPad2d((0, 0, 0, p))
            fbank = m(fbank)
        elif p < 0:
            fbank = fbank[0:target_length, :]

        if filename2 == None:
            return fbank
        else:
            return fbank, mix_lambda

    def __getitem__(self, index):

        if random.random() < self.mixup:
            datum = self.data[index]
            mix_sample_idx = random.randint(0, len(self.data)-1)
            mix_datum = self.data[mix_sample_idx]

            # get the mixed fbank
            fbank, mix_lambda = self._wav2fbank(datum['wav'], mix_datum['wav'])
            # initialize the label
            label_indices = np.zeros(self.label_num)
            # add labels for the original sample
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += mix_lambda
            # add more labels to the original sample labels
            for label_str in mix_datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += 1-mix_lambda
            label_indices = torch.FloatTensor(label_indices)
        # not doing mixup
        else:
            # import pdb
            # pdb.set_trace()
            datum = self.data[index]
            label_indices = np.zeros(self.label_num) + 0.00
            # fbank = self._wav2fbank(datum['wav'])
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)
            mix_lambda = 0

        return self.samples[index], label_indices

    def __len__(self):
        return len(self.data)


class VSDataset_raw(Dataset):
    def __init__(self, dataset_json_file, label_csv=None, audio_conf=None, raw_wav_mode=False, specaug=False):
        self.datapath = dataset_json_file
        with open(dataset_json_file, 'r') as fp:
            data_json = json.load(fp)
        self.data = data_json['data']
        for i in range(len(self.data)):
            self.data[i]['wav'] = self.data[i]['wav'][self.data[i]['wav'].find('data_16k'):]
            self.data[i]['wav'] = os.path.join('../data', self.data[i]['wav'].replace('data_16k', 'audio_16k'))

        self.audio_conf = audio_conf
        self.mode = self.audio_conf.get('mode')
        self.melbins = self.audio_conf.get('num_mel_bins')
        self.index_dict = make_index_dict(label_csv)
        self.label_num = len(self.index_dict)
        #print('Number of classes is {:d}'.format(self.label_num))

        self.windows = {'hamming': scipy.signal.hamming, 'hann': scipy.signal.hann, 'blackman': scipy.signal.blackman, 'bartlett': scipy.signal.bartlett}

        # if just load raw wavform
        self.raw_wav_mode = raw_wav_mode
        if specaug == True:
            self.freqm = self.audio_conf.get('freqm')
            self.timem = self.audio_conf.get('timem')
            #print('now using following mask: {:d} freq, {:d} time'.format(self.audio_conf.get('freqm'), self.audio_conf.get('timem')))
        self.specaug = specaug
        self.mixup = self.audio_conf.get('mixup')
        #print('now using mix-up with rate {:f}'.format(self.mixup))
        #print('now add rolling and new mixup stategy')

    def _wav2fbank(self, filename, filename2=None):
        # not mix-up
        if filename2 == None:
            waveform, sr = torchaudio.load(filename)
            waveform = waveform - waveform.mean()
        # use for mix-up
        else:
            waveform1, sr = torchaudio.load(filename)
            waveform2, _ = torchaudio.load(filename2)

            waveform1 = waveform1 - waveform1.mean()
            waveform2 = waveform2 - waveform2.mean()

            if waveform1.shape[1] != waveform2.shape[1]:
                if waveform1.shape[1] > waveform2.shape[1]:
                    # do a padding
                    temp_wav = torch.zeros(1, waveform1.shape[1])
                    temp_wav[0, 0:waveform2.shape[1]] = waveform2
                    waveform2 = temp_wav
                else:
                    # do cutting
                    waveform2 = waveform2[0, 0:waveform1.shape[1]]

            mix_lambda = np.random.beta(10, 10)

            mix_waveform = mix_lambda * waveform1 + (1 - mix_lambda) * waveform2
            waveform = mix_waveform - mix_waveform.mean()

        fbank = torchaudio.compliance.kaldi.fbank(waveform, htk_compat=True, sample_frequency=sr, use_energy=False,
                                                  window_type='hanning', num_mel_bins=self.melbins, dither=0.0, frame_shift=10)

        target_length = self.audio_conf.get('target_length', 1056)
        n_frames = fbank.shape[0]

        p = target_length - n_frames

        # cut and pad
        if p > 0:
            m = torch.nn.ZeroPad2d((0, 0, 0, p))
            fbank = m(fbank)
        elif p < 0:
            fbank = fbank[0:target_length, :]

        if filename2 == None:
            return fbank
        else:
            return fbank, mix_lambda

    def __getitem__(self, index):

        if random.random() < self.mixup:
            datum = self.data[index]
            mix_sample_idx = random.randint(0, len(self.data)-1)
            mix_datum = self.data[mix_sample_idx]

            # get the mixed fbank
            fbank, mix_lambda = self._wav2fbank(datum['wav'], mix_datum['wav'])
            # initialize the label
            label_indices = np.zeros(self.label_num)
            # add labels for the original sample
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += mix_lambda
            # add more labels to the original sample labels
            for label_str in mix_datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] += 1-mix_lambda
            label_indices = torch.FloatTensor(label_indices)
        # not doing mixup
        else:
            # import pdb
            # pdb.set_trace()
            datum = self.data[index]
            label_indices = np.zeros(self.label_num) + 0.00
            waveform, sr = torchaudio.load(datum['wav'])
            waveform = waveform - waveform.mean()
            for label_str in datum['labels'].split(','):
                label_indices[int(self.index_dict[label_str])] = 1.0
            label_indices = torch.FloatTensor(label_indices)

            waveform = waveform.squeeze()
            target_length = 160_000
            n_frames = waveform.shape[0]

            p = target_length - n_frames

            # cut and pad
            if p > 0:
                m = torch.nn.ConstantPad1d((0, p), 0)
                waveform = m(waveform)
            elif p < 0:
                waveform = waveform[0:target_length]

        # SpecAug, not do for eval set, by intention, it is done after the mix-up step
        # if self.specaug == True:
        #     freqm = torchaudio.transforms.FrequencyMasking(self.freqm)
        #     timem = torchaudio.transforms.TimeMasking(self.timem)
        #     fbank = torch.transpose(fbank, 0, 1)
        #     fbank = fbank.unsqueeze(0)
        #     fbank = freqm(fbank)
        #     fbank = timem(fbank)
        #     fbank = fbank.squeeze(0)
        #     fbank = torch.transpose(fbank, 0, 1)

        # # mean/std is get from the val set as a prior.
        # fbank = (fbank + 3.05) / 5.42

        # shift if in the training set, training set typically use mixup
        if self.mode == 'train':
            waveform = torch.roll(waveform, np.random.randint(0, 1024), 0)
        # mix_ratio = min(mix_lambda, 1-mix_lambda) / max(mix_lambda, 1-mix_lambda)
        return waveform, label_indices

    def __len__(self):
        return len(self.data)
