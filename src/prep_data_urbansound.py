import pandas as pd
import json
from tqdm import tqdm
import librosa
import soundfile as sf

df = pd.read_csv('../data_urbansound/UrbanSound8K/metadata/UrbanSound8K.csv')

# # Iterate through each row in the DataFrame
# for index, row in tqdm(df.iterrows()):
#     audio_file_path =  f"/mnt/w2g2/data_urbansound/UrbanSound8K/audio/fold{row['fold']}/{row['slice_file_name']}"
#     # Read the audio file
#     y, sr = librosa.load(audio_file_path, sr=None)
#     # Resample the audio to 22050 Hz
#     y_resampled = librosa.resample(y, orig_sr=sr, target_sr=22050)
#     # Define the output file path with the postfix
#     output_file_path = audio_file_path.replace('.wav', '_22050.wav')
#     # Save the resampled audio
#     sf.write(output_file_path, y_resampled, 22050)

classes = df['class'].unique()

label_df = pd.DataFrame()
label_df['mid'] = classes
label_df['display_name'] = classes
label_df.index.names = ['index']
label_df.to_csv('../data_urbansound/class_labels_indices_us8k.csv')

for fold in tqdm(range(1,11)):
    data_list = []
    test_data_list = []

    # Iterate through each row in the DataFrame
    for index, row in df.iterrows():
        audio_file_path =  f"/mnt/w2g2/data_urbansound/UrbanSound8K/audio/fold{row['fold']}/{row['slice_file_name']}"
        output_file_path = audio_file_path.replace('.wav', '_22050.wav')
        # Create a dictionary for each data entry
        data_entry = {
            "wav": output_file_path,
            "labels": row['class']  # Get the label from the mapping
        }
        if row['fold'] == fold:
            test_data_list.append(data_entry)
        else:
            data_list.append(data_entry)

    # Create the final JSON structure
    json_data = {
        "data": data_list
    }

    with open(f'../data_urbansound/UrbanSound8K/metadata/tr_{fold}.json', 'w') as json_file:
        json.dump(json_data, json_file, indent=2)

    # Create the final JSON structure
    json_data = {
        "data": test_data_list
    }

    with open(f'../data_urbansound/UrbanSound8K/metadata/te_{fold}.json', 'w') as json_file:
        json.dump(json_data, json_file, indent=2)
