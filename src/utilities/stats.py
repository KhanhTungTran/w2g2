import numpy as np
from scipy import stats
from sklearn import metrics
import torch

def d_prime(auc):
    standard_normal = stats.norm()
    d_prime = standard_normal.ppf(auc) * np.sqrt(2.0)
    return d_prime

def calculate_stats(output, target):
    """Calculate statistics including mAP, AUC, etc.

    Args:
      output: 2d array, (samples_num, classes_num)
      target: 2d array, (samples_num, classes_num)

    Returns:
      stats: list of statistic of each class.
    """

    classes_num = target.shape[-1]
    stats = []

    # Class-wise statistics
    for k in range(classes_num):

        # Average precision
        avg_precision = metrics.average_precision_score(
            target[:, k], output[:, k], average=None)

        # AUC
        try:
            auc = metrics.roc_auc_score(target[:, k], output[:, k], average=None)
        except Exception as e:
            # print(e)
            # auc = metrics.roc_auc_score(target[:, k] + 1e-7, output[:, k], average=None)
            auc = 0

        # Accuracy
        # this is only used for single-label classification such as esc-50, not for multiple label one such as AudioSet
        acc = metrics.accuracy_score(np.argmax(target, 1), np.argmax(output, 1))

        # F1
        target_i = np.argmax(target, axis=1)
        output_i = np.argmax(output, axis=1)
        f1 = metrics.f1_score(target_i, output_i, average=None)

        # Precisions, recalls
        (precisions, recalls, thresholds) = metrics.precision_recall_curve(
            target[:, k], output[:, k])

        # FPR, TPR
        (fpr, tpr, thresholds) = metrics.roc_curve(target[:, k], output[:, k])

        save_every_steps = 1     # Sample statistics to reduce size
        dict = {'precisions': precisions[0::save_every_steps],
                'recalls': recalls[0::save_every_steps],
                'AP': avg_precision,
                'fpr': fpr[0::save_every_steps],
                'fnr': 1. - tpr[0::save_every_steps],
                'auc': auc,
                'acc': acc,
                'f1': f1
                }
        stats.append(dict)

    return stats


# Compute F1 given predictions and truth
def f1(pred, truth):
    return 2.0 * (pred & truth).sum() / (pred.sum() + truth.sum())


# Given scores and truth for a single class (as 1-D numpy arrays), find optimal threshold and corresponding F1
# Statistics of other classes may be given to optimize micro-average F1
def optimize_f1(scores, truth, extraNcorr = 0, extraNtrue = 0, extraNpred = 0):
    # Start with predicting everything as negative
    best_thres = np.inf
    best_f1 = 0.0
    num = extraNcorr                                # number of correctly predicted instances
    den = extraNtrue + extraNpred + truth.sum()     # number of predicted instances + true instances
    instances = [(-np.inf, False)] + sorted(zip(scores, truth))
    # Lower the threshold gradually
    for i in range(len(instances) - 1, 0, -1):
        if instances[i][1]: num += 1
        den += 1
        if instances[i][0] > instances[i-1][0]:     # Can put threshold here
            f1 = 2.0 * num / den
            if f1 > best_f1:
                best_thres = (instances[i][0] + instances[i-1][0]) / 2
                best_f1 = f1
    return best_thres, best_f1


# Given scores and truth for many classes (as 2-D numpy arrays),
# find the optimal class-specific thresholds (as a 1-D numpy array) that maximizes the micro-average F1
# The algorithm is stochastic, but I have always observed deterministic results
def optimize_micro_avg_f1(scores, truth):
    # First optimize each class individually
    nClasses = truth.shape[1]
    thres = np.zeros(nClasses, dtype = 'float64')
    for i in range(nClasses):
        thres[i], _ = optimize_f1(scores[:,i], truth[:,i])
    Ntrue = truth.sum(axis = 0)
    Npred = (scores >= thres).sum(axis = 0)
    Ncorr = ((scores >= thres) & truth).sum(axis = 0)

    # Repeatly re-tune the threshold for each class until convergence
    candidates = range(nClasses)
    while len(candidates) > 0:
        i = np.random.choice(candidates)
        candidates.remove(i)
        old_thres = thres[i]
        thres[i], _ = optimize_f1(
            scores[:,i],
            truth[:,i],
            extraNcorr = Ncorr.sum() - Ncorr[i],
            extraNtrue = Ntrue.sum() - Ntrue[i],
            extraNpred = Npred.sum() - Npred[i],
        )
        if thres[i] != old_thres:
            Npred[i] = (scores[:,i] >= thres[i]).sum(axis = 0)
            Ncorr[i] = ((scores[:,i] >= thres[i]) & truth[:,i]).sum(axis = 0)
            candidates = range(nClasses)
            candidates.remove(i)

    return thres
