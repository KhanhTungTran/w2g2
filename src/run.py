# Author: David Harwath
import argparse
import os
import pickle
import sys
from collections import OrderedDict
import time
import torch
import shutil
import dataloaders
import models
from traintest import train, validate
import ast
from torch.utils.data import WeightedRandomSampler
import numpy as np
import wandb
from datetime import datetime
from utilities import d_prime
import random

def set_seed(seed):
    '''
    Disable cudnn to maximize reproducibility
    '''
    torch.cuda.cudnn_enabled = False
    torch.backends.cudnn.deterministic = True
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)

print("I am process %s, running on %s: starting (%s)" % (
        os.getpid(), os.uname()[1], time.asctime()))

os.environ["WANDB_API_KEY"] = "7e8775834363fa066e949d9701ede4f005a9485a"
os.environ["WANDB__SERVICE_WAIT"] = "300"
os.environ["WANDB_MODE"] = "online"

# I/O args
parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--data-train", type=str, default='', help="training data json")
parser.add_argument("--data-val", type=str, default='', help="validation data json")
parser.add_argument("--label-csv", type=str, default='', help="csv with class labels")
parser.add_argument("--exp-dir", type=str, default="", help="directory to dump experiments")
# training and optimization args
parser.add_argument("--optim", type=str, default="adam", help="training optimizer", choices=["sgd", "adam"])
parser.add_argument('-b', '--batch-size', default=60, type=int, metavar='N', help='mini-batch size (default: 100)')
parser.add_argument('-w', '--num-workers', default=2, type=int, metavar='NW', help='# of workers for dataloading (default: 8)')
parser.add_argument('--lr', '--learning-rate', default=0.001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--lrp', default=1.0, type=float, help='learning rate multiplier')
parser.add_argument('--lr-decay', default=40, type=int, metavar='LRDECAY', help='Divide the learning rate by 10 every lr_decay epochs')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M', help='momentum')
parser.add_argument('--weight-decay', '--wd', default=5e-7, type=float, metavar='W', help='weight decay (default: 1e-4)')
parser.add_argument("--n-epochs", type=int, default=1, help="number of maximum training epochs")
parser.add_argument("--n-print-steps", type=int, default=1, help="number of steps to print statistics")
# models args
parser.add_argument("--n_class", type=int, default=17, help="number of classes")
parser.add_argument('--save_model', help='save the models or not', type=ast.literal_eval)
parser.add_argument("--model", type=str, default='eff_mean', help="model")
parser.add_argument('--wave2graph', help='if use wave2graph', type=ast.literal_eval, default='False')
parser.add_argument("--w2g_hidden_dim", type=int, default=512, help="hidden dim of w2g")
parser.add_argument("--chunk_size", type=int, default=128, help="size of each window for temporal wave2graph")
parser.add_argument("--step_size", type=int, default=64, help="step size for each slide of temporal wave2graph")
parser.add_argument("--graph_out", type=int, default=256, help="output dim of graph layer of temporal wave2graph")
parser.add_argument("--rnn_hiddem_dim", type=int, default=256, help="rnn hidden dim of gru layer of temporal wave2graph")
parser.add_argument("--rnn_num_layers", type=int, default=2, help="number of gru layer of temporal wave2graph")
parser.add_argument("--rnn_output_dim", type=int, default=256, help="output dim for temporal wave2graph")
parser.add_argument("--w2g_dropout", type=float, default=0.8, help="drop out rate of w2g")
parser.add_argument("--clip_norm", type=float, default=10.0, help="clip norm")
parser.add_argument("--model_size", type=int, default=0, help="model size")
parser.add_argument('--imagenet_pretrain', help='if use pretrained imagenet efficient net', type=ast.literal_eval, default='True')
parser.add_argument('--freqm', help='frequency mask max length', type=int, default=0)
parser.add_argument('--timem', help='time mask max length', type=int, default=0)
parser.add_argument("--mixup", type=float, default=0, help="how many (0-1) samples need to be mixup during training")
parser.add_argument("--train_mode", help='if True, perform training', type=ast.literal_eval, default='True')
parser.add_argument("--extract_embedding", help='if True, extract embedding learned by each branch in the validate function', type=ast.literal_eval, default='False')
parser.add_argument("--wandb_project", type=str, default='wave2graph', help="wandb project")
parser.add_argument("--seed", type=int, default=0, help="seed")
parser.add_argument('--graph_n_hidden_dim', type=int,default=1)
parser.add_argument('--adj_threshold', type=float,default=-1.0)
parser.add_argument('--inner_dropout', type=float,default=0.0)
parser.add_argument('--add_self_loops', action='store_true',default=True)
parser.add_argument('--gat_n_head', type=int,default=1)
parser.add_argument('--patch_size', type=int,default=16)
parser.add_argument('--stride', type=int,default=10)
parser.add_argument('--temporal_patch_size', type=int,default=16)
parser.add_argument('--temporal_stride', type=int,default=10)
parser.add_argument('--spatial_patch_size', type=int,default=16)
parser.add_argument('--spatial_stride', type=int,default=10)
parser.add_argument('--transformer_num_heads', type=int,default=2)
parser.add_argument('--transformer_num_layers', type=int,default=2)
parser.add_argument('--transformer_in_dim', type=int,default=256)
parser.add_argument('--gat_ver', type=str,default='v2')
parser.add_argument('--cls_token', type=ast.literal_eval,default=False)
parser.add_argument('--dataset', type=str,default='VocalSound')
parser.add_argument('--target_length', type=int,default=512)
parser.add_argument('--fused_with_cnn', type=str,default=None)
parser.add_argument('--fused_with_effnet', type=ast.literal_eval,default=False)
parser.add_argument('--scale_multi_res', type=float,default=1.0)
parser.add_argument('--patch_mask', type=float,default=-1.0)
parser.add_argument('--use_drloc', type=ast.literal_eval,default=False)
parser.add_argument('--drloc_mode', type=str,default="l1")
parser.add_argument('--sample_size', type=int,default=32)
parser.add_argument('--use_abs', type=ast.literal_eval,default=False)
parser.add_argument('--lambda_drloc', type=float,default=0.5)
parser.add_argument('--weakly_sup', type=ast.literal_eval,default=False)
parser.add_argument('--weakly_sup_mode', type=str,default="mean")
parser.add_argument('--class_weights', type=ast.literal_eval,default=False)
parser.add_argument('--multi_label', type=ast.literal_eval,default=False)
parser.add_argument('--act', type=ast.literal_eval,default=False)
parser.add_argument('--multi_task', type=ast.literal_eval,default=False)
parser.add_argument('--second_task_dim', type=int,default=-1)
parser.add_argument('--second_task_lambda', type=float,default=1.0)
parser.add_argument('--multi_task_reverse_grad', type=ast.literal_eval,default=False)


args = parser.parse_args()

set_seed(args.seed)

audio_conf = {'num_mel_bins': 128, 'target_length': args.target_length, 'freqm': args.freqm, 'timem': args.timem, 'mixup': args.mixup, 'mode': 'train', 'patch_size': args.patch_size, 'stride': args.stride, 'temporal_patch_size': args.temporal_patch_size, 'temporal_stride': args.temporal_stride, 'spatial_patch_size': args.spatial_patch_size, 'spatial_stride': args.spatial_stride}

if args.model.endswith('mfcc'):
    data_loader = dataloaders.VSDataset_w2g
    collater = None
elif args.model.endswith('mfcc_temporal'):
    data_loader = dataloaders.VSDataset_temp_w2g
    collater = dataloaders.collate_fn
elif args.model == 'AST_GNN':
    data_loader = dataloaders.VSDataset_split_patches_v2
    collater = None
elif args.model == 'AST_GNN_diff_size':
    data_loader = dataloaders.VSDataset_split_patches_v3
    collater = None
elif args.model == 'w2v2':
    data_loader = dataloaders.VSDataset_raw
    collater = None
elif args.model == 'AST_GNN_multi_res':
    data_loader = dataloaders.VSDataset_multi_res
    collater = None
else:
    data_loader = dataloaders.VSDataset
    collater = None

if args.multi_task:
    data_loader = dataloaders.VSDataset_multi_task
    collater = None

print('balanced sampler is not used')
train_loader = torch.utils.data.DataLoader(
    data_loader(args.data_train, label_csv=args.label_csv, audio_conf=audio_conf, raw_wav_mode=False, specaug=True, dataset=args.dataset),
    batch_size=args.batch_size, shuffle=True, num_workers=args.num_workers, pin_memory=False,
    collate_fn=collater)

val_audio_conf = {'num_mel_bins': 128, 'target_length': args.target_length, 'mixup': 0, 'mode': 'valid', 'patch_size': args.patch_size, 'stride': args.stride, 'temporal_patch_size': args.temporal_patch_size, 'temporal_stride': args.temporal_stride, 'spatial_patch_size': args.spatial_patch_size, 'spatial_stride': args.spatial_stride}
test_audio_conf = {'num_mel_bins': 128, 'target_length': args.target_length, 'mixup': 0, 'mode': 'test', 'patch_size': args.patch_size, 'stride': args.stride, 'temporal_patch_size': args.temporal_patch_size, 'temporal_stride': args.temporal_stride, 'spatial_patch_size': args.spatial_patch_size, 'spatial_stride': args.spatial_stride}

val_loader = torch.utils.data.DataLoader(
    data_loader(args.data_val, label_csv=args.label_csv, audio_conf=val_audio_conf, raw_wav_mode=False, dataset=args.dataset),
    batch_size=args.batch_size, shuffle=False, num_workers=args.num_workers, pin_memory=False,
    collate_fn=collater)

if args.model == 'eff_mean':
    wave2graph_in = audio_conf['target_length'] if args.wave2graph else 0
    audio_model = models.EffNetMean(label_dim=args.n_class, level=args.model_size, pretrain=args.imagenet_pretrain, wave2graph_in=wave2graph_in)
elif args.model == 'eff_mean_v2_mfcc':
    # wave2graph_in = audio_conf['target_length'] if args.wave2graph else 0
    wave2graph_in = 800
    audio_model = models.EffNetMean_v2_mfcc(label_dim=args.n_class, level=args.model_size, pretrain=args.imagenet_pretrain, wave2graph_in=wave2graph_in, hidden_dim=args.w2g_hidden_dim, dropout=args.w2g_dropout)
elif args.model == 'eff_mean_v2':
    wave2graph_in = audio_conf['target_length'] if args.wave2graph else 0
    audio_model = models.EffNetMean_v2(
        label_dim=args.n_class, 
        level=args.model_size, 
        pretrain=args.imagenet_pretrain, 
        wave2graph_in=wave2graph_in, 
        hidden_dim=args.w2g_hidden_dim, 
        dropout=args.w2g_dropout, 
        weakly_sup=args.weakly_sup,
        weakly_sup_mode=args.weakly_sup_mode,
        act=args.act,
        multi_task=args.multi_task,
        second_task_dim=args.second_task_dim,
        multi_task_reverse_grad=args.multi_task_reverse_grad)
elif args.model == 'eff_mean_v3':
    wave2graph_in = audio_conf['target_length'] if args.wave2graph else 0
    audio_model = models.EffNetMean_v3(label_dim=args.n_class, level=args.model_size, pretrain=args.imagenet_pretrain, dropout=args.w2g_dropout, chunk_size=args.chunk_size, step_size=args.step_size, hidden_dim=args.w2g_hidden_dim, graph_out=args.graph_out, rnn_hiddem_dim=args.rnn_hiddem_dim, rnn_output_dim=args.rnn_output_dim, rnn_num_layers=args.rnn_num_layers)
elif args.model == 'eff_mean_v4':
    wave2graph_in = audio_conf['target_length'] if args.wave2graph else 0
    audio_model = models.EffNetMean_v4(label_dim=args.n_class, level=args.model_size, pretrain=args.imagenet_pretrain, dropout=args.w2g_dropout, chunk_size=args.chunk_size, step_size=args.step_size, hidden_dim=args.w2g_hidden_dim, graph_out=args.graph_out, graph_in=wave2graph_in)
elif args.model == 'graph_only':
    wave2graph_in = audio_conf['target_length'] if args.wave2graph else 0
    audio_model = models.GraphOnlyNet(label_dim=args.n_class, wave2graph_in=wave2graph_in, hidden_dim=args.w2g_hidden_dim, dropout=args.w2g_dropout)
elif args.model == 'graph_mfcc':
    # wave2graph_in = audio_conf['target_length'] if args.wave2graph else 0
    wave2graph_in = 800
    audio_model = models.GraphOnlyMFCCNet(
        label_dim=args.n_class, 
        wave2graph_in=wave2graph_in, 
        hidden_dim=args.w2g_hidden_dim, 
        dropout=args.w2g_dropout,
        graph_n_hidden_dim=args.graph_n_hidden_dim, 
        adj_threshold=args.adj_threshold, 
        inner_dropout=args.inner_dropout, 
        add_self_loops=args.add_self_loops, 
        gat_n_head=args.gat_n_head
    )
elif args.model == 'graph_mfcc_temporal':
    # wave2graph_in = audio_conf['target_length'] if args.wave2graph else 0
    wave2graph_in = 96
    audio_model = models.TemporalGraphOnlyMFCCNet(
        label_dim=args.n_class, 
        wave2graph_in=wave2graph_in, 
        hidden_dim=args.w2g_hidden_dim, 
        dropout=args.w2g_dropout,
        graph_n_hidden_dim=args.graph_n_hidden_dim, 
        adj_threshold=args.adj_threshold, 
        inner_dropout=args.inner_dropout, 
        add_self_loops=args.add_self_loops, 
        gat_n_head=args.gat_n_head,
        graph_out_dim=args.graph_out
    )
elif args.model == 'graph_posemb_mfcc_temporal':
    # wave2graph_in = audio_conf['target_length'] if args.wave2graph else 0
    wave2graph_in = 96
    audio_model = models.TemporalGraphOnlyMFCCNetPosEmb(
        label_dim=args.n_class, 
        wave2graph_in=wave2graph_in, 
        hidden_dim=args.w2g_hidden_dim, 
        dropout=args.w2g_dropout,
        graph_n_hidden_dim=args.graph_n_hidden_dim, 
        adj_threshold=args.adj_threshold, 
        inner_dropout=args.inner_dropout, 
        add_self_loops=args.add_self_loops, 
        gat_n_head=args.gat_n_head,
        graph_out_dim=args.graph_out
    )
elif args.model == 'AST':
    audio_model = models.AST(
        num_heads=args.transformer_num_heads,
        num_layers=args.transformer_num_layers,
    )
elif args.model == 'AST_pretrained':
    from ast_models import ASTModel
    audio_model = ASTModel(input_tdim=args.target_length,
                       label_dim=args.n_class,
                       audioset_pretrain=True, 
                       weakly_sup=args.weakly_sup,
                       weakly_sup_mode=args.weakly_sup_mode)
elif args.model == 'AST_GNN':
    audio_model = models.AST_GNN(
        graph_n_hidden_dim=args.graph_n_hidden_dim, 
        adj_threshold=args.adj_threshold, 
        inner_dropout=args.inner_dropout, 
        add_self_loops=args.add_self_loops, 
        gat_n_head=args.gat_n_head,
        patch_size=args.patch_size,
        stride=args.stride,
        num_heads=args.transformer_num_heads,
        num_layers=args.transformer_num_layers,
    )
elif args.model == 'AST_GNN_diff_size':
    if args.gat_ver=='v6':
        arch = models.AST_GNN_v7
    elif args.gat_ver=='v5':
        arch = models.AST_GNN_v6
    elif args.gat_ver=='v4':
        arch = models.AST_GNN_v5
    elif args.gat_ver=='v3':
        arch = models.AST_GNN_v4
    elif args.cls_token:
        arch = models.AST_GNN_v3
    elif args.gat_ver=='v2':
        arch = models.AST_GNN_v2
    else:
        arch = models.AST_GNN
    audio_model = arch(
        graph_n_hidden_dim=args.graph_n_hidden_dim, 
        adj_threshold=args.adj_threshold, 
        inner_dropout=args.inner_dropout, 
        add_self_loops=args.add_self_loops, 
        gat_n_head=args.gat_n_head,
        patch_size=args.temporal_patch_size,
        stride=args.stride,
        temporal_patch_size = args.temporal_patch_size,
        spatial_patch_size = args.spatial_patch_size,
        num_heads=args.transformer_num_heads,
        num_layers=args.transformer_num_layers,
        label_dim=args.n_class
    )
elif args.model in ['AST_GNN_virtual_node', 'AST_GNN_multi_res']:
    audio_model = models.AST_GNN_virtual_node(
        graph_n_hidden_dim=args.graph_n_hidden_dim, 
        adj_threshold=args.adj_threshold, 
        inner_dropout=args.inner_dropout, 
        add_self_loops=args.add_self_loops, 
        gat_n_head=args.gat_n_head,
        temporal_patch_size = args.temporal_patch_size,
        spatial_patch_size = args.spatial_patch_size,
        temporal_stride = args.temporal_stride,
        spatial_stride = args.spatial_stride,
        num_heads=args.transformer_num_heads,
        num_layers=args.transformer_num_layers,
        in_dim=args.transformer_in_dim,
        label_dim=args.n_class,
        graph_arch=args.gat_ver,
        fused_with_cnn=args.fused_with_cnn,
        multi_res=True if args.model == 'AST_GNN_multi_res' else False,
        scale_multi_res=args.scale_multi_res,
        fused_with_effnet=args.fused_with_effnet,
        patch_mask=args.patch_mask,
        use_drloc = args.use_drloc,
        drloc_mode = args.drloc_mode,
        sample_size = args.sample_size,
        use_abs = args.use_abs,
    )
elif args.model == 'Whisper_AE':
    audio_model = models.Whisper_AE(
        num_heads=args.transformer_num_heads,
        num_layers=args.transformer_num_layers,
    )
elif args.model == 'w2v2':
    audio_model = models.w2v2()
else:
    raise ValueError('Model Unrecognized')

if args.train_mode:
    wandb.init(
        project=args.wandb_project,
        config=args,
        settings=wandb.Settings(start_method="fork"),
    )
    api = wandb.Api()
    runs = api.runs(f'snp-robustness/{args.wandb_project}')
    all_run_names = [run.name for run in runs]
    while True:
        current_time = datetime.now().strftime('%Y-%m-%d/%H-%M-%S')
        if current_time not in all_run_names:
            wandb.run.name = current_time
            break
    # start training
    args.exp_dir = f'../exp/{current_time}'
    if os.path.exists(args.exp_dir):
        print("Deleting existing experiment directory %s" % args.exp_dir)
        shutil.rmtree(args.exp_dir)
    print("\nCreating experiment directory: %s" % args.exp_dir)
    os.makedirs("%s/models" % args.exp_dir)
    # os.makedirs(f"{current_time}/models")
    with open("%s/args.pkl" % args.exp_dir, "wb") as f:
        pickle.dump(args, f)

    print('Now starting training for {:d} epochs'.format(args.n_epochs))
    train(audio_model, train_loader, val_loader, args)

if not args.train_mode:
    args.exp_dir = f'../exp/2023-10-11/21-37-52'
    # args.exp_dir = f'../exp/2023-09-29/15-34-37'
# test on the test set and sub-test set, model selected on the validation set
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
sd = torch.load(args.exp_dir + '/models/best_audio_model.pth', map_location=device)
audio_model = torch.nn.DataParallel(audio_model)
audio_model.load_state_dict(sd)
all_res = []

# best model on the validation set, repeat to confirm
stats, _ = validate(audio_model, val_loader, args, 'valid_set', args.extract_embedding)
# note it is NOT mean of class-wise accuracy
val_acc = stats[0]['acc']
val_mAUC = np.mean([stat['auc'] for stat in stats])
print('---------------evaluate on the validation set---------------')
print("Accuracy: {:.6f}".format(val_acc))
all_res.append(val_acc)

mAP = np.mean([stat['AP'] for stat in stats])
mAUC = np.mean([stat['auc'] for stat in stats])
acc = np.mean([stat['acc'] for stat in stats])
print("AUC: {:.6f}".format(mAUC))

middle_ps = [stat['precisions'][int(len(stat['precisions'])/2)] for stat in stats]
middle_rs = [stat['recalls'][int(len(stat['recalls'])/2)] for stat in stats]
average_precision = np.mean(middle_ps)
average_recall = np.mean(middle_rs)

if args.train_mode:
    wandb.run.summary[f"valid_set/accuracy"] = acc
    wandb.run.summary[f"valid_set/mAP"] = mAP
    wandb.run.summary[f"valid_set/AUC"] = mAUC
    wandb.run.summary[f"valid_set/precision"] = average_precision
    wandb.run.summary[f"valid_set/recall"] = average_recall
    wandb.run.summary[f"valid_set/d_prime"] = d_prime(mAUC)


# test the model on the evaluation set
data_eval_list = ['te.json', 'subtest/te_age1.json', 'subtest/te_age2.json', 'subtest/te_age3.json', 'subtest/te_female.json', 'subtest/te_male.json']
eval_name_list = ['all_test', 'test age 18-25', 'test age 26-48', 'test age 49-80', 'test female', 'test male']
if not args.train_mode and args.extract_embedding:
    data_eval_list = [args.data_train.split('/')[-1]]
    eval_name_list = ['train_set']
# data_eval_list = ['te.json']
# eval_name_list = ['all_test']

data_dir = '/'.join(args.data_val.split('/')[:-1])
for idx, cur_eval in enumerate(data_eval_list):
    cur_eval = data_dir + '/' + cur_eval
    eval_loader = torch.utils.data.DataLoader(
        data_loader(cur_eval, label_csv=args.label_csv, audio_conf=test_audio_conf, dataset=args.dataset),
        batch_size=args.batch_size*2, shuffle=False, num_workers=args.num_workers, pin_memory=False,
        collate_fn=collater)
    stats, _ = validate(audio_model, eval_loader, args, eval_name_list[idx], args.extract_embedding)
    eval_acc = stats[0]['acc']
    all_res.append(eval_acc)
    print('---------------evaluate on {:s}---------------'.format(eval_name_list[idx]))
    print("Accuracy: {:.6f}".format(eval_acc))

    mAP = np.mean([stat['AP'] for stat in stats])
    mAUC = np.mean([stat['auc'] for stat in stats])
    acc = np.mean([stat['acc'] for stat in stats])
    print("AUC: {:.6f}".format(mAUC))

    middle_ps = [stat['precisions'][int(len(stat['precisions'])/2)] for stat in stats]
    middle_rs = [stat['recalls'][int(len(stat['recalls'])/2)] for stat in stats]
    average_precision = np.mean(middle_ps)
    average_recall = np.mean(middle_rs)

    if args.train_mode:
        wandb.run.summary[f"{eval_name_list[idx]}/accuracy"] = acc
        wandb.run.summary[f"{eval_name_list[idx]}/mAP"] = mAP
        wandb.run.summary[f"{eval_name_list[idx]}/AUC"] = mAUC
        wandb.run.summary[f"{eval_name_list[idx]}/precision"] = average_precision
        wandb.run.summary[f"{eval_name_list[idx]}/recall"] = average_recall
        wandb.run.summary[f"{eval_name_list[idx]}/d_prime"] = d_prime(mAUC)

all_res = np.array(all_res)
all_res = all_res.reshape([1, all_res.shape[0]])
np.savetxt(args.exp_dir + '/all_eval_result.csv', all_res, header=','.join(['validation'] + eval_name_list))
