import torch.nn as nn
import torch
import torchaudio as ta
import math
from efficientnet_pytorch import EfficientNet
from GCN import *
from GAT import GAT, GAT_v2, GAT_v3, GAT_v4, GAT_virtual_node, GAT_virtual_node_v2, GAT_virtual_node_v3, GAT_virtual_node_v4, GAT_virtual_node_v5, GAT_virtual_node_v6, GAT_virtual_node_v7, GAT_virtual_node_v8, GNN_sep_emb, GNN_sep_emb_v2, GNN_sep_emb_v3, GNN_sep_emb_v4, GNN_sep_emb_v5, only_emb, only_gnn_no_diff_emb, only_gnn, GNN_sep_emb_v6, only_gnn_v2, only_gnn_v3, only_gnn_decorr_adj, only_gnn_fc_adj
from timm.models.layers import trunc_normal_
from torchsummary import summary
from custom_transformers import TransformerEncoder, TransformerEncoderLayer
from munch import Munch
from drloc import DenseRelativeLoc

def init_layer(layer):
    if layer.weight.ndimension() == 4:
        (n_out, n_in, height, width) = layer.weight.size()
        n = n_in * height * width
    elif layer.weight.ndimension() == 2:
        (n_out, n) = layer.weight.size()

    std = math.sqrt(2. / n)
    scale = std * math.sqrt(3.)
    layer.weight.data.uniform_(-scale, scale)

    if layer.bias is not None:
        layer.bias.data.fill_(0.)

class MeanPooling(nn.Module):
    def __init__(self, n_in, n_out, att_activation, cla_activation):
        super(MeanPooling, self).__init__()
        self.cla_activation = cla_activation
        self.cla = nn.Conv2d(in_channels=n_in, out_channels=n_out, kernel_size=(1, 1), stride=(1, 1), padding=(0, 0), bias=True)
        self.init_weights()

    def init_weights(self):
        init_layer(self.cla)

    def activate(self, x, activation):
        return torch.sigmoid(x)

    def forward(self, x):
        """input: (samples_num, freq_bins, time_steps, 1)
        """
        cla = self.cla(x)
        cla = self.activate(cla, self.cla_activation)
        cla = cla[:, :, :, 0]
        x = torch.mean(cla, dim=2)
        return x

class GraphOnlyNet(nn.Module):
    def __init__(self, label_dim=6, wave2graph_in=512, hidden_dim=512, dropout=0.8):
        super(GraphOnlyNet, self).__init__()
        self.wave2graph = Wave2Graph_v2(nfeat=wave2graph_in, hidden_dim=hidden_dim, nclass=256, dropout=dropout)
        self.fc = nn.Sequential(
            nn.Linear(256, label_dim),
        )
    def forward(self, x, extract_embedding=False):
        if x.shape[2] < x.shape[1]:
            x = x.transpose(1, 2)

        graph_out = self.wave2graph(x) # B, num_nodes, out_dim
        pred_out = self.fc(graph_out)
        if extract_embedding:
            return pred_out, graph_out, graph_out
        else:
            return pred_out

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        params_lst = [
            {'params': [p for p in self.wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
        ]
        return params_lst

class GradientReverse(torch.autograd.Function):
    scale = 1.0
    @staticmethod
    def forward(ctx, x):
        return x.view_as(x)

    @staticmethod
    def backward(ctx, grad_output):
        return GradientReverse.scale * grad_output.neg()

def grad_reverse(x, scale=1.0):
    GradientReverse.scale = scale
    return GradientReverse.apply(x)


class EffNetMean_v2(nn.Module):
    def __init__(self, label_dim=527, level=0, pretrain=True, wave2graph_in=0, hidden_dim=512, dropout=0.8, weakly_sup=False, weakly_sup_mode='mean', act=False, multi_task=False, second_task_dim=-1, multi_task_reverse_grad=False):
        super(EffNetMean_v2, self).__init__()
        self.middim = [1280, 1280, 1408, 1536, 1792, 2048, 2304, 2560]
        if pretrain == False:
            print('not using imagenet pretrained network')
            self.effnet = EfficientNet.from_name('efficientnet-b'+str(level), in_channels=1)
        else:
            print('using imagenet pretrained network')
            self.effnet = EfficientNet.from_pretrained('efficientnet-b'+str(level), in_channels=1)
        self.attention = MeanPooling(
            self.middim[level],
            256,
            att_activation='sigmoid',
            cla_activation='sigmoid')
        self.avgpool = nn.AvgPool2d((4, 1))
        self.effnet._fc = nn.Identity()

        self.wave2graph_in = wave2graph_in
        if wave2graph_in > 0:
            self.wave2graph = Wave2Graph_v2(nfeat=wave2graph_in, hidden_dim=hidden_dim, nclass=256, dropout=dropout)
            # self.fc = nn.Linear(256 + self.middim[level], self.middim[level])
            # self.fc = nn.Linear(128, label_dim)
            self.fc = nn.Sequential(
                # nn.Linear(512, 128),
                # nn.ReLU(),
                # nn.Dropout(0.1),
                # nn.Linear(128, label_dim),
                nn.Linear(512, label_dim),
            )
        else:
            self.fc = nn.Sequential(
                nn.Linear(256, label_dim),
                nn.Sigmoid() if act else nn.Identity(),
            )
        self.count = 0
        self.weakly_sup = weakly_sup
        self.weakly_sup_mode = weakly_sup_mode

        self.multi_task = multi_task
        if self.multi_task:
            self.second_task_fc = nn.Sequential(
                nn.Linear(256, second_task_dim),
            )
            self.third_task_fc = nn.Sequential(
                nn.Linear(256, second_task_dim),
            )
            self.multi_task_reverse_grad = multi_task_reverse_grad

    def forward(self, x, extract_embedding=False):
        B = x.shape[0]
        if x.shape[2] < x.shape[1]:
            x = x.transpose(1, 2)
        # x: B, mel_bin, time_len
        if x.dim() == 3:
            f = x.unsqueeze(1)
        else:
            f = x
        # f: B, 1, mel_bin, time_len, time_len = 1000
        if self.weakly_sup:
            num_sam = int(f.shape[-1]/200)
            f = f.reshape(f.shape[0], f.shape[1], f.shape[2], 200, num_sam)
            f = f.reshape(f.shape[0]*num_sam, f.shape[1], f.shape[2], 200)
        f = self.effnet.extract_features(f)
        # print(f.shape)
        # f = self.avgpool(f)
        # f = f.transpose(2, 3)
        f = torch.mean(f, dim=-1)
        f = f.unsqueeze(dim=-1)
        # print(f.shape)
        effnet_out = self.attention(f)
        # print(out.shape)
        if self.wave2graph_in > 0:
            '''
            outs = []
            for b in range(x.shape[0]):
                # outs.append(self.fc(torch.cat([torch.flatten(self.wave2graph(x[b])), out[b]])))
                graph_out = torch.max(self.wave2graph(x[b]), dim=0, keepdim=False)[0]
                outs.append(torch.cat([graph_out, out[b]]))
                # outs.append(self.fc(torch.matmul(self.wave2graph(x[b]), out[b])))
                # outs.append(torch.add(out[b], torch.mean(self.wave2graph(x[b]), dim=0, keepdim=False), alpha=0.3))
                # outs.append(torch.add(out[b], self.fc(torch.matmul(self.wave2graph(x[b]), out[b])), alpha=0.3))
            out = torch.stack(outs)
            '''
            graph_out = self.wave2graph(x) # B, num_nodes, out_dim
            # graph_out = torch.mean(graph_out, dim=1, keepdim=False)
            out = torch.cat((graph_out, effnet_out), dim=1)
            # out = torch.einsum('bne,be->bn', graph_out, effnet_out)
        else:
            out = effnet_out
        pred_out = self.fc(out)
        if self.weakly_sup:
            pred_out = pred_out.reshape(B,-1,num_sam)
            # pred_out_weakly_sup = torch.argmax(pred_out, dim=1).squeeze() #new shape: B, 30
            pred_out_weakly_sup = torch.clone(pred_out) #new shape: B, 30
            if self.weakly_sup_mode == 'softmax':
                softmax_weight = torch.nn.functional.softmax(pred_out, dim=-1)
                pred_out = torch.sum(pred_out * softmax_weight, dim=-1)
            else:
                pred_out = torch.max(pred_out, dim=-1)[0]
                # pred_out = torch.mean(pred_out, dim=-1)
        if extract_embedding:
            if self.wave2graph_in > 0:
                return pred_out, graph_out, effnet_out
            else:
                return pred_out, pred_out_weakly_sup
        else:
            if self.multi_task:
                second_out = self.second_task_fc(out)
                third_out = self.third_task_fc(out)
                if self.multi_task_reverse_grad:
                    second_out = grad_reverse(second_out)
                    third_out = grad_reverse(third_out)
                return pred_out, second_out, third_out
            return pred_out

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        #return [p for p in audio_model.parameters() if p.requires_grad]
        params_lst = [
            # {'params': [p for p in self.effnet.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.avgpool.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.attention.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
        ]
        if self.wave2graph_in > 0:
            params_lst.append({'params': [p for p in self.wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay})

        return params_lst


class EffNetMean(nn.Module):
    def __init__(self, label_dim=527, level=0, pretrain=True, wave2graph_in=0):
        super(EffNetMean, self).__init__()
        self.middim = [1280, 1280, 1408, 1536, 1792, 2048, 2304, 2560]
        if pretrain == False:
            print('not using imagenet pretrained network')
            self.effnet = EfficientNet.from_name('efficientnet-b'+str(level), in_channels=1)
        else:
            print('using imagenet pretrained network')
            self.effnet = EfficientNet.from_pretrained('efficientnet-b'+str(level), in_channels=1)
        self.attention = MeanPooling(
            self.middim[level],
            label_dim,
            att_activation='sigmoid',
            cla_activation='sigmoid')
        self.avgpool = nn.AvgPool2d((4, 1))
        self.effnet._fc = nn.Identity()

        self.wave2graph_in = wave2graph_in
        if wave2graph_in > 0:
            self.wave2graph = Wave2Graph(nfeat=wave2graph_in, hidden_dim=128, nclass=label_dim, dropout=0.5)
            self.fc = nn.Linear(128*label_dim + label_dim, label_dim)

    def forward(self, x):
        if x.shape[2] < x.shape[1]:
            x = x.transpose(1, 2)

        if x.dim() == 3:
            f = x.unsqueeze(1)
        else:
            f = x
        f = self.effnet.extract_features(f)
        f = self.avgpool(f)
        f = f.transpose(2, 3)
        out = self.attention(f)
        if self.wave2graph_in > 0:
            outs = []
            for b in range(x.shape[0]):
                # outs.append(self.fc(torch.cat([torch.flatten(self.wave2graph(x[b])), out[b]])))
                #outs.append(self.fc(torch.cat([torch.mean(self.wave2graph(x[b]), dim=0, keepdim=False), out[b]])))
                #outs.append(self.fc(torch.matmul(self.wave2graph(x[b]), out[b])))
                outs.append(torch.add(out[b], torch.mean(self.wave2graph(x[b]), dim=0, keepdim=False), alpha=0.3))
                #outs.append(torch.add(out[b], self.fc(torch.matmul(self.wave2graph(x[b]), out[b])), alpha=0.3))
            out = torch.stack(outs)
        return out

    def get_config_optim(self, lr, weight_decay=0.0, lrp=10):
        #return [p for p in audio_model.parameters() if p.requires_grad]
        return [
            {'params': [p for p in self.effnet.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.avgpool.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.attention.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.wave2graph.parameters() if p.requires_grad], 'lr': lr*lrp, "weight_decay": weight_decay},
        ]


# Temporal w2g
class EffNetMean_v3(nn.Module):
    def __init__(self, label_dim=527, level=0, pretrain=True, chunk_size=128, step_size=64, hidden_dim=512, graph_out=128, rnn_hiddem_dim=256, rnn_output_dim=256, rnn_num_layers=2, dropout=0.8):
        super(EffNetMean_v3, self).__init__()
        self.middim = [1280, 1280, 1408, 1536, 1792, 2048, 2304, 2560]
        if pretrain == False:
            print('not using imagenet pretrained network')
            self.effnet = EfficientNet.from_name('efficientnet-b'+str(level), in_channels=1)
        else:
            print('using imagenet pretrained network')
            self.effnet = EfficientNet.from_pretrained('efficientnet-b'+str(level), in_channels=1)
        self.attention = MeanPooling(
            self.middim[level],
            256,
            att_activation='sigmoid',
            cla_activation='sigmoid')
        self.avgpool = nn.AvgPool2d((4, 1))
        self.effnet._fc = nn.Identity()

        self.temp_wave2graph = GRU_GCN(chunk_size, step_size, hidden_dim, graph_out, rnn_hiddem_dim, rnn_output_dim, rnn_num_layers, dropout)
        # self.fc = nn.Linear(256 + self.middim[level], self.middim[level])
        # self.fc = nn.Linear(128, label_dim)
        self.fc = nn.Sequential(
            nn.Linear(512, 128),
            nn.ReLU(),
            nn.Dropout(0.1),
            nn.Linear(128, label_dim),
            # nn.Softmax(dim=1)
        )

    def forward(self, x, extract_embedding=False):
        if x.shape[2] < x.shape[1]:
            x = x.transpose(1, 2)
        # x: B, mel_bin, time_len
        if x.dim() == 3:
            f = x.unsqueeze(1)
        else:
            f = x
        f = self.effnet.extract_features(f)
        # print(f.shape)
        # f = self.avgpool(f)
        # f = f.transpose(2, 3)
        f = torch.mean(f, dim=-1)
        f = f.unsqueeze(dim=-1)
        # print(f.shape)
        effnet_out = self.attention(f)
        # print(out.shape)
        graph_out = self.temp_wave2graph(x)
        
        out = torch.cat((graph_out, effnet_out), dim=1)
        pred_out = self.fc(out)
        # if self.count % 100 == 0:
        #     print(pred_out, out)
        if extract_embedding:
            return pred_out, graph_out, effnet_out
        else:
            return pred_out

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        #return [p for p in audio_model.parameters() if p.requires_grad]
        params_lst = [
            {'params': [p for p in self.effnet.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.avgpool.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.attention.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.temp_wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
        ]

        return params_lst


# Temporal CNN w2g
class EffNetMean_v4(nn.Module):
    def __init__(self, label_dim=527, level=0, pretrain=True, chunk_size=128, step_size=64, hidden_dim=512, graph_in=512, graph_out=256, dropout=0.8):
        super(EffNetMean_v4, self).__init__()
        self.middim = [1280, 1280, 1408, 1536, 1792, 2048, 2304, 2560]
        if pretrain == False:
            print('not using imagenet pretrained network')
            self.effnet = EfficientNet.from_name('efficientnet-b'+str(level), in_channels=1)
        else:
            print('using imagenet pretrained network')
            self.effnet = EfficientNet.from_pretrained('efficientnet-b'+str(level), in_channels=1)
        self.attention = MeanPooling(
            self.middim[level],
            256,
            att_activation='sigmoid',
            cla_activation='sigmoid')
        self.avgpool = nn.AvgPool2d((4, 1))
        self.effnet._fc = nn.Identity()

        self.temp_wave2graph = TempCNN_GCN(chunk_size, step_size, hidden_dim, graph_in, graph_out, dropout)
        # self.fc = nn.Linear(256 + self.middim[level], self.middim[level])
        # self.fc = nn.Linear(128, label_dim)
        self.fc = nn.Sequential(
            nn.Linear(512, 128),
            nn.ReLU(),
            nn.Dropout(0.1),
            nn.Linear(128, label_dim),
            # nn.Softmax(dim=1)
        )

    def forward(self, x, extract_embedding=False):
        if x.shape[2] < x.shape[1]:
            x = x.transpose(1, 2)
        # x: B, mel_bin, time_len
        if x.dim() == 3:
            f = x.unsqueeze(1)
        else:
            f = x
        f = self.effnet.extract_features(f)
        # print(f.shape)
        # f = self.avgpool(f)
        # f = f.transpose(2, 3)
        f = torch.mean(f, dim=-1)
        f = f.unsqueeze(dim=-1)
        # print(f.shape)
        effnet_out = self.attention(f)
        # print(out.shape)
        graph_out = self.temp_wave2graph(x)
        
        out = torch.cat((graph_out, effnet_out), dim=1)
        pred_out = self.fc(out)
        # if self.count % 100 == 0:
        #     print(pred_out, out)
        if extract_embedding:
            return pred_out, graph_out, effnet_out
        else:
            return pred_out

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        #return [p for p in audio_model.parameters() if p.requires_grad]
        params_lst = [
            {'params': [p for p in self.effnet.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.avgpool.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.attention.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.temp_wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
        ]

        return params_lst


class EffNetMean_v2_mfcc(nn.Module):
    def __init__(self, label_dim=527, level=0, pretrain=True, wave2graph_in=0, hidden_dim=512, dropout=0.8):
        super(EffNetMean_v2_mfcc, self).__init__()
        self.middim = [1280, 1280, 1408, 1536, 1792, 2048, 2304, 2560]
        if pretrain == False:
            print('not using imagenet pretrained network')
            self.effnet = EfficientNet.from_name('efficientnet-b'+str(level), in_channels=1)
        else:
            print('using imagenet pretrained network')
            self.effnet = EfficientNet.from_pretrained('efficientnet-b'+str(level), in_channels=1)
        self.attention = MeanPooling(
            self.middim[level],
            256,
            att_activation='sigmoid',
            cla_activation='sigmoid')
        self.avgpool = nn.AvgPool2d((4, 1))
        self.effnet._fc = nn.Identity()

        self.wave2graph_in = wave2graph_in
        if wave2graph_in > 0:
            self.wave2graph = Wave2Graph_v2(nfeat=wave2graph_in, hidden_dim=hidden_dim, nclass=256, dropout=dropout)
            # self.fc = nn.Linear(256 + self.middim[level], self.middim[level])
            # self.fc = nn.Linear(128, label_dim)
            self.fc = nn.Sequential(
                # nn.Linear(512, 128),
                # nn.ReLU(),
                # nn.Dropout(0.1),
                # nn.Linear(128, label_dim),
                nn.Linear(512, label_dim),
            )
        else:
            self.fc = nn.Sequential(
                nn.Linear(256, label_dim),
            )
        self.count = 0

    def forward(self, x, extract_embedding=False):
        x, mfcc, corr = x
        mfcc = torch.Tensor(mfcc).float().to(x.device, non_blocking=True)
        corr = torch.Tensor(corr).float().to(x.device, non_blocking=True)

        if x.shape[2] < x.shape[1]:
            x = x.transpose(1, 2)

        if x.dim() == 3:
            f = x.unsqueeze(1)
        else:
            f = x
        f = self.effnet.extract_features(f)
        # print(f.shape)
        # f = self.avgpool(f)
        # f = f.transpose(2, 3)
        f = torch.mean(f, dim=-1)
        f = f.unsqueeze(dim=-1)
        # print(f.shape)
        effnet_out = self.attention(f)
        # print(out.shape)
        if self.wave2graph_in > 0:
            graph_out = self.wave2graph(mfcc, corr) # B, num_nodes, out_dim
            # graph_out = torch.mean(graph_out, dim=1, keepdim=False)
            out = torch.cat((graph_out, effnet_out), dim=1)
            # out = torch.einsum('bne,be->bn', graph_out, effnet_out)
        else:
            out = effnet_out
        pred_out = self.fc(out)
        self.count += 1
        # if self.count % 100 == 0:
        #     print(pred_out, out)
        if extract_embedding:
            if self.wave2graph_in > 0:
                return pred_out, graph_out, effnet_out
            else:
                return pred_out, effnet_out, effnet_out
        else:
            return pred_out

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        #return [p for p in audio_model.parameters() if p.requires_grad]
        params_lst = [
            {'params': [p for p in self.effnet.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.avgpool.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.attention.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
        ]
        if self.wave2graph_in > 0:
            params_lst.append({'params': [p for p in self.wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay})

        return params_lst


class GraphOnlyMFCCNet(nn.Module):
    def __init__(self, label_dim=6, wave2graph_in=512, hidden_dim=512, dropout=0.8, graph_n_hidden_dim=1, adj_threshold=-1.0, inner_dropout=0.0, add_self_loops=True, gat_n_head=1):
        super(GraphOnlyMFCCNet, self).__init__()
        self.wave2graph = GAT(
            nfeat=wave2graph_in, 
            hidden_dim=hidden_dim, 
            nclass=256, 
            dropout=dropout,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head
        )
        self.fc = nn.Sequential(
            nn.Linear(256, label_dim),
        )
    def forward(self, x, extract_embedding=False):
        x, mfcc, corr = x
        mfcc = torch.Tensor(mfcc).float().to(x.device, non_blocking=True)
        corr = torch.Tensor(corr).float().to(x.device, non_blocking=True)

        graph_out = self.wave2graph(mfcc, corr) # B, num_nodes, out_dim
        pred_out = self.fc(graph_out)
        if extract_embedding:
            return pred_out, graph_out, graph_out
        else:
            return pred_out

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        params_lst = [
            {'params': [p for p in self.wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
        ]
        return params_lst


# MFCC + corr -> GNN -> graph_emb -> Transformer
class TemporalGraphOnlyMFCCNet(nn.Module):
    def __init__(self, label_dim=6, wave2graph_in=512, hidden_dim=512, dropout=0.8, graph_n_hidden_dim=1, adj_threshold=-1.0, inner_dropout=0.0, add_self_loops=True, 
    gat_n_head=1, graph_out_dim=256):
        super(TemporalGraphOnlyMFCCNet, self).__init__()
        self.graph_out_dim = graph_out_dim
        self.wave2graph = GAT(
            nfeat=wave2graph_in, 
            hidden_dim=hidden_dim, 
            nclass=self.graph_out_dim, 
            dropout=dropout,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head
        )
        self.transformer_encoder_layer = torch.nn.TransformerEncoderLayer(d_model=self.graph_out_dim, dim_feedforward=self.graph_out_dim*2, nhead=4, batch_first=True)
        self.transformer_encoder = torch.nn.TransformerEncoder(self.transformer_encoder_layer, num_layers=4)
        self.fc = nn.Sequential(
            nn.Linear(self.graph_out_dim, label_dim),
        )

    def forward(self, x, extract_embedding=False):
        x, mfcc, corr, padding_mask = x
        mfcc = torch.Tensor(mfcc).float().to(x.device, non_blocking=True)
        corr = torch.Tensor(corr).float().to(x.device, non_blocking=True)

        batch_size = mfcc.shape[0]
        mfcc = mfcc.reshape(-1, mfcc.shape[2], mfcc.shape[3])
        corr = corr.reshape(-1, corr.shape[2], corr.shape[3])

        graph_out = self.wave2graph(mfcc, corr) # B, num_nodes, out_dim
        graph_out = graph_out.reshape(batch_size, -1, self.graph_out_dim)
        graph_out = torch.nan_to_num(graph_out, 0.0)
        # import pdb; pdb.set_trace()

        transformer_out = self.transformer_encoder(graph_out, src_key_padding_mask=padding_mask)
        transformer_out = torch.mean(transformer_out, dim=1)

        pred_out = self.fc(transformer_out)
        # import pdb; pdb.set_trace()
        if extract_embedding:
            return pred_out, graph_out, graph_out
        else:
            return pred_out

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        params_lst = [
            {'params': [p for p in self.wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.transformer_encoder.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
        ]
        return params_lst


# MFCC + corr -> GNN -> graph_emb + pos_emb -> Transformer
class TemporalGraphOnlyMFCCNetPosEmb(nn.Module):
    def __init__(self, label_dim=6, wave2graph_in=512, hidden_dim=512, dropout=0.8, graph_n_hidden_dim=1, adj_threshold=-1.0, inner_dropout=0.0, add_self_loops=True, 
    gat_n_head=1, graph_out_dim=256):
        super(TemporalGraphOnlyMFCCNetPosEmb, self).__init__()
        self.graph_out_dim = graph_out_dim
        self.wave2graph = GAT(
            nfeat=wave2graph_in, 
            hidden_dim=hidden_dim, 
            nclass=self.graph_out_dim, 
            dropout=dropout,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head
        )
        self.pos_embed = nn.Parameter(torch.zeros(1, 1024, self.graph_out_dim))
        trunc_normal_(self.pos_embed, std=.02)
        self.transformer_encoder_layer = torch.nn.TransformerEncoderLayer(d_model=self.graph_out_dim, dim_feedforward=self.graph_out_dim*2, nhead=2, batch_first=True)
        self.transformer_encoder = torch.nn.TransformerEncoder(self.transformer_encoder_layer, num_layers=2)
        self.fc = nn.Sequential(
            nn.Linear(self.graph_out_dim, label_dim),
        )

    def forward(self, x, extract_embedding=False):
        x, mfcc, corr, padding_mask = x
        # B x Num_graph x 39 x len_time_stamp
        mfcc = torch.Tensor(mfcc).float().to(x.device, non_blocking=True)
        corr = torch.Tensor(corr).float().to(x.device, non_blocking=True)

        batch_size = mfcc.shape[0]
        mfcc = mfcc.reshape(-1, mfcc.shape[2], mfcc.shape[3])
        corr = corr.reshape(-1, corr.shape[2], corr.shape[3])

        graph_out = self.wave2graph(mfcc, corr) # Bxnum_graph, out_dim
        graph_out = graph_out.reshape(batch_size, -1, self.graph_out_dim)
        graph_out = torch.nan_to_num(graph_out, 0.0)
        # import pdb; pdb.set_trace()

        graph_out = graph_out + self.pos_embed[:,:graph_out.shape[1]]

        transformer_out = self.transformer_encoder(graph_out, src_key_padding_mask=padding_mask)
        transformer_out = torch.mean(transformer_out, dim=1)

        pred_out = self.fc(transformer_out)
        # import pdb; pdb.set_trace()
        if extract_embedding:
            return pred_out, graph_out, graph_out
        else:
            return pred_out

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        params_lst = [
            # {'params': [p for p in self.wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.transformer_encoder.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.parameters() if p.requires_grad], 'lr': lr, 'weight_decay': weight_decay}
        ]
        return params_lst


class PatchEmbed(nn.Module):
    def __init__(self, patch_size=16, stride=10, in_chans=3, embed_dim=768):
        super().__init__()

        patch_size = (patch_size, patch_size)
        stride = (stride, stride)

        self.proj = nn.Conv2d(in_chans, embed_dim, kernel_size=patch_size, stride=stride)

    def forward(self, x):
        x = self.proj(x).flatten(2).transpose(1, 2)
        return x


# mel-spec -> CNN -> patches + pos_emb -> Transformer
class AST(nn.Module):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256, num_layers=2, num_heads=2):
        super(AST, self).__init__()

        self.transformer_in_dim = in_dim
        self.out_dim = out_dim
        self.pos_embed = nn.Parameter(torch.zeros(1, 4096, self.transformer_in_dim))
        trunc_normal_(self.pos_embed, std=.02)

        self.patch_embed = PatchEmbed(patch_size, stride, 1, self.transformer_in_dim)

        self.transformer_encoder_layer = TransformerEncoderLayer(d_model=self.transformer_in_dim, dim_feedforward=self.transformer_in_dim*4, nhead=num_heads, batch_first=True)
        self.transformer_encoder = TransformerEncoder(self.transformer_encoder_layer, num_layers=num_layers)
        self.fc = nn.Sequential(
            nn.Linear(self.transformer_in_dim, label_dim),
        )

    def forward(self, x, extract_embedding=False):
        # B x len_time_stamp x 128
        x = x.unsqueeze(1)

        x = self.patch_embed(x)

        x = x + self.pos_embed[:,:x.shape[1]]

        x, attn_hm = self.transformer_encoder(x)

        x = torch.mean(x, dim=1)

        x = self.fc(x)

        if extract_embedding:
            return x, attn_hm
        else:
            return x

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        params_lst = [
            # {'params': [p for p in self.wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.transformer_encoder.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.parameters() if p.requires_grad], 'lr': lr, 'weight_decay': weight_decay}
        ]
        return params_lst


# mel-spec -> GNN -> patches + pos_emb -> Transformer
class AST_GNN(AST):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256, graph_n_hidden_dim=0, adj_threshold=-1, inner_dropout=0, add_self_loops=True, gat_n_head=2, temporal_patch_size=None, spatial_patch_size=None, num_layers=2, num_heads=2):
        super(AST_GNN, self).__init__(num_layers=num_layers, num_heads=num_heads)

        self.patch_size = patch_size
        self.stride = stride

        new_proj = GAT(
            nfeat=patch_size, 
            hidden_dim=256, 
            nclass=self.transformer_in_dim, 
            dropout=0.2,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head
        )

        self.patch_embed = new_proj
        self.temporal_patch_size = temporal_patch_size
        self.spatial_patch_size = spatial_patch_size

    def forward(self, x, extract_embedding=False):
        # B x len_time_stamp x 128
        x, corr = x
        B = x.shape[0]

        if self.temporal_patch_size:
            x = x.reshape(-1, self.spatial_patch_size, self.temporal_patch_size)
            corr = corr.reshape(-1, self.spatial_patch_size, self.spatial_patch_size)
        else:
            x = x.reshape(-1, self.patch_size, self.patch_size)
            corr = corr.reshape(-1, self.patch_size, self.patch_size)
        x = self.patch_embed(x, corr)

        x = x.reshape(B, -1, self.transformer_in_dim)

        x = x + self.pos_embed[:,:x.shape[1]]

        x = self.transformer_encoder(x)

        x = torch.mean(x, dim=1)

        x = self.fc(x)

        return x


# mel-spec -> GNN -> patches + pos_emb -> Transformer
class AST_GNN_v2(AST):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256, graph_n_hidden_dim=0, adj_threshold=-1, inner_dropout=0, add_self_loops=True, gat_n_head=2, temporal_patch_size=None, spatial_patch_size=None, num_layers=2, num_heads=2):
        super(AST_GNN_v2, self).__init__(num_layers=num_layers, num_heads=num_heads)

        self.patch_size = patch_size
        self.stride = stride

        new_proj = GAT_v2(
            nfeat=patch_size, 
            hidden_dim=256, 
            nclass=self.transformer_in_dim, 
            dropout=0.2,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head,
            n_node=spatial_patch_size,
        )

        self.patch_embed = new_proj
        self.temporal_patch_size = temporal_patch_size
        self.spatial_patch_size = spatial_patch_size

    def forward(self, x, extract_embedding=False):
        # B x len_time_stamp x 128
        x, corr = x
        B = x.shape[0]

        if self.temporal_patch_size:
            x = x.reshape(-1, self.spatial_patch_size, self.temporal_patch_size)
            corr = corr.reshape(-1, self.spatial_patch_size, self.spatial_patch_size)
        else:
            x = x.reshape(-1, self.patch_size, self.patch_size)
            corr = corr.reshape(-1, self.patch_size, self.patch_size)
        x = self.patch_embed(x, corr)

        x = x.reshape(B, -1, self.transformer_in_dim)

        x = x + self.pos_embed[:,:x.shape[1]]

        x = self.transformer_encoder(x)

        x = torch.mean(x, dim=1)

        x = self.fc(x)

        return x


# mel-spec -> GNN -> patches + pos_emb -> Transformer
# with class tokens
class AST_GNN_v3(AST):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256, graph_n_hidden_dim=0, adj_threshold=-1, inner_dropout=0, add_self_loops=True, gat_n_head=2, temporal_patch_size=None, spatial_patch_size=None, num_layers=2, num_heads=2):
        super(AST_GNN_v3, self).__init__(num_layers=num_layers, num_heads=num_heads)

        self.patch_size = patch_size
        self.stride = stride

        new_proj = GAT_v2(
            nfeat=patch_size, 
            hidden_dim=256, 
            nclass=self.transformer_in_dim, 
            dropout=0.2,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head,
            n_node=spatial_patch_size,
        )

        self.patch_embed = new_proj
        self.temporal_patch_size = temporal_patch_size
        self.spatial_patch_size = spatial_patch_size

        self.cls_token = nn.Parameter(torch.zeros(1, 1, self.transformer_in_dim))
        nn.init.normal_(self.cls_token, std=1e-6)

    def forward(self, x, extract_embedding=False):
        # B x len_time_stamp x 128
        x, corr = x
        B = x.shape[0]

        if self.temporal_patch_size:
            x = x.reshape(-1, self.spatial_patch_size, self.temporal_patch_size)
            corr = corr.reshape(-1, self.spatial_patch_size, self.spatial_patch_size)
        else:
            x = x.reshape(-1, self.patch_size, self.patch_size)
            corr = corr.reshape(-1, self.patch_size, self.patch_size)

        x = self.patch_embed(x, corr)

        x = x.reshape(B, -1, self.transformer_in_dim)

        x = torch.cat((self.cls_token.expand(x.shape[0], -1, -1), x), dim=1) # class token has postional embedding
        x = x + self.pos_embed[:,:x.shape[1]]

        x = self.transformer_encoder(x)

        # x = torch.mean(x, dim=1)
        x = x[:,0]

        x = self.fc(x)

        return x


# mel-spec -> GNN -> patches + pos_emb -> Transformer
# with class tokens
class AST_GNN_v4(AST):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256, graph_n_hidden_dim=0, adj_threshold=-1, inner_dropout=0, add_self_loops=True, gat_n_head=2, temporal_patch_size=None, spatial_patch_size=None, num_layers=2, num_heads=2):
        super(AST_GNN_v4, self).__init__(num_layers=num_layers, num_heads=num_heads)

        self.patch_size = patch_size
        self.stride = stride

        new_proj = GAT_v3(
            nfeat=patch_size, 
            hidden_dim=256, 
            nclass=self.transformer_in_dim, 
            dropout=0.2,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head,
            n_node=spatial_patch_size,
        )

        self.patch_embed = new_proj
        self.temporal_patch_size = temporal_patch_size
        self.spatial_patch_size = spatial_patch_size

        self.cls_token = nn.Parameter(torch.zeros(1, 1, self.transformer_in_dim))
        nn.init.normal_(self.cls_token, std=1e-6)

    def forward(self, x, extract_embedding=False):
        # B x len_time_stamp x 128
        x, corr = x
        B = x.shape[0]

        if self.temporal_patch_size:
            x = x.reshape(-1, self.spatial_patch_size, self.temporal_patch_size)
            corr = corr.reshape(-1, self.spatial_patch_size, self.spatial_patch_size)
        else:
            x = x.reshape(-1, self.patch_size, self.patch_size)
            corr = corr.reshape(-1, self.patch_size, self.patch_size)

        x = self.patch_embed(x, corr)

        x = x.reshape(B, -1, self.transformer_in_dim)

        x = torch.cat((self.cls_token.expand(x.shape[0], -1, -1), x), dim=1) # class token has postional embedding
        x = x + self.pos_embed[:,:x.shape[1]]

        x = self.transformer_encoder(x)

        # x = torch.mean(x, dim=1)
        x = x[:,0]

        x = self.fc(x)

        return x


# mel-spec -> GNN (new mean) -> patches + pos_emb -> Transformer
# with class tokens
class AST_GNN_v5(AST):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256, graph_n_hidden_dim=0, adj_threshold=-1, inner_dropout=0, add_self_loops=True, gat_n_head=2, temporal_patch_size=None, spatial_patch_size=None, num_layers=2, num_heads=2):
        super(AST_GNN_v5, self).__init__(num_layers=num_layers, num_heads=num_heads)

        self.patch_size = patch_size
        self.stride = stride

        new_proj = GAT_v4(
            nfeat=patch_size, 
            hidden_dim=64, 
            nclass=64, 
            dropout=0.2,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head,
            n_node=spatial_patch_size,
        )

        self.patch_embed = new_proj
        self.temporal_patch_size = temporal_patch_size
        self.spatial_patch_size = spatial_patch_size

        self.cls_token = nn.Parameter(torch.zeros(1, 1, self.transformer_in_dim))
        nn.init.normal_(self.cls_token, std=1e-6)

    def forward(self, x, extract_embedding=False):
        # B x len_time_stamp x 128
        x, corr = x
        B = x.shape[0]

        if self.temporal_patch_size:
            # import pdb; pdb.set_trace()
            x = x.reshape(-1, self.spatial_patch_size, self.temporal_patch_size)
            corr = corr.reshape(-1, self.spatial_patch_size, self.spatial_patch_size)
        else:
            x = x.reshape(-1, self.patch_size, self.patch_size)
            corr = corr.reshape(-1, self.patch_size, self.patch_size)

        x = self.patch_embed(x, corr)

        x = x.reshape(B, -1, self.transformer_in_dim)

        x = torch.cat((self.cls_token.expand(x.shape[0], -1, -1), x), dim=1) # class token has postional embedding
        x = x + self.pos_embed[:,:x.shape[1]]

        x = self.transformer_encoder(x)

        # x = torch.mean(x, dim=1)
        x = x[:,0]

        x = self.fc(x)

        return x


# mel-spec -> GNN (new mean) -> patches + pos_emb -> Transformer
# with class tokens
class AST_GNN_v6(AST):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256, graph_n_hidden_dim=0, adj_threshold=-1, inner_dropout=0, add_self_loops=True, gat_n_head=2, temporal_patch_size=None, spatial_patch_size=None, num_layers=2, num_heads=2):
        super(AST_GNN_v6, self).__init__(num_layers=num_layers, num_heads=num_heads)

        self.patch_size = patch_size
        self.stride = stride

        new_proj = GAT_v4(
            nfeat=patch_size, 
            hidden_dim=256, 
            nclass=256, 
            dropout=0.2,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head,
            n_node=spatial_patch_size,
        )

        self.patch_embed = new_proj
        self.temporal_patch_size = temporal_patch_size
        self.spatial_patch_size = spatial_patch_size

        self.cls_token = nn.Parameter(torch.zeros(1, 1, self.transformer_in_dim))
        nn.init.normal_(self.cls_token, std=1e-6)

    def forward(self, x, extract_embedding=False):
        # B x len_time_stamp x 128
        x, corr = x
        B = x.shape[0]

        if self.temporal_patch_size:
            # import pdb; pdb.set_trace()
            # x = x.reshape(-1, self.spatial_patch_size, self.temporal_patch_size)
            # corr = corr.reshape(-1, self.spatial_patch_size, self.spatial_patch_size)
            # x = self.patch_embed(x, corr)
            outputs = []
            for idx in range(x.size(0)):
                element = x.index_select(0, torch.tensor(idx).to('cuda')).squeeze()
                element_corr = corr.index_select(0, torch.tensor(idx).to('cuda')).squeeze()
                # import pdb; pdb.set_trace()
                # Apply your_function to the element
                output_element = self.patch_embed(element, element_corr)

                # Append the output_element to the outputs list
                outputs.append(output_element)

            # Concatenate the outputs along the specified dimension
            # import pdb; pdb.set_trace()
            output_tensor = torch.stack(outputs, dim=0)
            x = output_tensor

        else:
            x = x.reshape(-1, self.patch_size, self.patch_size)
            corr = corr.reshape(-1, self.patch_size, self.patch_size)
            x = self.patch_embed(x, corr)
            x = x.reshape(B, -1, self.transformer_in_dim)

        x = torch.cat((self.cls_token.expand(x.shape[0], -1, -1), x), dim=1) # class token has postional embedding
        x = x + self.pos_embed[:,:x.shape[1]]

        x = self.transformer_encoder(x)

        # x = torch.mean(x, dim=1)
        x = x[:,0]

        x = self.fc(x)

        return x


# same as AST_GNN_v6
# currently use for Urbansound8k
# follow hts-at, batchnorm instead of stats  
class AST_GNN_v7(AST):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256, graph_n_hidden_dim=0, adj_threshold=-1, inner_dropout=0, add_self_loops=True, gat_n_head=2, temporal_patch_size=None, spatial_patch_size=None, num_layers=2, num_heads=2):
        super(AST_GNN_v7, self).__init__(num_layers=num_layers, num_heads=num_heads, label_dim=label_dim)

        self.patch_size = patch_size
        self.stride = stride

        new_proj = GAT_v4(
            nfeat=patch_size, 
            hidden_dim=256, 
            nclass=256, 
            dropout=0.2,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head,
            n_node=spatial_patch_size,
        )

        self.patch_embed = new_proj
        self.temporal_patch_size = temporal_patch_size
        self.spatial_patch_size = spatial_patch_size

        self.cls_token = nn.Parameter(torch.zeros(1, 1, self.transformer_in_dim))
        nn.init.normal_(self.cls_token, std=1e-6)

        self.bn0 = nn.BatchNorm1d(128)

    def forward(self, x, extract_embedding=False):
        # B x len_time_stamp x 128
        x, corr = x
        B = x.shape[0]

        if self.temporal_patch_size:
            # import pdb; pdb.set_trace()
            x = x.reshape(-1, self.spatial_patch_size, self.temporal_patch_size)
            corr = corr.reshape(-1, self.spatial_patch_size, self.spatial_patch_size)
            x = self.bn0(x)
            corr = torch.stack([torch.corrcoef(temp) for temp in torch.unbind(x, dim=0)], axis=0)
        else:
            x = x.reshape(-1, self.patch_size, self.patch_size)
            corr = corr.reshape(-1, self.patch_size, self.patch_size)

        x = self.patch_embed(x, corr)
        x = x.reshape(B, -1, self.transformer_in_dim)

        x = torch.cat((self.cls_token.expand(x.shape[0], -1, -1), x), dim=1) # class token has postional embedding
        x = x + self.pos_embed[:,:x.shape[1]]

        x = self.transformer_encoder(x)

        # x = torch.mean(x, dim=1)
        x = x[:,0]

        x = self.fc(x)

        return x

# same as AST_GNN_v6
# follow hts-at, batchnorm instead of stats, and split into patches in the forward function
class AST_GNN_virtual_node(AST):
    def __init__(self, label_dim=6, graph_n_hidden_dim=0, adj_threshold=-1, inner_dropout=0, add_self_loops=True, gat_n_head=2, temporal_patch_size=None, spatial_patch_size=None, temporal_stride=None, spatial_stride=None, num_layers=2, num_heads=2, in_dim=256, graph_arch='v1', fused_with_cnn=None, multi_res=False, scale_multi_res=1.0, fused_with_effnet=False, patch_mask=-1, use_drloc=False, drloc_mode="l1", sample_size=32, use_abs=False,):
        super(AST_GNN_virtual_node, self).__init__(num_layers=num_layers, num_heads=num_heads, label_dim=label_dim, in_dim=in_dim)

        self.temporal_patch_size = temporal_patch_size
        self.spatial_patch_size = spatial_patch_size
        self.temporal_stride = temporal_stride
        self.spatial_stride = spatial_stride

        if graph_arch == 'v1': 
            graph_net = GAT_virtual_node
        elif graph_arch == 'v2':
            graph_net = GAT_virtual_node_v2
        elif graph_arch == 'v3':
            graph_net = GAT_virtual_node_v3
        elif graph_arch == 'v4':
            graph_net = GAT_virtual_node_v4
        elif graph_arch == 'v5':
            graph_net = GAT_virtual_node_v5
        elif graph_arch == 'v6':
            graph_net = GAT_virtual_node_v6
        elif graph_arch == 'v7':
            graph_net = GAT_virtual_node_v7
        elif graph_arch == 'v8':
            graph_net = GAT_virtual_node_v8
        elif graph_arch == 'gat_v2':
            graph_net = GAT_v2
        elif graph_arch == 'sep_emb_v1':
            graph_net = GNN_sep_emb
        elif graph_arch == 'sep_emb_v2':
            graph_net = GNN_sep_emb_v2
        elif graph_arch == 'sep_emb_v3':
            graph_net = GNN_sep_emb_v3
        elif graph_arch == 'sep_emb_v4':
            graph_net = GNN_sep_emb_v4
        elif graph_arch == 'sep_emb_v5':
            graph_net = GNN_sep_emb_v5
        elif graph_arch == 'sep_emb_v6':
            graph_net = GNN_sep_emb_v6
        elif graph_arch == 'only_gnn':
            graph_net = only_gnn
        elif graph_arch == 'only_gnn_decorr_adj':
            graph_net = only_gnn_decorr_adj
        elif graph_arch == 'only_gnn_fc_adj':
            graph_net = only_gnn_fc_adj
        elif graph_arch == 'only_gnn_v2':
            graph_net = only_gnn_v2
        elif graph_arch == 'only_gnn_v3':
            graph_net = only_gnn_v3
        elif graph_arch == 'only_gnn_no_diff_emb':
            graph_net = only_gnn_no_diff_emb
        elif graph_arch == 'only_emb':
            graph_net = only_emb

        if fused_with_cnn:
            if fused_with_cnn == 'v4':
                self.cnn_patch_embed = torch.nn.Sequential(
                    torch.nn.Conv1d(128, self.transformer_in_dim, kernel_size=self.temporal_patch_size, padding=0, stride=self.temporal_stride),
                    torch.nn.GELU(),
                )
            elif fused_with_cnn == 'v5':
                self.cnn_patch_embed = torch.nn.Sequential(
                    torch.nn.Conv1d(128, int(self.transformer_in_dim/2), kernel_size=self.temporal_patch_size, padding=0, stride=self.temporal_stride),
                    torch.nn.GELU(),
                )
                self.fused_fc = torch.nn.Linear(self.transformer_in_dim, self.transformer_in_dim)
            elif fused_with_cnn == 'v6': # should go with gat v7 
                self.cnn_patch_embed = torch.nn.Sequential(
                    torch.nn.Conv1d(128, self.transformer_in_dim, kernel_size=self.temporal_patch_size, padding=0, stride=self.temporal_stride),
                    torch.nn.GELU(),
                    torch.nn.BatchNorm1d(self.transformer_in_dim),
                )
            elif fused_with_cnn == 'v7': # should go with gat v7 
                self.cnn_patch_embed = torch.nn.Sequential(
                    torch.nn.Conv1d(128, int(self.transformer_in_dim/2), kernel_size=self.temporal_patch_size, padding=0, stride=self.temporal_stride),
                    torch.nn.GELU(),
                    torch.nn.BatchNorm1d(int(self.transformer_in_dim/2)),
                )
                self.fused_fc = torch.nn.Linear(self.transformer_in_dim, self.transformer_in_dim)
            elif fused_with_cnn == 'v8': # should go with gat v7 
                self.cnn_patch_embed = torch.nn.Sequential(
                    torch.nn.Conv1d(128, int(self.transformer_in_dim/2), kernel_size=self.temporal_patch_size, padding=0, stride=self.temporal_stride),
                    torch.nn.GELU(),
                    torch.nn.BatchNorm1d(int(self.transformer_in_dim/2)),
                )
                self.fused_fc = torch.nn.Identity()
            elif fused_with_cnn == 'v9': # should go with gat v6
                self.cnn_patch_embed = torch.nn.Sequential(
                    torch.nn.Conv1d(128, self.transformer_in_dim, kernel_size=self.temporal_patch_size, padding=0, stride=self.temporal_stride),
                    torch.nn.GELU(),
                )
                self.fused_fc = torch.nn.Linear(self.transformer_in_dim*2, self.transformer_in_dim)
            elif fused_with_cnn == 'v10': # should go with gat v7 
                self.cnn_patch_embed = torch.nn.Sequential(
                    torch.nn.Conv1d(128, self.transformer_in_dim, kernel_size=self.temporal_patch_size, padding=0, stride=self.temporal_stride),
                    torch.nn.GELU(),
                    torch.nn.BatchNorm1d(self.transformer_in_dim),
                )
                self.fused_fc = torch.nn.Linear(self.transformer_in_dim*2, self.transformer_in_dim)
            else:
                if fused_with_cnn == 'v1':
                    kernel_size=3
                    stride = 1
                    pad = 1
                elif fused_with_cnn == 'v2':
                    kernel_size=3
                    stride = 2
                    pad = 1
                elif fused_with_cnn == 'v3':
                    kernel_size=4
                    stride = 2
                    pad = 0
                self.cnn_patch_embed = torch.nn.Sequential(
                    torch.nn.Conv1d(128, self.transformer_in_dim, kernel_size=kernel_size, padding=pad, stride=stride),
                    torch.nn.GELU(),
                    torch.nn.Conv1d(self.transformer_in_dim, self.transformer_in_dim, kernel_size=3, stride=2, padding=pad),
                    torch.nn.GELU(),
                )

        self.fused_with_cnn = fused_with_cnn

        self.graph_dim = self.transformer_in_dim if fused_with_cnn not in ['v5', 'v7', 'v8'] else int(self.transformer_in_dim/2)
        new_proj = graph_net(
            nfeat=self.temporal_patch_size, 
            hidden_dim=self.graph_dim, 
            nclass=self.graph_dim, 
            dropout=0.2,
            n_hidden_dim=graph_n_hidden_dim, 
            adj_thresh=adj_threshold, 
            inner_dropout=inner_dropout, 
            add_self_loops=add_self_loops, 
            n_head=gat_n_head,
            n_node=spatial_patch_size,
        )

        self.patch_embed = new_proj

        self.cls_token = nn.Parameter(torch.zeros(1, 1, self.transformer_in_dim))
        nn.init.normal_(self.cls_token, std=1e-6)

        self.bn0 = nn.BatchNorm1d(128, affine=False)

        self.multi_res = multi_res
        self.scale_multi_res = scale_multi_res
        self.num_heads = num_heads

        if fused_with_effnet:
            self.middim = 1280
            self.effnet = EfficientNet.from_name('efficientnet-b0', in_channels=1)
            self.attention = MeanPooling(
                self.middim,
                self.transformer_in_dim,
                att_activation='sigmoid',
                cla_activation='sigmoid')
            self.avgpool = nn.AvgPool2d((4, 1))
            self.effnet._fc = nn.Identity()
            self.final_fc = nn.Sequential(
                nn.Linear(self.transformer_in_dim*2, label_dim),
            )
            self.effnet_norm = nn.LayerNorm(256, eps=1e-6)

        self.fused_with_effnet = fused_with_effnet

        self.patch_mask = patch_mask

        self.use_drloc = use_drloc
        if self.use_drloc:
            self.drloc = DenseRelativeLoc(
                in_dim=self.transformer_in_dim, 
                out_dim=1 if drloc_mode=="l1" else 14,
                sample_size=sample_size,
                drloc_mode=drloc_mode,
                use_abs=use_abs
            )

    def forward(self, x, extract_embedding=False):
        if self.fused_with_effnet:
            if x.shape[2] < x.shape[1]:
                f = x.transpose(1, 2)

            if x.dim() == 3:
                f = f.unsqueeze(1)
            else:
                f = x
            f = self.effnet.extract_features(f)
            f = torch.mean(f, dim=-1)
            f = f.unsqueeze(dim=-1)
            effnet_out = self.attention(f)
            effnet_out = self.effnet_norm(effnet_out)

        if self.multi_res:
            x, x_high_level = x
            x_high_level = x_high_level.reshape(x_high_level.shape[0], -1)
            x_high_level_1 = x_high_level.unsqueeze(-1) # B x num_tokens x 1
            x_high_level_2 = x_high_level_1.transpose(1,2) # B x 1 x num_tokens
            x_high_level = 0.5 - abs(x_high_level_1-x_high_level_2)/(x_high_level_1+x_high_level_2+1e-5)
            x_high_level = x_high_level / self.scale_multi_res
            m = torch.nn.ConstantPad2d((1,0,1,0),1)
            x_high_level = m(x_high_level)
        # B x len_time_stamp x 128
        B = x.shape[0]
        x = x.transpose(1,2)
        x = self.bn0(x)
        x = x.transpose(1,2)

        if self.fused_with_cnn:
            x_cnn = x.permute(0,2,1) # B X 128 x len_time_stamp
            x_cnn = self.cnn_patch_embed(x_cnn)
            x_cnn = x_cnn.permute(0,2,1) # B x reduced_time_stamp x in_dim

        x = x.unfold(1, self.temporal_patch_size, self.temporal_stride).unfold(2, self.spatial_patch_size, self.spatial_stride).reshape(-1, self.temporal_patch_size, self.spatial_patch_size)
        x = x.transpose(1,2)
        temp = x.clone()
        corr = torch.stack([torch.corrcoef(temp) for temp in torch.unbind(x, dim=0)], axis=0)
        # import pdb; pdb.set_trace()
        x = self.patch_embed(x, corr)
        if torch.isnan(x).any():
            import pdb; pdb.set_trace()
        x = x.reshape(B, -1, self.graph_dim)

        if self.fused_with_cnn:
            if self.fused_with_cnn in ['v1', 'v2']:
                x = x + x_cnn[:,:-int(self.temporal_patch_size/self.temporal_stride - 1),:]
            elif self.fused_with_cnn in ['v5', 'v7', 'v8', 'v9', 'v10']:
                # import pdb; pdb.set_trace()
                x = self.fused_fc(torch.concat((x, x_cnn), dim=-1))
            else:
                # import pdb; pdb.set_trace()
                x = x + x_cnn

        x = torch.cat((self.cls_token.expand(x.shape[0], -1, -1), x), dim=1) # class token has postional embedding
        x = x + self.pos_embed[:,:x.shape[1]]

        if self.patch_mask != -1:
            b_corr = corr.reshape(B,-1,128,128)
            mask = torch.isnan(b_corr).sum(dim=(2,3))/(128*128) > self.patch_mask
            m = torch.nn.ConstantPad1d((1,0), False)
            mask = m(mask)
        x, attn_hm = self.transformer_encoder(x, 
                                              attn_bias=x_high_level.repeat(self.num_heads,1,1) if self.multi_res else None,
                                              need_weights=True if extract_embedding else False,
                                              src_key_padding_mask=None if self.patch_mask == -1 else mask
                                              )
        outs = Munch()
        # SSUP
        if self.use_drloc:
            # import pdb; pdb.set_trace()
            x_last = x[:,1:] # B, L, C 
            x_last = x_last.transpose(1, 2) # [B, C, L]
            B, C, L = x_last.size()
            # H = W = int(math.sqrt(HW))
            # x_last = x_last.view(B, C, H, W) # [B, C, H, W]

            drloc_feats, deltaxy = self.drloc(x_last)
            outs.drloc = [drloc_feats]
            outs.deltaxy = [deltaxy]
            outs.plz = [L] # plane size 

        # import pdb; pdb.set_trace()
        x = x[:,0]

        if self.fused_with_effnet:
            # import pdb; pdb.set_trace()
            x = self.final_fc(torch.cat((x, effnet_out), dim=1))
        else:
            x = self.fc(x)

        if extract_embedding:
            # import pdb; pdb.set_trace()
            return x, attn_hm
        elif self.use_drloc:
            return x, outs
        else:
            return x

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        # this is to set specific learning rate for this layer:
        for p in self.patch_embed.parameters():
            p.requires_grad = False

        params_lst = [
            # {'params': [p for p in self.bn0.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.patch_embed.parameters() if p.requires_grad], 'lr': lr*lrp, "weight_decay": weight_decay},
            # {'params': [p for p in self.transformer_encoder.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.transformer_encoder_layer.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.pos_embed if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.cls_token if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.parameters() if p.requires_grad], 'lr': lr, 'weight_decay': weight_decay}
        ]

        for p in self.patch_embed.parameters():
            p.requires_grad = True
        params_lst.append(
            {'params': [p for p in self.patch_embed.parameters() if p.requires_grad], 'lr': lr*lrp, "weight_decay": weight_decay},
        )
        return params_lst


# mel-spec -> GNN -> patches + pos_emb -> Transformer
class Whisper_AE(AST):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256, num_layers=2, num_heads=2):
        super(Whisper_AE, self).__init__()

        # new_proj = torch.nn.Sequential(
        #     torch.nn.Conv1d(128, self.transformer_in_dim, kernel_size=3, padding=1),
        #     torch.nn.GELU(),
        #     torch.nn.Conv1d(self.transformer_in_dim, self.transformer_in_dim, kernel_size=3, stride=2, padding=1),
        #     torch.nn.GELU(),
        # )
        new_proj = torch.nn.Sequential(
            torch.nn.Conv1d(128, self.transformer_in_dim, kernel_size=16, padding=0, stride=4),
            torch.nn.GELU(),
        )

        self.patch_embed = new_proj

    def forward(self, x, extract_embedding=False):
        # B x len_time_stamp x 128
        # x, corr = x
        B = x.shape[0]
        x = x.permute(0,2,1) # B X 128 x len_time_stamp

        # import pdb; pdb.set_trace()
        x = self.patch_embed(x)

        x = x.permute(0,2,1) # B x reduced_time_stamp x in_dim

        x = x + self.pos_embed[:,:x.shape[1]]


        x = self.transformer_encoder(x)

        x = torch.mean(x, dim=1)

        x = self.fc(x)

        return x


class w2v2(nn.Module):
    def __init__(self, label_dim=6, in_dim = 256, patch_size=16, stride=10, out_dim=256):
        super(w2v2, self).__init__()

        self.w2v2_model = ta.models.wav2vec2_model(
            extractor_mode='layer_norm',
            extractor_conv_layer_config=[(in_dim, 20, 10), (in_dim, 10, 5), (in_dim, 10, 5)],
            extractor_conv_bias=True,
            encoder_embed_dim=in_dim,
            encoder_projection_dropout=0.1,
            encoder_pos_conv_kernel=128,
            encoder_pos_conv_groups=16,
            encoder_num_layers=2,
            encoder_num_heads=2,
            encoder_attention_dropout=0.1,
            encoder_ff_interm_features=in_dim*2,
            encoder_ff_interm_dropout=0.1,
            encoder_dropout=0.1,
            encoder_layer_norm_first=False,
            encoder_layer_drop=0.1,
            aux_num_out=256,
        )
        self.fc = nn.Sequential(
            nn.Linear(in_dim, label_dim),
        )

    def forward(self, x, extract_embedding=False):
        # B x num_frame
        x = self.w2v2_model(x)

        # import pdb; pdb.set_trace()
        x = x[0]
        x = torch.mean(x, dim=1)

        x = self.fc(x)

        return x

    def get_config_optim(self, lr, weight_decay=0.0, lrp=1):
        params_lst = [
            # {'params': [p for p in self.wave2graph.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.transformer_encoder.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            # {'params': [p for p in self.fc.parameters() if p.requires_grad], 'lr': lr, "weight_decay": weight_decay},
            {'params': [p for p in self.parameters() if p.requires_grad], 'lr': lr, 'weight_decay': weight_decay}
        ]
        return params_lst
