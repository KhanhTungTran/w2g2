import math
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
from torch.nn.modules.module import Module
from torch_geometric.nn.dense import DenseGCNConv, DenseGraphConv, DenseSAGEConv
#from torch_geometric.nn import global_mean_pool
#from torch_geometric.nn.conv.message_passing import MessagePassing
#from torch_geometric.utils import add_self_loops


import torchvision.transforms.functional as Ft

# Helper function for visualization.
#%matplotlib inline
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE

def visualize(h, color):
    z = TSNE(n_components=2).fit_transform(h.detach().cpu().numpy())

    plt.figure(figsize=(10,10))
    plt.xticks([])
    plt.yticks([])

    plt.scatter(z[:, 0], z[:, 1], s=70, c=color, cmap="Set2")
    plt.show()


class GraphConvolution(nn.Module):
    """
    Simple GCN layer, similar to https://arxiv.org/abs/1609.02907
    """

    def __init__(self, in_features, out_features, bias=False):
        super(GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(in_features, out_features))
        if bias:
            self.bias = Parameter(torch.Tensor(1, 1, out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input, adj):
        support = torch.matmul(input, self.weight)
        # deg_inv_sqrt = adj.sum(dim=-1).clamp(min=1).pow(-0.5)
        # adj = deg_inv_sqrt.unsqueeze(-1) * adj * deg_inv_sqrt.unsqueeze(-2)
        output = torch.matmul(adj, support)
        if self.bias is not None:
            return output + self.bias
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'


class GraphConvolution_v2(nn.Module):
    """
    Simple GCN layer, similar to https://arxiv.org/abs/1609.02907
    from: https://github.com/ReML-AI/MGTN/blob/76bf9ea1f036eec2374576f1d7509f8a2c5dd065/gnn.py#L7
    """

    def __init__(self, in_features, out_features, bias=False):
        super(GraphConvolution_v2, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(in_features, out_features))
        if bias:
            self.bias = Parameter(torch.Tensor(1, 1, out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input, adj):
        support = torch.matmul(input, self.weight)
        # output = torch.matmul(adj, support)

        adj = adj.clone()
        idx = torch.arange(adj.shape[1], dtype=torch.long, device=adj.device)
        adj[:, idx, idx] = 2

        deg_inv_sqrt = adj.sum(dim=-1).clamp(min=1).pow(-0.5)
        adj = deg_inv_sqrt.unsqueeze(-1) * adj * deg_inv_sqrt.unsqueeze(-2)
        output = torch.einsum('bnn,bnh->bnh',adj, support)
        if self.bias is not None:
            return output + self.bias
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'


# https://github.com/seongjunyun/Graph_Transformer_Networks/
def glorot(tensor):
    stdv = math.sqrt(6.0 / (tensor.size(-2) + tensor.size(-1)))
    if tensor is not None:
        tensor.data.uniform_(-stdv, stdv)


def zeros(tensor):
    if tensor is not None:
        tensor.data.fill_(0)

"""
class GCNConv2(MessagePassing):
    def __init__(self,
                 in_channels,
                 out_channels,
                 improved=False,
                 cached=False,
                 bias=True,
                 args=None):
        super(GCNConv2, self).__init__('add', flow='target_to_source')

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.improved = improved
        self.cached = cached
        self.cached_result = None

        self.weight = Parameter(torch.Tensor(in_channels, out_channels))

        if bias:
            self.bias = Parameter(torch.Tensor(out_channels))
        else:
            self.register_parameter('bias', None)

        self.args = args
        self.reset_parameters()

    def reset_parameters(self):
        glorot(self.weight)
        zeros(self.bias)
        self.cached_result = None


    @staticmethod
    def norm(edge_index, num_nodes, edge_weight, improved=False, dtype=None, args=None):
        if edge_weight is None:
            edge_weight = torch.ones((edge_index.size(1), ),
                                     dtype=dtype,
                                     device=edge_index.device)
        edge_weight = edge_weight.view(-1)
        assert edge_weight.size(0) == edge_index.size(1)

        edge_index, _ = add_self_loops(edge_index, num_nodes=num_nodes)
        
        loop_weight = torch.full((num_nodes, ),
                                1 if not args.remove_self_loops else 0,
                                dtype=edge_weight.dtype,
                                device=edge_weight.device)
        edge_weight = torch.cat([edge_weight, loop_weight], dim=0)

        row, col = edge_index
        
        # deg = scatter_add(edge_weight, col, dim=0, dim_size=num_nodes)
        deg = scatter_add(edge_weight, row, dim=0, dim_size=num_nodes)
        deg_inv_sqrt = deg.pow(-1)
        deg_inv_sqrt[deg_inv_sqrt == float('inf')] = 0

        # return edge_index, (deg_inv_sqrt[col] ** 0.5) * edge_weight * (deg_inv_sqrt[row] ** 0.5)
        return edge_index, deg_inv_sqrt[row] * edge_weight


    def forward(self, x, edge_index, edge_weight=None):
        """"""
        x = torch.matmul(x, self.weight)

        if not self.cached or self.cached_result is None:
            edge_index, norm = self.norm(edge_index, x.size(0), edge_weight,
                                         self.improved, x.dtype, args=self.args)
            self.cached_result = edge_index, norm
        edge_index, norm = self.cached_result

        return self.propagate(edge_index, x=x, norm=norm)


    def message(self, x_j, norm):
        return norm.view(-1, 1) * x_j

    def update(self, aggr_out):
        if self.bias is not None:
            aggr_out = aggr_out + self.bias
        return aggr_out

    def __repr__(self):
        return '{}({}, {})'.format(self.__class__.__name__, self.in_channels,
                                   self.out_channels)
"""

"""
https://github.com/tkipf/pygcn
"""
class GCN(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout):
        super(GCN, self).__init__()

        self.gc1 = GraphConvolution(nfeat, hidden_dim)
        self.gc2 = GraphConvolution(hidden_dim, nclass)
        self.dropout = dropout

    def forward(self, x, adj):
        x = F.relu(self.gc1(x, adj))
        x = F.dropout(x, self.dropout, training=self.training)
        x = self.gc2(x, adj)
        return F.log_softmax(x, dim=1)

"""
### https://colab.research.google.com/drive/1fLJbFPz0yMCQg81DdCP5I8jXw9LoggKO?usp=sharing#scrollTo=ExNsODp3rKjN
from torch_geometric.nn import GCNConv, global_mean_pool
from torch_geometric.datasets import TUDataset
        
class GCN2(torch.nn.Module):
    def __init__(self, nfeat: int, nclass: int, hidden_dim: int = 16, dropout: float = 0.5) -> None:
        super(GCN, self).__init__()
        torch.manual_seed(12345)
        self.conv1 = GCNConv(nfeat, hidden_dim)
        self.conv2 = GCNConv(hidden_dim, hidden_dim)
        self.conv3 = GCNConv(hidden_dim, hidden_channels)
        self.dropout = dropout
        self.linear = Linear(hidden_dim, nclass)

    def forward(self, x, edge_index, batch):
        # 1. Obtain node embeddings 
        x = self.conv1(x, edge_index).relu()
        x = self.conv2(x, edge_index)
        x = x.relu()
        x = self.conv3(x, edge_index)

        # 2. Readout layer
        x = global_mean_pool(x, batch)  # [batch_size, hidden_channels]

        # 3. Apply a final classifier
        x = F.dropout(x, p=self.dropout, training=self.training)
        x = self.linear(x)
        return F.log_softmax(x, dim=-1)

data = TUDataset(root='data/TUDataset', name='MUTAG').shuffle()

model = GCN2(nfeat=7, nclass=2, hidden_dim=64)
model.eval()
out = model(data.x, data.edge_index, data.batch)
#pred = out.argmax(dim=1)
pred = out.max(dim=1)[1]
visualize(pred, color=data.y)
"""

class Wave2Graph_v2(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5):
        super(Wave2Graph_v2, self).__init__()
        self.dropout = dropout
        # GraphConvolution, GCNConv, GCNConv2
        # graph_arch = GraphConvolution_v2
        graph_arch = DenseGraphConv
        self.gc1 = graph_arch(nfeat, int(hidden_dim/2))  # (feature_dim, node_dims)
        self.gc2 = graph_arch(int(hidden_dim/2), hidden_dim)
        self.gc3 = graph_arch(hidden_dim, nclass)
        # self.gc1 = GraphConvolution_v2(nfeat, hidden_dim)  # (feature_dim, node_dims)
        # self.gc2 = GraphConvolution_v2(hidden_dim, hidden_dim)
        # self.gc3 = GraphConvolution_v2(hidden_dim, nclass)
        # self.gc1 = GraphConvolution_v2(nfeat, hidden_dim)  # (feature_dim, node_dims)
        # self.gc2 = GraphConvolution_v2(hidden_dim, nclass)
        self.relu = nn.LeakyReLU(0.2)
        # self.layer_norm1 = nn.LayerNorm(hidden_dim)
        # self.layer_norm2 = nn.LayerNorm(nclass)
        # self.first = True
        # self.bn0 = nn.BatchNorm1d(nfeat)
        # self.bn1 = nn.BatchNorm1d(int(hidden_dim/2))
        # self.bn2 = nn.BatchNorm1d(hidden_dim)
        # self.bn3 = nn.BatchNorm1d(nclass)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [num_nodes, feature_dim]
        if adj is None:
            # if self.first:
            #     for b in range(x.shape[0]):
            #         adj_mat =  torch.corrcoef(x[b]).detach().cpu().numpy()
            #         import matplotlib.pyplot as plt
            #         plt.imshow(adj_mat, cmap='hot')
            #         plt.savefig(f'{b}.png')
            adj = torch.stack([torch.corrcoef(x[b]) for b in range(x.shape[0])])

        adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
        adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        adj = torch.where(adj > 0.8, adj, 0.0)

        # import pdb
        # pdb.set_trace()
        # x = self.bn0(x.transpose(1,2)).transpose(1,2)
        x_ = self.gc1(x, adj)
        x_ = self.relu(x_)
        x_ = F.dropout(x_, self.dropout, training=self.training)
        # x_ = self.bn1(x_.transpose(1,2)).transpose(1,2)

        x_ = self.gc2(x_, adj)
        x_ = self.relu(x_)
        x_ = F.dropout(x_, self.dropout, training=self.training)
        # x_ = self.bn2(x_.transpose(1,2)).transpose(1,2)

        x_ = self.gc3(x_, adj)
        x_ = self.relu(x_)

        # x_ = self.gc2(x_, adj)
        # x_ = self.relu(x_)
        #x_ = F.log_softmax(x_, dim=1)
        #x_ = x_.transpose(0, 1)
        x_ = torch.max(x_, dim=1, keepdim=False)[0]
        # x_ = self.bn3(x_)
        return x_


class Wave2Graph(nn.Module):
    def __init__(self, nfeat, hidden_dim, nclass, dropout=0.5):
        super(Wave2Graph, self).__init__()
        self.dropout = dropout
        # GraphConvolution, GCNConv, GCNConv2
        self.gc1 = GraphConvolution(nfeat, hidden_dim)  # (feature_dim, node_dims)
        self.gc2 = GraphConvolution(hidden_dim, nclass)

    def forward(self, x, adj=None):
        # x: Node feature matrix of shape [num_nodes, feature_dim]
        if adj is None:
            adj = torch.corrcoef(x)
            #adj = torch.where(adj > 0.0, 1.0, 0.0)
            #adj = torch.where(adj > 0.5, 1.0, 0.0)
            adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
            adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
        x_ = self.gc1(x, adj).relu()
        x_ = F.dropout(x_, self.dropout, training=self.training)
        x_ = self.gc2(x_, adj).relu()
        # x_ = F.log_softmax(x_, dim=1)
        #x_ = x_.transpose(0, 1)
        return x_


class GRU_GCN(nn.Module):
    def __init__(self, chunk_size, step_size, hidden_dim, nclass, rnn_hiddem_dim, rnn_output_dim, rnn_num_layers, dropout=0.5):
        super(GRU_GCN, self).__init__()
        self.rnn_n_layers = rnn_num_layers
        self.rnn_hiddem_dim = rnn_hiddem_dim
        self.gru = nn.GRU(input_size=nclass, hidden_size=rnn_hiddem_dim, num_layers=rnn_num_layers, batch_first=True, dropout=0.3)
        self.gc1 = GraphConvolution_v2(chunk_size, hidden_dim)  # (feature_dim, node_dims)
        # self.gc2 = GraphConvolution_v2(hidden_dim, nclass)
        self.gc2 = GraphConvolution_v2(hidden_dim, int(hidden_dim/2))
        self.gc3 = GraphConvolution_v2(int(hidden_dim/2), nclass)

        self.relu = nn.LeakyReLU(0.2)
        self.dropout = dropout
        self.chunk_size = chunk_size
        self.step_size = step_size
        self.graph_output_dim = nclass
        self.fc = nn.Linear(rnn_hiddem_dim, rnn_output_dim)


    def forward(self, x, adj=None):
        # x: B, num_node, num_feat
        num_node = x.shape[1]
        batch_size = x.shape[0]
        num_step = int((x.shape[2]-self.chunk_size)/self.step_size) +1
        x = x.unfold(dimension=2, size=self.chunk_size, step=self.step_size).reshape(-1, num_node, self.chunk_size) # B*num_step + 1, num_node, chunk_size

        if adj is None:
            adj = torch.stack([torch.corrcoef(x[b]) for b in range(x.shape[0])])
            adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
            adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
            adj = torch.where(adj > 0.8, adj, 0.0)
        x_ = self.gc1(x, adj)
        x_ = self.relu(x_)
        x_ = F.dropout(x_, self.dropout, training=self.training)
        x_ = self.gc2(x_, adj)
        x_ = self.relu(x_)
        x_ = F.dropout(x_, self.dropout, training=self.training)
        x_ = self.gc3(x_, adj)
        x_ = self.relu(x_)

        x_ = x_.view(batch_size, num_step, num_node, self.graph_output_dim)
        graph_rep = torch.max(x_, dim=2, keepdim=False)[0] # batch_size, num_step, embed_shape

        h = self.init_hidden(batch_size)

        out, _ = self.gru(graph_rep, h)
        
        out = self.fc(self.relu(out[:,-1,:].squeeze()))
        
        return out

    def init_hidden(self, batch_size):
        weight = next(self.parameters()).data
        hidden = weight.new(self.rnn_n_layers, batch_size, self.rnn_hiddem_dim).zero_().to('cuda' if torch.cuda.is_available() else 'cpu')
        return hidden


class TempCNN_GCN(nn.Module):
    def __init__(self, chunk_size, step_size, hidden_dim, graph_in_dim, graph_out_dim, dropout=0.2):
        super(TempCNN_GCN, self).__init__()
        def block(n_in, n_out, k, dilate, stride):
            def make_conv():
                conv = torch.nn.Conv1d(n_in, n_out, k, dilation=dilate, padding=0, stride=stride)
                torch.nn.init.kaiming_normal_(conv.weight)
                return conv

            return nn.Sequential(make_conv(), nn.Dropout(0.2), torch.nn.GELU())

        self.chunk_size = chunk_size
        self.step_size = step_size
        self.graph_in_dim = graph_in_dim
        self.n_in = int((self.graph_in_dim-self.chunk_size)/self.step_size) +1
        self.dropout = dropout
        self.convs = nn.Sequential(
            block(self.n_in, int(self.n_in/2), 2, 1, 1),
            block(int(self.n_in/2), int(self.n_in/4), 2, 2, 1),
            block(int(self.n_in/4), 1, 2, 4, 1),
        )

        self.__padding = [(2-1)*1, (2-1)*2, (2-1)*4]

        self.graph_output_dim = graph_out_dim
        self.gc1 = GraphConvolution_v2(chunk_size, hidden_dim)  # (feature_dim, node_dims)
        # self.gc2 = GraphConvolution_v2(hidden_dim, nclass)
        self.gc2 = GraphConvolution_v2(hidden_dim, int(hidden_dim/2))
        self.gc3 = GraphConvolution_v2(int(hidden_dim/2), graph_out_dim)

        self.relu = nn.LeakyReLU(0.2)

    def forward(self, x, adj=None):
        # x: B, num_node, num_feat
        num_node = x.shape[1]
        batch_size = x.shape[0]
        num_step = int((x.shape[2]-self.chunk_size)/self.step_size) +1
        x = x.unfold(dimension=2, size=self.chunk_size, step=self.step_size).reshape(-1, num_node, self.chunk_size) # B*num_step + 1, num_node, chunk_size

        if adj is None:
            adj = torch.stack([torch.corrcoef(x[b]) for b in range(x.shape[0])])
            adj = (adj + 1.0) * 0.5 # From (-1, 1) to (0, 1)
            adj = torch.nan_to_num(adj, nan=0.0) #NOTE: TODO here
            # adj = torch.where(adj > 0.9, adj, 0.0)
        x_ = self.gc1(x, adj)
        x_ = self.relu(x_)
        x_ = F.dropout(x_, self.dropout, training=self.training)
        x_ = self.gc2(x_, adj)
        x_ = self.relu(x_)
        x_ = F.dropout(x_, self.dropout, training=self.training)
        x_ = self.gc3(x_, adj)
        x_ = self.relu(x_)

        x_ = x_.view(batch_size, num_step, num_node, self.graph_output_dim)
        graph_rep = torch.max(x_, dim=2, keepdim=False)[0] # batch_size, num_step, embed_shape

        for conv, pad in zip(self.convs, self.__padding):
            graph_rep = conv(F.pad(graph_rep, (pad, 0)))

        return graph_rep.squeeze()
