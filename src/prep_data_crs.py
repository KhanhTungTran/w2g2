import pandas as pd
import json
from tqdm import tqdm
import librosa
import soundfile as sf
from sklearn.model_selection import StratifiedKFold
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--lb_type', default='fish')

args = parser.parse_args()

df = pd.read_excel('../data_CRS/metadata/Phonic richness on 262 minute-long recordings.xlsx')


if args.lb_type == 'fish':
    classes = ['Growl', 'Knock', 'Grunt', 'Scrape', 'Whoop', 'Raspberry', 'Purr', 'Croak', 'Hum']
    label_path = '../data_CRS/class_labels_indices_crs.csv'

    df['sum'] = df[['Growl', 'Knock', 'Grunt', 'Scrape', 'Whoop', 'Raspberry', 'Purr', 'Croak', 'Hum', 'Laugh']].sum(axis=1)
    df = df[df['sum'] == 1]
    df['label'] = df[classes].idxmax(axis=1)
    json_path = '../data_CRS/metadata'
elif args.lb_type == 'fish_multi_label':
    classes = ['Growl', 'Knock', 'Grunt', 'Scrape', 'Whoop', 'Raspberry', 'Purr', 'Croak', 'Hum', 'Laugh']
    label_path = '../data_CRS/fish_multilabel_metadata/class_labels_indices_crs.csv'

    df = df[df['Phonic richness'] != 0]
    # Function to map 1s to column names
    def map_to_column_names(row):
        return ','.join([col for col in classes if row[col] == 1])

    df['label'] = df.apply(map_to_column_names, axis=1)
    json_path = '../data_CRS/fish_multilabel_metadata'
elif args.lb_type == 'location':
    classes = ['Ba', 'Bo', 'Sa']
    label_path = '../data_CRS/location_metadata/class_labels_indices_crs.csv'
    df['label'] = df.apply(lambda row: row['File name'][:2], axis=1)
    json_path = '../data_CRS/location_metadata'
elif args.lb_type == 'reef_state':
    classes = ['H', 'D', 'R']
    label_path = '../data_CRS/reef_state_metadata/class_labels_indices_crs.csv'
    df['label'] = df.apply(lambda row: row['File name'][row['File name'].find('.')+5], axis=1)
    json_path = '../data_CRS/reef_state_metadata'


label_df = pd.DataFrame()
label_df['mid'] = classes
label_df['display_name'] = classes
label_df.index.names = ['index']
label_df.to_csv(label_path)

n_splits = 5
stratified_kfold = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)

fold_indices = []
for train_index, test_index in stratified_kfold.split(df['File name'], df['label']):
    fold_indices.append((train_index, test_index))

# Display the indices for each fold
for i, (train_idx, test_idx) in enumerate(fold_indices):
    data_list = []
    test_data_list = []

    print(f"Fold {i+1}:")
    print("Train indices:", train_idx)
    print("Test indices:", test_idx)
    print()
    for index, row in df.iterrows():
        output_file_path = f'/mnt/w2g2/data_CRS/{row["File name"]}'
        data_entry = {
            "wav": output_file_path,
            "labels": row['label']  # Get the label from the mapping
        }
        if index in test_idx:
            test_data_list.append(data_entry)
        else:
            data_list.append(data_entry)
    # Create the final JSON structure
    json_data = {
        "data": data_list
    }

    with open(f'{json_path}/tr_{i+1}.json', 'w') as json_file:
        json.dump(json_data, json_file, indent=2)

    # Create the final JSON structure
    json_data = {
        "data": test_data_list
    }

    with open(f'{json_path}/te_{i+1}.json', 'w') as json_file:
        json.dump(json_data, json_file, indent=2)
